#!/bin/sh

TARGET_LDAP=
TARGET_LDAPS=
TARGET_BACKENDS=

if test -s /root/.context.conf; then
    . /root/.context.conf
fi

if test "$OC_TOKEN" -a "$CUSTOMER_ID"; then
    if test -z "$OC_API"; then
	OC_API=kubernetes.default.svc.cluster.local
    fi
    if test -z "$OC_ROLES"; then
	OC_ROLES="infra compute worker"
    fi
    TMPFILE=/tmp/.proxy.$$
    oc "--token=$OC_TOKEN" get svc -n wsweet-$CUSTOMER_ID -o yaml \
	--insecure-skip-tls-verify "--server=https://$OC_API" \
	openldap-$CUSTOMER_ID-bluemind >$TMPFILE
    TARGET_LDAP=$(grep -A3 tcp-1389 $TMPFILE \
	| awk '/nodePort/' | sed 's|.*: ||')
    TARGET_LDAPS=$(grep -A3 tcp-1636 $TMPFILE \
	| awk '/nodePort/' | sed 's|.*: ||')
    rm -f $TMPFILE
    for role in $OC_ROLES
    do
	LDAP_BACKENDS=$(oc "--token=$OC_TOKEN" get nodes -o wide \
	    "--server=https://$OC_API" --insecure-skip-tls-verify \
	    --selector=node-role.kubernetes.io/$role= \
	    | awk "/$role/{print \$6}")
	if test "$LDAP_BACKENDS"; then
	    break
	fi
    done
elif test "$LDAP_PORT" -a "$LDAPS_PORT"; then
    TARGET_LDAP=$LDAP_PORT
    TARGET_LDAPS=$LDAPS_PORT
fi

if test -z "$LDAP_BACKENDS"; then
    echo Context does not define LDAP_BACKENDS
    echo Ignoring HAProxy configuration
    exit 0
elif test -z "$TARGET_LDAPS"; then
    echo Context does not define TARGET_LDAPS
    echo Ignoring HAProxy configuration
    exit 0
fi

cat <<EOF >/etc/haproxy/haproxy.cfg
global
    log /dev/log local0
    log /dev/log local1 notice
    chroot /var/lib/haproxy
    stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
    stats timeout 30s
    user haproxy
    group haproxy
    daemon
    ca-base /etc/ssl/certs
    crt-base /etc/ssl/private

defaults
    log global
    mode tcp
    option tcplog
    timeout connect 5000
    timeout client 50000
    timeout server 50000

backend ldap-demo-ocp
    balance source
EOF
cpt=0
for backend in $LDAP_BACKENDS
do
    echo "    server $role$cpt $backend:${TARGET_LDAP:-65334} check"
    cpt=$(expr $cpt + 1)
done >>/etc/haproxy/haproxy.cfg
cat <<EOF >>/etc/haproxy/haproxy.cfg

frontend ldap-demo-ocp
    bind 127.0.0.1:389
    default_backend ldap-demo-ocp

backend ldaps-demo-ocp
    balance source
EOF
cpt=0
for backend in $LDAP_BACKENDS
do
    echo "    server $role$cpt $backend:$TARGET_LDAPS check"
    cpt=$(expr $cpt + 1)
done >>/etc/haproxy/haproxy.cfg
cat <<EOF >>/etc/haproxy/haproxy.cfg

frontend ldaps-demo-ocp
    bind 127.0.0.1:636
    default_backend ldaps-demo-ocp
EOF

systemctl enable haproxy
systemctl restart haproxy

exit 0
