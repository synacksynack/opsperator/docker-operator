#!/bin/sh

WATCH_FOR="
    BMSWAP:swap
    BMBACKUPS:/var/backups/bluemind:noatime,nodiratime,nodev,nosuid,noexec,sync
    BMPOSTGRES:/var/lib/postgresql:noatime,nodiratime,nodev,nosuid,noexec,sync
    BMCYRUS:/var/spool/cyrus:noatime,nodiratime,nosuid,sync
    BMHSM:/var/spool/bm-hsm:noatime,nodiratime
    BMLOGS:/var/log:noatime,nodiratime,nosuid,noexec"
HAS_STOPPED=false

for volume in $WATCH_FOR
do
    eval $(echo "$volume" | awk -F: '{print "label=" $1 " mpath=" $2 " mopts=" $3}')
    if grep " $mpath " /etc/fstab >/dev/null; then
	echo "$mpath already mounted - skipping"
	continue
    fi
    BLK=
    if test -L /dev/disk/by-id/virtio-$label; then
	BLK=$(readlink /dev/disk/by-id/virtio-$label)
	BLK=/dev/$(basename $BLK)
    fi
    if test -z "$BLK"; then
	echo " $label not found, $mpath would use rootfs"
	continue
    elif ! test -b "$BLK"; then
	echo " invalid device type for $BLK, $mpath use rootfs"
	continue
    elif test "$mpath" = swap; then
	if ! mkswap /dev/$BLK; then
	    echo Failed initializing swap from $BLK, ignoring
	    continue
	fi
	uuid=$(ls -l /dev/disk/by-uuid | awk "/$BLK/{print \$9}")
	if test "$uuid"; then
	    echo "UUID=$uuid none swap sw 0 0"
	else
	    echo "/dev/$BLK none swap sw 0 0"
	fi >>/etc/fstab
	swapon -a
    fi
    if ! $HAS_STOPPED; then
	echo Stopping BlueMind before migrating FS, scheduling restart
	echo start >/tmp/bm-action
	bmctl stop
	sleep 20
	sync
	HAS_STOPPED=true
    fi
    mkdir -p /mnt/$label
    if test -z "$mopts"; then
	mopts=defaults
    fi
    if mount /dev/$BLK /mnt/$label -o "$mopts" >/dev/null 2>&1; then
	FSTYPE=`mount | awk "/ \/mnt\/$label /{print \$5}"`
    else
	FSTYPE=xfs
	echo "formatting $BLK ($FSTYPE), would mount on $mpath"
	if ! mkfs.$FSTYPE /dev/$BLK; then
	    echo "failed formatting, $mpath would use rootfs"
	    continue
	elif ! mount /dev/$BLK /mnt/$label -o "$mopts"; then
	    echo "failed mounting disk with label $label, $mpath would use rootfs"
	    continue
	fi
    fi
    mode=755
    owner=root
    group=root
    if test -d "$mpath"; then
	oldwd="`pwd`"
	cd "$mpath"
	echo "initializing $label with data from $mpath"
	if ! rsync -avWxP . /mnt/$label/; then
	    echo "failed initializing $label, $mpath would use rootfs"
	    continue
	fi
	eval `stat -c "owner=%U group=%G mode=%a" "$mpath"`
	cd "$oldwd"
	rm -fr "$mpath"
    fi
    mkdir -p "$mpath"
    chown "$owner:$group" "$mpath" /mnt/$label
    chmod "$mode" "$mpath" /mnt/$label
    uuid=$(ls -l /dev/disk/by-uuid | awk "/$BLK/{print \$9}")
    if test "$uuid"; then
	echo "UUID=$uuid $mpath $FSTYPE $mopts 0 0"
    else
	echo "/dev/$BLK $mpath $FSTYPE $mopts 0 0"
    fi >>/etc/fstab
    umount /mnt/$label
    mount "$mpath"
done

if $HAS_STOPPED; then
    echo "Done reconfiguring system"
    mount | grep -E '(xfs|btrfs|ext[234])'
fi

exit 0
