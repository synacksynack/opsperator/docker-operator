# vim:ts=2:sw=2:et:ai:sts=2:cinoptions=(0
# Copyright 2017 Martín Ferrari <tincho@tincho.org>. All Rights Reserved.
# This file is available under the Apache license.

# Syslog parser for Postfix, based on the parsing rules from:
# https://github.com/kumina/postfix_exporter
# Copyright 2017 Kumina, https://kumina.nl/
# Available under the Apache license.

# source: https://github.com/google/mtail/blob/main/examples/postfix.mtail
# fixed dealing with BlueMind log format + adding bmhostname label

const DELIVERY_DELAY_LINE /.*, relay=(?P<relay>\S+), .*,/ +
    / delays=(?P<bqm>[0-9\.]+)\/(?P<qm>[0-9\.]+)\/(?P<cs>[0-9\.]+)\/(?P<tx>[0-9\.]+),\s/
const SMTP_TLS_LINE /(\S+) TLS connection established to \S+: (\S+) with cipher (\S+) \((\d+)\/(\d+) bits\)/
const SMTPD_TLS_LINE /(\S+) TLS connection established from \S+: (\S+) with cipher (\S+) \((\d+)\/(\d+) bits\)/
const QMGR_INSERT_LINE /:.*, size=(?P<size>\d+), nrcpt=(?P<nrcpt>\d+)/
const QMGR_REMOVE_LINE /: removed$/

/ (?P<bmhostname>[\w\._-]+) postfix\/(?P<application>[\w\.-\/]+)(?:\[(?P<pid>\d+)\])?:\s+(?P<message>.*)/ {
    counter postfix_cleanup_messages_processed_total by bmhostname
    counter postfix_cleanup_messages_rejected_total by bmhostname
    $application == "cleanup" {
	/: message-id=</ {
	    postfix_cleanup_messages_processed_total[$bmhostname]++
	}
	/: reject: / {
	    postfix_cleanup_messages_rejected_total[$bmhostname]++
	}
    }

    counter postfix_lmtp_sent by bmhostname, mailbox
    counter postfix_lmtp_sent_total by bmhostname
    $application == "lmtp" {
	/ to=<(?P<mailbox>[a-zA-Z0-9@!\._-]+)>, .*status=sent .*/ {
	    postfix_lmtp_sent_total[$bmhostname]++
	    postfix_lmtp_sent[$bmhostname][$mailbox]++
	}
    }

    histogram postfix_lmtp_delivery_delay_seconds by bmhostname, stage buckets 0.001, 0.01, 0.1, 10, 1e2, 1e3
    $application == "lmtp" {
	// + DELIVERY_DELAY_LINE {
	    postfix_lmtp_delivery_delay_seconds[$bmhostname]["before_queue_manager"] = $bqm
	    postfix_lmtp_delivery_delay_seconds[$bmhostname]["queue_manager"] = $qm
	    postfix_lmtp_delivery_delay_seconds[$bmhostname]["connection_setup"] = $cs
	    postfix_lmtp_delivery_delay_seconds[$bmhostname]["transmission"] = $tx
	}
    }

    histogram postfix_pipe_delivery_delay_seconds by bmhostname, relay, stage buckets 0.001, 0.01, 0.1, 1, 10, 100, 1e3
    $application == "pipe" {
	// + DELIVERY_DELAY_LINE {
	    postfix_pipe_delivery_delay_seconds[$bmhostname][$relay]["before_queue_manager"] = $bqm
	    postfix_pipe_delivery_delay_seconds[$bmhostname][$relay]["queue_manager"] = $qm
	    postfix_pipe_delivery_delay_seconds[$bmhostname][$relay]["connection_setup"] = $cs
	    postfix_pipe_delivery_delay_seconds[$bmhostname][$relay]["transmission"] = $tx
	}
    }

    histogram postfix_qmgr_messages_inserted_recipients by bmhostname buckets 1, 2, 4, 7, 16, 32, 64, 128
    histogram postfix_qmgr_messages_inserted_size_bytes by bmhostname buckets 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9
    counter postfix_qmgr_messages_removed_total by bmhostname

    $application == "qmgr" {
	// + QMGR_INSERT_LINE {
	    postfix_qmgr_messages_inserted_recipients[$bmhostname] = $nrcpt
	    postfix_qmgr_messages_inserted_size_bytes[$bmhostname] = $size
	}
	// + QMGR_REMOVE_LINE {
	    postfix_qmgr_messages_removed_total[$bmhostname]++
	}
    }

    histogram postfix_smtp_delivery_delay_seconds by bmhostname, stage buckets 1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3
    counter postfix_smtp_tls_connections_total by bmhostname, trust, protocol, cipher, secret_bits, algorithm_bits
    $application == "smtp" {
	// + DELIVERY_DELAY_LINE {
	    postfix_smtp_delivery_delay_seconds[$bmhostname]["before_queue_manager"] = $bqm
	    postfix_smtp_delivery_delay_seconds[$bmhostname]["queue_manager"] = $qm
	    postfix_smtp_delivery_delay_seconds[$bmhostname]["connection_setup"] = $cs
	    postfix_smtp_delivery_delay_seconds[$bmhostname]["transmission"] = $tx
	}

	// + SMTP_TLS_LINE {
	    postfix_smtp_tls_connections_total[$bmhostname][$1][$2][$3][$4][$5]++
	}
    }

    counter postfix_smtpd_connects_total by bmhostname
    counter postfix_smtpd_disconnects_total by bmhostname
    counter postfix_smtpd_forward_confirmed_reverse_dns_errors_total by bmhostname
    counter postfix_smtpd_connections_lost_total by bmhostname, after_stage
    counter postfix_smtpd_messages_processed_total by bmhostname, sasl_username
    counter postfix_smtpd_messages_rejected_total by bmhostname, code
    counter postfix_smtpd_sasl_authentication_failures_total by bmhostname
    counter postfix_smtpd_tls_connections_total by bmhostname, trust, protocol, cipher, secret_bits, algorithm_bits
    $application =~ /smtpd/ {
	/ connect from / {
	    postfix_smtpd_connects_total[$bmhostname]++
	}
	/ disconnect from / {
	    postfix_smtpd_disconnects_total[$bmhostname]++
	}
	/ warning: hostname \S+ does not resolve to address / {
	    postfix_smtpd_forward_confirmed_reverse_dns_errors_total[$bmhostname]++
	}
	/ lost connection after (\w+) from / {
	    postfix_smtpd_connections_lost_total[$bmhostname][$1]++
	}
	/: client=/ {
	    /, sasl_username=(\S+)/ {
		postfix_smtpd_messages_processed_total[$bmhostname][$1]++
	    } else {
		postfix_smtpd_messages_processed_total[$bmhostname][""]++
	    }
	}
	/NOQUEUE: reject: RCPT from \S+: (\d+) / {
	    postfix_smtpd_messages_rejected_total[$bmhostname][$1]++
	}
	/warning: \S+: SASL \S+ authentication failed: / {
	    postfix_smtpd_sasl_authentication_failures_total[$bmhostname]++
	}
	// + SMTPD_TLS_LINE {
	    postfix_smtpd_tls_connections_total[$bmhostname][$1][$2][$3][$4][$5]++
	}
    }
}
