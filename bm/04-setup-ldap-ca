#!/bin/sh

BMKS=/usr/lib/jvm/bm-jdk/jre/lib/security/cacerts
KEYTOOL_TIMEOUT=1
#LDAP_BACKENDS=openldap-<crd-name>.svc.<customer-ns>.cluster.local
OPENSSL_SCLIENT_TIMEOUT=5
TMPFILE=/tmp/.ca-ldap.$$

if test -s /root/.context.conf; then
    . /root/.context.conf
fi
if test -d /etc/pki/ca-trust/source/anchors; then
    dir=/etc/pki/ca-trust/source/anchors
else
    dir=/usr/local/share/ca-certificates
fi

if test "$OC_TOKEN" -a "$CUSTOMER_ID"; then
    if test -z "$OC_API"; then
	OC_API=kubernetes.default.svc.cluster.local
    fi
    if test -z "$OC_ROLES"; then
	OC_ROLES="infra compute worker"
    fi
    oc "--token=$OC_TOKEN" get svc -n wsweet-$CUSTOMER_ID -o yaml \
	--insecure-skip-tls-verify "--server=https://$OC_API" \
	openldap-$CUSTOMER_ID-bluemind >$TMPFILE
    TARGET_LDAPS=$(grep -A3 tcp-1636 $TMPFILE \
	| awk '/nodePort/' | sed 's|.*: ||')
    rm -f $TMPFILE
    for role in $OC_ROLES
    do
	LDAP_BACKENDS=$(oc "--token=$OC_TOKEN" get nodes -o wide \
	    "--server=https://$OC_API" --insecure-skip-tls-verify \
	    --selector=node-role.kubernetes.io/$role= \
	    | awk "/$role/{print \$6}")
	if test "$LDAP_BACKENDS"; then
	    break
	fi
    done
elif test "$LDAPS_PORT"; then
    TARGET_LDAPS=$LDAPS_PORT
fi
if test -z "$LDAP_BACKENDS"; then
    echo Context does not define LDAP_BACKENDS
    echo Ignoring CA import
    exit 0
elif test -z "$TARGET_LDAPS"; then
    echo Context does not define TARGET_LDAPS
    echo Ignoring HAProxy configuration
    exit 0
fi

# concatenates couple first parts of FQDN into a domain ID
# ensuring somewhat-unique keytool alias names
DOMAINID=$(echo $LDAP_BACKENDS | awk -F. '{print $0$1;exit;}')

echo Contacting $LDAP_BACKENDS looking for CA chain ...
timeout $OPENSSL_SCLIENT_TIMEOUT openssl s_client \
    -connect $LDAP_BACKENDS:$TARGET_LDAPS -showcerts >$TMPFILE 2>&1
idx=0
dump=false
echo Parsing response ...
while read line
do
    if echo "$line" | grep 'BEGIN CERTIFICATE' >/dev/null; then
	dump=true
    fi
    if $dump; then
	echo "$line" >>$TMPFILE.$idx
	if echo "$line" | grep 'END CERTIFICATE' >/dev/null; then
	    dump=false
	    idx=`expr $idx + 1`
	fi
    fi
done <$TMPFILE

echo Processing $LDAP_BACKENDS certificates ...
changed=false
idx=0
should_rehash=false
while test -s $TMPFILE.$idx
do
    if test "$DEBUG"; then
	echo processing CRT:
	cat $TMPFILE.$ix
    fi
    if openssl x509 -text -noout -in $TMPFILE.$idx 2>/dev/null \
	    | grep CA:TRUE >/dev/null; then
	FINGER=$(openssl x509 -in $TMPFILE.$idx -fingerprint -noout \
		| awk -F= '{print $2}')
	if ! test -s $dir/ca-ldap-$idx.crt; then
	    cp $TMPFILE.$idx $dir/ca-ldap-$idx.crt
	    should_rehash=true
	fi
	if test "$DEBUG"; then
	    echo "CRT is CA, fingerprint $FINGER"
	fi
	if test -z "$FINGER"; then
	    echo failed resolving fingerprint - skipping
	elif ! echo changeit | keytool -list -keystore $BMKS 2>/dev/null \
		| grep -i $FINGER >/dev/null; then
	    if test "$DEBUG"; then
		echo CRT missing from BM Keystore, adding it now
	    fi
	    if ! ( echo changeit ; sleep $KEYTOOL_TIMEOUT ; echo yes ) \
		| keytool -import -trustcacerts \
		-alias $DOMAINID$idx -file $TMPFILE.$idx \
		-keystore $BMKS; then
	        echo Failed adding $FINGER
	    else
		changed=true
	    fi
	fi
    fi
    rm -f $TMPFILE.$idx
    idx=`expr $idx + 1`
done
rm -f $TMPFILE

if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
    changed=true
fi

if $changed; then
    if test -z "$NORESTART"; then
	echo Keystore was updated
	if ! grep start /tmp/bm-action >/dev/null 2>&1; then
	    echo Scheduling BlueMind restart
	    echo restart >/tmp/bm-action
	fi
    fi
fi

echo Done with $LDAP_BACKENDS

exit 0
