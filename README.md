# Opsperator

  * [Opsperator](#opsperator)
    * [Components Overview](#components-overview)
      * [Sources Repositories](#sources-repositories)
      * [Artifacts Repositories](#artifacts-repositories)
      * [Vulnerabilities and Bugs Tracker](#vulnerabilities-and-bugs-tracker)
      * [CI & CD](#ci---cd)
      * [Project Management](#project-management)
      * [Collaborative Softwares](#collaborative-softwares)
      * [Mails](#mails)
      * [Multimedia Library](#multimedia-library)
      * [Directory](#directoy)
      * [Infrastructure](#infrastructure)
      * [Others](#others)
      * [Components Index](#components-index)
      * [LDAP & Lemon-LDAP-NG SSO Support Matrix](#ldap---lemon-ldap-ng-sso-support-matrix)
    * [Build and Deploy Operator](#build-and-deploy-operator)
      * [Deploy Operator on OpenShift](#deploy-operator-on-openshift)
      * [Deploy Operator on Kubernetes](#deploy-operator-on-kubernetes)
      * [Using ARA](#using-ara)
    * [Bootstrap Operated Components](#bootstrap-operated-components)
      * [Deployment Scheme](#deployment-scheme)
      * [Integrating Components](#integrating-components)
      * [Common Variables](#common-variables)
        * [Notes on Registries](#notes-on-registries)
        * [Notes on Images Signature](#notes-on-images-signature)
        * [Notes on NetworkPolicies](#notes-on-networkpolicies)
        * [Notes on Prometheus](#notes-on-prometheus)
          * [Notes on Ceph & Prometheus](#notes-on-ceph---prometheus)
          * [Notes on BlueMind & Prometheus](#notes-on-bluemind---prometheus)
        * [Notes on HorizontalPodAutoscalers](#notes-on-horizontalpodautoscalers)
        * [Notes on Redis](#notes-on-redis)
        * [Notes on s3 Integration](#notes-on-s3-integration)
          * [s3 on MinIO](#s3-on-minio)
          * [s3 on RadosGW](#s3-on-radosgw)
        * [Notes on Exposing Non-HTTP Services](#notes-on-exposing-non-http-services)
        * [Notes on TLS Configuration](#notes-on-tls-configuration)
          * [North-South](#north-south)
          * [East-West](#east-west)
          * [Certificates Generation Configuration](#certificates-generation-configuration)
            * [Using Your Own CA](#using-your-own-ca)
            * [Using CertManager](#using-certmanager)
      * [Deploy Components](#deploy-components)
    * [Repositories & Images Index](#repositories---images-index)
      * [Custom Images Git Repositories](#custom-images-git-repositories)
      * [Third-Party Images](#third-party-images)
      * [Third-Party Configurations](#third-party-configurations)
    * [Notes](#notes)
      * [FIXME](#fixme)
      * [TODO](#todo)
      * [Other Considerations](#other-considerations)


General purpose Ansible Operator, working with Kubernetes v1.24+ or OKD 3.11 or
4.10+.


## Components Overview

Based on the [Operator SDK](https://github.com/operator-framework/operator-sdk),
release v1.15.0 - though using our own binaries & images, due to project
shipping with unusable arm64 images... Runtime remains relatively similar,
shipping with Ansible 2.9.26. Sticking with python 3.6, instead of 3.8.
Now switched to AlmaLinux or RockyLinux bases. While still leaving CentOS8
and UBI8 based Dockerfile as an FYI. CentOS8 no longer maintained, UBI won't
build on ARM.

### Sources Repositories

 * Gerrit: Tech Preview, using custom 3.6.1-1. replication to check.
 * Gitea: GA, custom v1.17.2.
 * Gogs: Tech Preview, custom v0.12.10.

### Artifacts Repositories

 * Artifactory: Dev Preview, using jfrog's oss 7.27.3 image
   Comes with default admin count / integration TODO
 * Nexus: GA, custom image (3.41.1) shipping with admin and service accounts
   provisioning, as well as optional LDAP Auth, LDAP Groups/Nexus roles mapping,
   s3-backed blob stores
 * Registry: GA. An OCI registry. Beware it would use either a custom
   CA (Kubernetes) or OpenShift Service CA, generating its service certificate,
   which may require additional configuration for your nodes to use some of
   our Tekton or Jenkins build agents. Optional Registry Console (TP).

### Vulnerability and Bugs Tracker

 * CoreOS Clair: GA, using quay.io/coreos/clair:v2.1.4, deprecation in progress,
   in favor of Trivy, which does better.
 * Errbit: GA, self-hosted airbrake, v0.9.1
 * SonarQube: GA, using custom image (9.6.1), admin and service accounts
   provisioning and optional LDAP auth, schema updates
 * Trivy: TP, as a replacement for CoreOS Clair, using non-default image adding
   arm64 support (see https://github.com/aquasecurity/trivy/issues/496)

### CI & CD

 * Ansible Run Analysis: TP, v1.5.8.
 * Jenkins: GA, using openshift origin 4.8.0 image (2.235.5+)
 * Tekton: GA, using pipelines v0.37.0, dashboard v0.27.0, triggers v0.20.1,
   chains v0.9.0

### Project Management

 * SOPlanning: GA, from their sourceforge release (1.49.00), patched to allow
   account creation on LDAP login
 * Wekan: GA, 6.51

### Collaborative Softwares

 * CodiMD: TP, using 2.4.1-1
 * Diaspora: DEV, using 0.7.15.0
 * DokuWiki: GA, 2022-07-31a, shipping with authlemonldap plugin
 * Draw: GA, using 20.2.3
 * EtherPad: TP, custom 1.8.6 patched for LemonLDAP-NG integration
 * Greenlight: GA, custom 2.12.6 patched trusting custom PKIs & setting up
   SAML integration. Relies on some pre-existing BigBlueButton deployment,
   or operator-managed Scalelite if such was detected.
 * Jitsi Meet: DEV
 * Kiwi IRC: TP, using 20.05.24.1-1 (v1.5.0)
 * LibreOfficeOnline: DEV
 * Mastodon: DEV, using 3.4.3
 * Mattermost: DEV, using 6.0.2. Teams edition lacks basic features such as
   LDAP or SAML authentication, basic Prometheus metrics, ... when in doubt, use
   RocketChat instead -- which sadly, lately, is also removing IDP related
   features to offer them as part of enterprise editions ... stay below v4!
 * NextCloud: GA, using 22.2.6, shipping with Draw, FullText Search,
   GroupFolders, Impersonate, Matomo, MarkDown Editor, Passwords and
   QuotaWarning plugins
 * RocketChat: GA, using 3.18.3
 * SysPass: TP, using 3.2.2

### Mails

 * BlueMind: DEV, 4.x - qcow hosted on some private repository, custom init can
   be found in [bm/](https://gitlab.com/synacksynack/opsperator/docker-operator/-/tree/master/bm)
   subfolder
 * Cyrus: GA, 3.0.7, LDAP based authentication & quotas.
 * Maildrop: TP, maildrop.cc fork
 * MailCatcher: GA, 0.8.2
 * MailHog: DEV, 1.0.1-1
 * OrangeAssassin: TP, 1.2b
 * Postfix: GA, 3.4.14, LDAP based authentication, mailboxes & aliases,
   RSpamd & OrangeAssassin optional integration. Relies on Cyrus for mailbox
   LMTP deliveries.
 * Roundcube: GA, 1.6.0. Relies on Cyrus (saslauthd & LDAP) authenticating
   users, and on Postfix sending messages.
 * Rspamd: TP, 3.3.2
 * SOGo: TP, 5.5.1 nightly, LDAP based GAL, SQL contacts & calendars, ActiveSync,
   based on Cyrus/Postfix
 * Sympa: TP, 6.2.40
 * ZPush: DEV, 2.6.4

### Multimedia Library

 * Airsonic-Advanced: 11.0.0-SNAPSHOT.20220625052932, GA
 * Medusa: 0.5.20, GA
 * Newznab: 0.2.3 (or from SVN, if login data provided), TP, patched for LDAP
   authentication
 * PeerTube: 4.3.0, GA
 * SABnzbd: 3.6.1, GA
 * Transmission: 2.94, GA

### Directory

 * OpenLDAP: GA, defaults to ltb-openldap 2.6.3, with mdb backend
 * FusionDirectoy: 1.3, GA, patched for PHP 7.4 & LLNG headers authentication
 * LemonLDAP-NG: 2.0.15, GA, LDAP-backed configuration & sessions, optional
   Redis sessions
 * LTB-SelfServicePassword: 1.5.1, GA
 * LTB-ServiceDesk: 0.4, GA
 * LTB-WhitePages: 0.3, GA
 * PHP LDAP Admin: 1.2.6.3, GA

### Infrastructure

 * AWX: DEV, 19.4.0
 * Backups: GA, k8s-native backups 1.0.0-20210905
 * EFK: TP, ElasticSearch 8.6.0 | OpenSearch 2.2.0, Fluentd 1.14.5,
   Kibana 8.6.0 | OpenSearch-Dashboards 2.2.0, ElasticSearch Comrade 1.2.0-3
 * MinIO: TP, RELEASE.2022-09-25T15-44-53Z, Console 0.20.5
 * Munin: TP, 2.0.49
 * NextCloud Applications Cache: TP, using patched master
 * NextCloud Lookup Server: DEV, using patched master, involves other patches
   in NextCloud, ...
 * Prometheus: TP, including Prometheus 2.42.0, AlertManager 0.25.0,
   Kubernetes Nodes Exporter 1.3.1, Kube State Metrics v2.5.0, optional
   Grafana 9.3.6 and Thanos v0.26.0
 * Rados Gateway: Quincy (17.2.5), GA, implements s3 connecting to an existing
   Ceph cluster
 * Rados Gateway S3 Buckets Explorer: GA, based on
   https://github.com/guimou/ceph-js-s3-explorer
 * Scalelite: GA, BigBlueButton LoadBalancing, v1.3.4 patched trusting custom
   CAs
 * BigBlueButton Conferences Streaming Platform: TP, BigBlueButton to PeerTube
   lives streaming manager
 * Squid Proxy: GA

### Others

 * Matomo: GA, 4.11.0
 * ReleaseBell, DEV, based on v1.7.1, patched for LemonLDAP-NG, prometheus
   & airbrake integration
 * TinyTinyRSS: TP, from upstream master, with `auth_ldap` plugin, patched for
   LDAPs support
 * WordPress: GA, v6.0.2 from public releases, wpdirauth or wp-cassify installed
   during site startup based on runtime configuration

### Components Index

| CRD Name                                  | Component               | Component Version                             | OCP/OKD | K8s     | Status |
| :---------------------------------------- | :---------------------- | :-------------------------------------------- | :-----: | :-----: | :----: |
| Airsonic                                  | airsonic-advanced       | 11.0.0-SNAPSHOT.20220625052932                | Yes     | Yes     | GA     |
| Ara                                       | ansible-run-analysis    | 1.5.8                                         | Yes     | Yes     | TP     |
| Artifactory                               | artifactory-oss         | 7.27.3                                        | Yes     | Yes     | DEV    |
| AWX                                       | ansible-awx             | 19.4.0                                        | Yes     | Yes     | DEV    |
| Backups                                   | backups                 | 1.0.0-20210905                                | Yes     | Yes     | GA     |
| BigBlueButtonConferencesStreamingPlatform | conferences streaming   | 0.0.2                                         | Yes     | Yes     | TP     |
| BlueMind                                  | bluemind                | 4.x                                           | Fixme   | Yes     | DEV    |
| Clair                                     | coreos clair            | 2.1.4 - deprecated                            | Yes     | Yes     | GA     |
| CodiMD                                    | codimd                  | 2.4.1-1                                       | Yes     | Yes     | TP     |
| Cyrus                                     | cyrus                   | 3.0.7-23.el8                                  | Yes     | Yes     | TP     |
| Diaspora                                  | diaspora                | 0.7.15.0                                      | Yes     | Yes     | DEV    |
| Directory                                 | openldap                | 2.4.44, or 2.6.3 (ltb)                        | Yes     | Yes     | GA     |
| Directory                                 | lemonldap               | 2.0.15                                        | Yes     | Yes     | GA     |
| Directory                                 | fusiondirectory         | 1.3                                           | Yes     | Yes     | GA     |
| Directory                                 | ltb-selfservicepassword | 1.5.1                                         | Yes     | Yes     | GA     |
| Directory                                 | ltb-servicedesk         | 0.4                                           | Yes     | Yes     | GA     |
| Directory                                 | ltb-whitepages          | 0.3                                           | Yes     | Yes     | GA     |
| Directory                                 | phpldapadmin            | 1.2.6.3                                       | Yes     | Yes     | GA     |
| DokuWiki                                  | dokuwiki                | 2022-07-31a                                   | Yes     | Yes     | GA     |
| Draw                                      | draw                    | 20.2.3                                        | Yes     | Yes     | GA     |
| Errbit                                    | errbit                  | 0.9.1                                         | Yes     | Yes     | GA     |
| EtherPad                                  | etherpad                | 1.8.6                                         | Yes     | Yes     | TP     |
| Gerrit                                    | gerrit                  | 3.6.1-1                                       | Yes     | Yes     | TP     |
| Gitea                                     | gitea                   | 1.17.2                                        | Yes     | Yes     | GA     |
| Gogs                                      | gogs                    | 0.12.10                                       | Yes     | Yes     | TP     |
| Greenlight                                | greenlight (bbb)        | 2.12.6                                        | Yes     | Yes     | GA     |
| Jenkins                                   | jenkins from OKDv4.8    | 2.235.5+                                      | Yes     | Partial | GA     |
| Jitsi                                     | jitsi-meet jicofo       | stable-5963                                   | Yes     | Yes     | DEV    |
| Jitsi                                     | jitsi-meet jvb          | stable-5963                                   | Yes     | Yes     | DEV    |
| Jitsi                                     | jitsi-meet prosody      | stable-5963                                   | Yes     | Yes     | DEV    |
| Jitsi                                     | jitsi-meet web          | stable-5963                                   | Yes     | Yes     | DEV    |
| KiwiIRC                                   | kiwi-irc                | 20.05.24.1-1 (1.5.0)                          | Yes     | Yes     | TP     |
| KubeVirt                                  | kubevirt                | 0.37.1                                        | No      | Yes     | TP     |
| LibreOfficeOnline                         | libreofficeonline       | ?? (latest)                                   | Yes     | Fixme   | DEV    |
| Logging                                   | comrade                 | 1.2.0-3 (master)                              | Yes     | Yes     | TP     |
| Logging                                   | curator                 | 5.8.1                                         | Yes     | Yes     | GA     |
| Logging                                   | elasticsearch           | 8.6.0                                         | Yes     | Yes     | GA     |
| Logging                                   | fluentd                 | 1.14.5-debian-elasticsearch7-1.0              | Partial | Yes     | TP     |
| Logging                                   | kibana                  | 8.6.0                                         | Yes     | Yes     | GA     |
| Logging                                   | opensearch              | 2.2.0                                         | Yes     | Yes     | GA     |
| Logging                                   | opensearch-dashboards   | 2.2.0                                         | Yes     | Yes     | GA     |
| Maildrop                                  | m242/maildrop           | 3.0.1                                         | Yes     | Yes     | TP     |
| MailCatcher                               | mailcatcher             | v0.8.2                                        | Yes     | Yes     | GA     |
| Mailhog                                   | mailhog                 | v1.0.1-1                                      | Yes     | Yes     | GA     |
| Mastodon                                  | mastodon                | 3.4.3                                         | Yes     | Yes     | DEV    |
| Matomo                                    | matomo                  | 4.11.0                                        | Yes     | Yes     | GA     |
| Mattermost                                | mattermost              | 6.0.2                                         | Yes     | Yes     | DEV    |
| Medusa                                    | medusa                  | 0.5.20                                        | Yes     | Yes     | GA     |
| MinIO                                     | console                 | 0.20.5                                        | Yes     | Yes     | TP     |
| MinIO                                     | minio                   | RELEASE.2022-09-25T15-44-53Z                  | Yes     | Yes     | TP     |
| Munin                                     | munin                   | 2.0.49                                        | Yes     | Yes     | TP     |
| Newznab                                   | newznab                 | 0.2.3                                         | Yes     | Yes     | TP     |
| NextCloud                                 | nextcloud               | patched 22.2.6 / 23.0.3                       | Yes     | Yes     | GA     |
| NextCloudApplicationsCache                | nextcloud apps-cache    | master                                        | Yes     | Yes     | TP     |
| NextCloudLookupServer                     | nextcloud lookup-server | patched 0.3.2                                 | Yes     | Yes     | DEV    |
| Nexus                                     | nexus                   | 3.41.1-01                                     | Yes     | Yes     | GA     |
| OrangeAssassin                            | orangeassassin          | 1.2b-20210905                                 | Yes     | Yes     | TP     |
| PeerTube                                  | peertube                | 4.3.0                                         | Yes     | Yes     | GA     |
| Postfix                                   | postfix                 | 3.4.14-0-deb10u1                              | Yes     | Yes     | TP     |
| Prometheus                                | alertmanager            | 0.25.0                                        | Yes     | Yes     | GA     |
| Prometheus                                | grafana                 | 9.3.6                                         | Yes     | Yes     | GA     |
| Prometheus                                | kube-state-metrics      | v2.5.0                                        | Yes     | Yes     | GA     |
| Prometheus                                | node-exporter           | 1.3.1                                         | Yes     | Yes     | GA     |
| Prometheus                                | prometheus              | 2.42.0                                        | Yes     | Yes     | GA     |
| Prometheus                                | thanos-bucket           | v0.26.0                                       | Yes     | Yes     | DEV    |
| Prometheus                                | thanos-compact          | v0.26.0                                       | Yes     | Yes     | TP     |
| Prometheus                                | thanos-query            | v0.26.0                                       | Yes     | Yes     | TP     |
| Prometheus                                | thanos-queryfront       | v0.26.0                                       | Yes     | Yes     | TP     |
| Prometheus                                | thanos-receive          | v0.26.0                                       | Yes     | Yes     | GA     |
| Prometheus                                | thanos-rule             | v0.26.0                                       | Yes     | Yes     | GA     |
| Prometheus                                | thanos-sidecar          | v0.26.0                                       | Yes     | Yes     | GA     |
| Prometheus                                | thanos-store            | v0.26.0                                       | Yes     | Yes     | TP     |
| RadosGW                                   | radosgw                 | Quincy / 17.2.5                               | Yes     | Yes     | GA     |
| ReleaseBell                               | cloudron-releasebell    | 1.7.1                                         | Yes     | Yes     | DEV    |
| Registry                                  | docker-registry         | 2.8.1                                         | Yes     | Yes     | GA     |
| Registry                                  | registry-console        | 2.3.3                                         | Yes     | Yes     | TP     |
| RocketChat                                | rocket.chat             | 3.18.3                                        | Yes     | Yes     | GA     |
| Roundcube                                 | roundcube               | 1.6.0                                         | Yes     | Yes     | TP     |
| Rspamd                                    | rspamd                  | 3.3.2                                         | Yes     | Yes     | TP     |
| S3Explorer                                | radosgw bkt explorer    | 1.0                                           | Yes     | Yes     | GA     |
| SABnzbd                                   | sabnzbd                 | 3.6.1                                         | Yes     | Yes     | GA     |
| Scalelite                                 | scalelite               | 1.3.4                                         | Yes     | Yes     | GA     |
| SOGo                                      | sogo                    | 5.7.1                                         | Yes     | Yes     | TP     |
| SonarQube                                 | sonarqube               | 9.6.1                                         | Yes     | Yes     | GA     |
| SOPlanning                                | soplanning              | patched 1.49.00                               | Yes     | Yes     | GA     |
| Squid                                     | squid                   | 4.6                                           | Yes     | Yes     | GA     |
| SyncImageTag                              | skope images sync       | latest                                        | TODO    | Yes     | TP     |
| Sympa                                     | sympa                   | 6.2.40                                        | Yes     | Yes     | TP     |
| SysPass                                   | syspass                 | 3.2.2                                         | Yes     | Yes     | TP     |
| Tekton                                    | tekton-assets           | 2022.05.29                                    | Yes     | Yes     | GA     |
| Tekton                                    | tekton-chains           | 0.9.0                                         | Yes     | Yes     | DEV    |
| Tekton                                    | tekton-dashboard        | 0.27.0                                        | Maybe?  | Yes     | TP     |
| Tekton                                    | tekton-pipelines        | 0.37.0                                        | Yes     | Yes     | GA     |
| Tekton                                    | tekton-trigger          | 0.20.1                                        | Yes     | Yes     | GA     |
| TinyTinyRSS                               | tinytinyrss             | master                                        | Yes     | Yes     | TP     |
| Transmission                              | transmission            | 2.94                                          | Yes     | Yes     | GA     |
| Trivy                                     | trivy server            | 0.28.0                                        | Yes     | Yes     | TP     |
| Wekan                                     | wekan                   | 6.51                                          | Yes     | Yes     | GA     |
| WordPress                                 | wordpress               | 6.0.2                                         | Yes     | Yes     | GA     |
| ZPush                                     | zpush                   | 2.6.4                                         | Yes     | Yes     | DEV    |

In addition of which, the following softwares / images may be used:

| Product       | Version     | Specificity                                  |
| :-----------: | :---------: | :------------------------------------------: |
| Apache        | 2.4.52      | mod_perl / LemonLDAP-NG integration          |
| Cassandra     | 4.0.1       | Cluster auto-configuration                   |
| ElasticSearch | 8.6.0       | You-know-for-search                          |
| Java          | 15.0.2-7    | AdoptOpenJDK                                 |
| HAProxy       | 2.5.5       | Custom Load Balancing                        |
| MariaDB       | 10.7        | docker-libray                                |
| Memcached     | 1.6.12      | Official image                               |
| MongoDB       | 4.4.10      | ReplicaSet auto-configuration, exposes oplog |
| Nginx         | 1.19.3      | Custom Application Proxy                     |
| NodeJS        | 10/12/14/16 | S2I + PM2 integration                        |
| OpenSearch    | 1.2.4       | You-know-for-search Apache2                  |
| Percona       | 5.7.36      | Cluster auto-configuration                   |
| Postgres      | 12          | docker-library                               |
| PHP           | 7.4.27      | Based on our custom Apache base              |
| Redis         | 6.2.5       | Master/Slave auto-configuration              |
| Sshd          | 1.0         | Custom SSHD sidecar (backups/gerrit/...)     |

### LDAP & Lemon-LDAP-NG SSO Support Matrix

See [Opsperator GitLab Page](https://synacksynack.gitlab.io/opsperator/opsperator/).

## Build and Deploy Operator

### Deploy Operator on OpenShift

Using repository Makefile:

```sh
$ oc new-project opsperator
$ oc label project opsperator netpol=opsperator
$ make ocdeploy
```

Or manually:

```sh
$ oc process -f deploy/openshift/crd-v4.yaml | oc apply -f-
$ oc process -f deploy/openshift/rbac-v4.yaml | oc apply -f-
$ oc new-project opsperator
$ oc process -f deploy/openshift/namespace.yaml \
    -p OPERATOR_NAMESPACE=opeperator \
    | oc apply -f-; \
$ oc process -f deploy/openshift/run-ephemeral.yaml \
    -p OPERATOR_NAMESPACE=opeperator \
    | oc apply -f-; \
```

Building your own image:

```sh
$ oc process -f deploy/openshift/ci/imagestream.yaml | oc apply -f-
$ oc process -f deploy/openshift/ci/build-with-secret.yaml \
    -p GIT_DEPLOYMENT_TOKEN=<my-git-token> | oc apply -f-
```

### Deploy Operator on Kubernetes

```sh
$ kubectl apply -f deploy/kubernetes/crd.yaml
$ kubectl apply -f deploy/kubernetes/rbac.yaml
$ kubectl apply -f deploy/kubernetes/namespace.yaml
$ kubectl apply -f deploy/kubernetes/run-ephemeral.yaml
```

Deploying components, it is recommended to enable the PodNodeSelector admission
plugin, which may be used setting nodeSelectors as an annotation on Namespaces.
If that admission controller is not enabled, then those selectors would not
be taken into consideration by Kubernetes while scheduling our Pods.

### Using ARA

To visualize Ansible reports, we may deploy ARA alongside our operator.

We would first deploy the operator using its standard configuration, as seen
above, then provision our Ara deployment using the following:

```sh
$ sed -e 's|namespace: operator|namespace: <my-operator-ns>|' \
      -e 's|name: kube|name: <my-ara-cr-name>|' \
      -e 's|root_domain:.*|root_domain: <my-ara-root-domain>|' \
      deploy/kubernetes/cr/ara.yaml | kubectl apply -f-
```

Building your own image, instead of pulling those from GitLab, you may use:

```sh
$ sed -e 's|namespace: operator|namespace: <my-operator-ns>|' \
      -e 's|name: kube|name: <my-ara-cr-name>|' \
      -e 's|images_builds_namespace: .*|images_builds_namespace: <my-ci-ns>|' \
      -e 's|k8s_registry_ns: .*|k8s_registry_ns: <my-registry-ns>|' \
      -e 's|root_domain:.*|root_domain: <my-ara-root-domain>|' \
      deploy/kubernetes/cr/ara-ci.yaml | kubectl apply -f-
```

We may then re-apply the operator deployment, from `run-ara.yaml`:

```sh
$ sed -e 's|http://ara:8080|http://ara-<my-ara-cr-name>:8080|' \
      -e 's|opsperator|<my-operator-ns>|' \
      deploy/kubernetes/run-ara.yaml | kubectl apply -f-
```

## Bootstrap Operated Components

### Deployment Scheme

This operator may deploy its component, either based on:

- an existing registry, eg `registry.gitlab.com/synacksynack/opsperator/<repo>`,
  serving images from the master branch, for repositories in the
  `gitlab.com/synacksynack/opsperator` project. An external registry may be
  configured setting the `opsperator_images_registry` variable to an URL prefix.
  Full images URL are set from that prefix, and the product image name (eg: the
  `docker-openldap` image, would be pulled from
  `${opsperator_images_registry}/docker-openldap:master`). It is assumed that
  your cluster may pull images from this registry without any authentication.
  For a sample object integrating with an external registry, see
  `deploy/kubernetes/cr/ara.yaml`.

- a CI namespace, where you would build all custom images - depending on
  which ones are actually needed hosting the components requeste by end
  users. This would be done setting the `images_builds_namespace` variable
  into your custom resources. It is assumed such Namespace exists alreay.
  The Operator may then create `BuildConfigs` or `PipelineRuns` (depending
  on your runtime), building any image that is not yet available.
  Note that images from the CI namespace may be provisioned using a
  `Ci` object, such as shown in `kubernetes/deploy/cr/ci.yaml`. We would
  then clone remote repositories (public, or private, given a clone token
  was passed to the `Ci` object), into a local Gitea or Gogs instance, then
  setup `BuildConfigs` or Tekton `PipelineRuns` based on those clones.
  This may be the best choice working on this operator internal components,
  though it may apply to any Kubernetes/Docker project as well. See
  `deploy/kubernetes/cr/directory-ci-other-namespace.yaml` for a sample.

- your own Namespace. By default, any image required by a resources
  deployment would be built, if it was not already. See
  `deploy/kubernetes/cr/directory-ci.yaml`.

Note that the two latter cases would rely on access to Git repositories,
hosting container images sources. Most of which remain to be publicly
published. In the meantime, you may need a clone token (http, when
building on OpenShift, if using `BuildConfigs`), or an SSH key (whenever
building with Tekton), so you may pull the corresponding sources.

### Integrating Components

Bear in mind that for components to integrate together (eg: NextCloud
authenticates against Directory, its drawio plugin should query our own
Draw deployment), then we should add a label to the dependent resource
(Draw, Directory), for the top level resource (NextCloud) to detect it, using
labels.

In most cases, the label matched would be formatted as such:

```
opsperator.io/<top-level-CRD-name>: <top-level-CR-name>
```

So, for Nexus to use MinIO s3 storage and authenticates users against some
Directory, and use a local proxy when connecting out of your SDN, we would
create something like this:

```
---
apiVersion: wopla.io/v1beta1
kind: Directory
metadata:
  labels:
    opsperator.io/Nexus: demo
  name: kube
  namespace: kube-infra
spec:
  do_nexus: True
---
apiVersion: wopla.io/v1beta1
kind: Squid
metadata:
  labels:
    opsperator.io/CI: demo
  name: commons
  namespace: default
---
apiVersion: wopla.io/v1beta1
kind: MinIO
metadata:
  labels:
    opsperator.io/Nexus: demo
  name: s3
  namespace: s3ns
spec:
  minio_replicas: 4
---
apiVersion: wopla.io/v1beta1
kind: Nexus
metadata:
  name: demo
  namespace: nexusns
spec:
  auth_namespace: kube-infra
  do_s3: True
  minio_namespace: s3ns
  nexus_s3_flavor: minio
  squid_namespace: default
  squid_selectors:
  - opsperator.io/CI=demo
...
```

`${identifier}_namespace` allows you to integrate with resources in a
remote namespace - default being to watch within your CR namespace. While
`${identifier}_selectors` allows you to customize the label selector used
matching resources - defaults to `[ opsperator.io/${kind}=${metadata.name} ]`.

However the following exceptions are made:

- a Backup, KubeVirt or Tekton instance in local namespace would always be
  detected, regardless of its name and labels. Cross-namespace integration
  for those resources is not supported:
  - regarding KubeVirt and Tekton, a single set of controllers would be
    deployed anyway
  - Backups could struggle when dealing with lots of resource, main loop
    should ideally run every hour, ... adding cross-namespace support is
    not planned. Several instances could share a same RadosGW/MinIO storage
    exporting their data
- the Prometheus instance is matched regardless of its name and labels, in
  the namespace whose name matches the `prometheus_namespace_match` variable
  (by default, `prometheus-monitoring`).

The samples given in `deploy/kubernetes/cr/` should include valid samples
integrating all components together.

### Common Variables

The few variables we may pass to most custom resources would include the
following:

| Name                                 | Default Value                        | Effect                                                               |
| :----------------------------------- | :----------------------------------- | :------------------------------------------------------------------- |
| `allow_rwx_storage`                  | `true`                               | Allows operator to use discovered RWX StorageClasses                 |
| `cluster_domain`                     | `cluster.local`                      | Kubernetes internal DNS root domain                                  |
| `backups_daily_start_hour`           | `15`                                 | Daily backups default start hour                                     |
| `backups_interval`                   | `daily`                              | Backups interval (`daily`, `weekly`, `hourly`)                       |
| `backups_weekly_start_day`           | `2`                                  | Weekly backups default start week-day                                |
| `default_namespace_match`            | `default`                            | Kubernetes Namespace selector value matching Kubernetes Ingresses    |
| `do_affinities`                      | `true`                               | Toggles Pods antiAffinity rules                                      |
| `do_backups`                         | `false`                              | Toggles Backups labels - then used by Backup instance                |
| `do_debugs`                          | `false`                              | Toggles debugs in images, applications & playbooks                   |
| `do_exporters`                       | `false`                              | Toggles Prometheus Exporters sidecar containers                      |
| `do_hpa`                             | `false`                              | Toggles Horizontal Pods Autoscalers configuration                    |
| `do_loadbalancer_services`           | `false`                              | Toggles Kubernetes LoadBalancer services exposing non-http apps      |
| `do_network_policy`                  | `false`                              | Toggles Kubernetes NetworkPolicy configuration                       |
| `do_pdb`                             | `false`                              | Toggles Pod Disruption Budget configuration                          |
| `do_podsecuritypolicy`               | `true`                               | Toggles Kubernetes PodSecurityPolicy configuration (only for k8s)    |
| `do_status_dashboards`               | `false`                              | Toggles CR status updates including list of Grafana Dashboards       |
| `do_topology_spread_constraints`     | `false`                              | Toggles Pods topologySpreadConstraints rules                         |
| `do_triggers`                        | `false`                              | Toggles OpenShift DeploymentConfiguration triggers configuration     |
| `do_vpa`                             | `false`                              | Toggles Vertical Pods Autoscalers configuration                      |
| `docker_registry`                    | depends                              | Kubernetes/OpenShift integrated registry address                     |
| `ensure_dbs_has_guaranteed_qosclass` | undef (`true`)                       | Ensures Databases deployed with Guaranteed QoS Class                 |
| `k8s_affinity_zone_label`            | `kubernetes.io/hostname`             | Topology Key setting up AntiAffinity & topologySpreadConstraints     |
| `k8s_namespace_nodes_selectors`      | `[ kubernetes.io/arch=amd64 ]`       | Array of Label Selectors setting up Namespace nodeSelector           |
| `k8s_resource_nodes_selectors`       | `[ ]`                                | Array of Label Selectors setting up child resources nodeSelector     |
| `k8s_registry_ns`                    | `registry`                           | Kubernetes namespace hosting your integrated registry - if any       |
| `k8s_registry_svc`                   | `registry`                           | Kubernetes service hosting your integrated registry - if any         |
| `k8s_unprivileged_userid`            | `1000`                               | When not on OpenShift, default unprivileged user ID                  |
| `images_builds_namespace`            | .metadata.namespace                  | Namespace hosting images builds objects                              |
| `ingress_filter_method`              | `none`                               | Ingress NetworkPolicies filter method (`labels`, `nodeips`, `none`)  |
| `ingress_nodes_selector`             | `node-role.kubernetes.io/infra=true` | Selector resolving Ingress Nodes addresses                           |
| `k8s_unprivileged_userid`            | `1001`                               | Kubernetes unprivileged User ID                                      |
| `network_policy_deployment_label`    | `name`                               | Kubernetes local Pod selector key generating NetworkPolicies         |
| `network_policy_namespace_label`     | `netpol`                             | Kubernetes Namespace selector key generating NetworkPolicies         |
| `no_proxy`                           | `[ ]`                                | Exclusion list setting up HTTP Proxies                               |
| `opsperator_images_registry`         | undef                                | External Registry serving with custom images - disables local builds |
| `pause`                              | undef                                | Set to `true`, to prevent the Operator from re-applying stuff        |
| `prometheus_namespace`               | `${prometheus_namespace_match}`      | Namespace hosting Prometheus, generating `.status.dashboards[]`      |
| `prometheus_namespace_match`         | `prometheus-monitoring`              | Label Value on Prometheus Namespace setting up NetworkPolicies       |
| `proxy_host`                         | undef, or Squid if CR exists         | HTTP Proxy Hostname or Address                                       |
| `proxy_port`                         | `3128`, or Squid if CR exists        | HTTP Proxy Port                                                      |
| `pull_policy`                        | `IfNotPresent`                       | Kubernetes container images pull policy                              |
| `root_domain`                        | `demo.local`                         | Root DNS domain name, exposing operated deployments                  |
| `rwo_needs_fsgroup`                  | undef (`false`)                      | Forces securityContext.fsGroup ID when mounting RWO volumes          |
| `rwx_needs_fsgroup`                  | undef (`false`)                      | Forces securityContext.fsGroup ID when mounting RWX volumes          |
| `scheduling_affinity_required`       | `true`                               | Toggles mandatory antiAffinities & topologySpreadConstraints rules   |
| `smtp_relay`                         | undef                                | SMTP relay, looks for Postix CR if unset                             |
| `smtp_port`                          | `25`                                 | SMTP port                                                            |
| `timezone`                           | `Europe/Paris`                       | Timezone to pass operated containers                                 |
| `topology_spread_max_skew`           | `1`                                  | TopologySpreadConstraints maxSkew value                              |
| `use_rwo_storage`                    | undef                                | RWO PersistentVolumeClaims should use this StorageClass              |
| `use_rwx_storage`                    | undef                                | RWX PersistentVolumeClaims should use this StorageClass              |
| `${product}_clone_secret`            | undef                                | Clone Secret to use with OKD BuildConfigs / no effect with Tekton    |
| `${product}_cpu_limit`               | depends                              | CPU Resources Limit for a given component                            |
| `${product}_cpu_request`             | depends                              | CPU Resources Request for a given component                          |
| `${product}_image_source`            | depends                              | Git (sometimes Docker) repository hosting a given component sources  |
| `${product}_memory_limit`            | depends                              | Memory Resources Limit for a given component                         |
| `${product}_memory_request`          | depends                              | Memory Resources Request for a given component                       |
| `${product}_replicas`                | depends, usually `1`                 | Replicas count a given component should include                      |
| `${product}_tag`                     | depends, usually `master`            | Git tag, branch or ref. Or Docker image tag                          |

Note that most CPU, memory & disk resources allocations defaults would be
relatively small, which should allow to demo most operated components on saller
clusters. Sizing components for production is left as an exercise, as it mostly
depends on your context. CRD should let you tune pretty much anything you may
need.

#### Notes on Registries

On OCP3, `docker_registry` defaults to
`docker-registry.default.svc.${cluster_domain}:5000`.

On OCP4, `docker_registry` defaults to
`image-registry.openshift-image-registry.svc.${cluster_domain}:5000`.

On Kubernetes `docker_registry` defaults to
`registry.${k8s_registry_ns}.svc.${cluster_domain}:5000`. If such Service
does not exist, then an Registry object would be created locally, deploying
a dummy docker registry. Note it is very likely your Kubernetes nodes would
not be able to pull images out of it without some additional manual
configuration.

By default, this operator would build its images within the
`images_registry_namespace` namespace - which defaults to that custom resource
parent namespace. Although we could instead use a shared namespace, to avoid
re-building the same assets multiple times. Or even set in a registry URL prefix
in `opsperator_images_registry`, to disable any image build.

#### Notes on Images Signature

Using Tekton building images, you may provide either a Cosign or a GPG private
key, signing images in the end of a pipeline.

With cosign:

```sh
$ cosign generate-key-pair
[ enter passphrase ]
$ kubectl create secret generic -n <ns> cosign-<tekton-cr-name> \
    --from-file=cosign.key=cosign.key \
    --from-literal=passphrase=<your-passphrase>
```

Or GPG:

```sh
$ gpg gen-key
[ enter key details/don't set a passphrase/FIXME ]
$ gpg2 --export-secret-keys -a <your-id> >private.asc
$ kubectl create secret generic -n <ns> gpg-<tekton-cr-name> \
    --from-file=private.asc=private.asc
```

Note on GPG: gathering signatures will be an issue. Prefer Cosign right now,
which could then be used with https://github.com/sse-secure-systems/connaisseur

#### Notes on NetworkPolicies

When setting `do_network_policy` to `true`, we may customize the way to match
traffic from Kubernetes Ingress controllers.

By default, `ingress_filter_method` is set to `none`, allowing all trafic to
Services that should be exposed publicly.

We may be able to harden this changing it to `labels`. Operator would then match
namespaces using `${network_policy_namespace_label}=${default_namespace_match}`
(by default: `netpol=default`).

As an alternative, setting `ingress_filter_method` to `nodeips`, the Operator
would then look for nodes with a label `${ingress_nodes_selector}`, and allow
those addresses in.

#### Notes on Prometheus

This operator would allow you to deploy the Prometheus stack, alongside
with Kubernetes API & Nodes exporters.

Grafana is an optional component, as we may also use a remote Prometheus,
federating metrics from several clusters. In which case, our operator would
allow you to configure this federation, and plug Grafana to the central
Prometheus setup. When enabled, Grafana would ship with a set of default
dashboards, matching the products deployed by our operator.

Using NetworkrPolicies and integrating with Prometheus, we would allow
connections to exporters, on from Pods in Namespaces labelled with
`${network_policy_namespace_label}=${prometheus_namespace_match}`
(by default: `netpol=prometheus-monitoring`).

Connections within projects would be restricted using labels placed by the
operator. Each deployment is identified by a label
`${network_policy_deployment_label}=${deployment_name}`
(eg: `name=<deployment-name>`).

##### Notes on Ceph & Prometheus

Optionally, we may setup collection for Ceph metrics by Prometheus. To do so,
make sure your Ceph cluster exports its metrics:

```sh
# ceph mgr module enable prometheus
# ceph config set mgr mgr/prometheus/server_addr 0.0.0.0
# ceph config set mgr mgr/prometheus/server_port 9283
# ceph config set mgr mgr/prometheus/scrape_interval 15.0
# ceph config set mgr mgr/prometheus/rbd_stats_pools kube,cephfs_data
# firewall-cmd --zone=public --permanent --add-port=9283/tcp
# firewall-cmd --reload
# systemctl restart ceph-mgr@$(hostname)
# journalctl -fu ceph-mgr@$(hostname)
# curl http://$(hostname):9283/metrics
[... restart all mgrs ...]
```

When deploying Prometheus, the operator may create an exporter Service, pointing
to your Ceph monitors (assuming, maybe wrongfully, they're also running the mgr
service and the prometheus exporter is configured), if the discovered RWO
StorageClass provisioner matches a value known to involve Ceph.

Meanwhile, when our operator deploys a Rados Gateway, integrating with an
existing Ceph cluster offering with s3 storage in Kubernetes, we could collect
additional metrics in Prometheus. To do so, we would need to create an s3 user,
such as the following:

```sh
# radosgw-admin user create --display-name=Prometheus --uid=prometheus
# radosgw-admin caps add --uid=prometheus "--caps=usage=read;buckets=read"
```

##### Notes on BlueMind & Prometheus

Deploying BlueMind out of Kubernetes, we may still want Prometheus collecting
its metrics - which is otherwise configured by the operator, using KubeVirt.

First, we would have to configure Telegraf, on the BlueMind server, serving
metrics for Prometheus:

```sh
# cat <<EOF >/etc/telegraf/telegraf.d/output-prometheus.conf
[global_tags]
    bmhostname = "mail.demo.lan"
[agent]
    interval = "10s"
    round_interval = true
    metric_batch_size = 1000
    metric_buffer_limit = 10000
    collection_jitter = "0s"
    flush_interval = "10s"
    flush_jitter = "0s"
    precision = ""
    debug = false
    quiet = false
    logfile = ""
    hostname = "10.42.253.220"
    omit_hostname = false
[[outputs.prometheus_client]]
    listen = ":9273"
    path = "/metrics"
    expiration_interval = "0"
EOF
# systemctl restart telegraf
```

Firewall configuration may be required, allowing Prometheus to connect BlueMind
on port 9273.

We should then be able to setup BM metrics collection, creating the following
objects in Kubernetes:

```sh
# cat <<EOF | kubectl create -f-
---
apiVersion: v1
kind: Endpoints
metadata:
  labels:
    k8s-app: scrape-me
  name: exporter-bluemind-demo
  namespace: demo
subsets:
- addresses:
  - ip: 10.42.253.220
  ports:
  - name: exporter-9273
    port: 9273
    protocol: TCP
---
apiVersion: v1
kind: Service
metadata:
  labels:
    k8s-app: scrape-me
  name: exporter-bluemind-demo
  namespace: demo
spec:
  ports:
  - name: exporter-9273
    port: 9273
    protocol: TCP
    targetPort: 9273
EOF
```

#### Notes on HorizontalPodAutoscalers

With OKD3, make sure your cluster was deployed with the following Ansible
variables defined. If not, you would have to deploy the metrics server:

```yaml
openshift_metrics_install_metrics: True
openshift_metrics_server_install: True
```

On OKD4+, the metrics server should be enabled by default. With the latest
Kubernetes version, it *should* be as well.

#### Notes on Redis

Several components may integrate with Redis, without much issues so far, except
for m242/Maildrop that may exhaust the number of connexions one may open against
a redis container.

Though Redis does complain, starting up, that we should raise somaxconn. Which
wasn't necessary until now. A problem being that such sysctl could be dangerous,
and as such is blacklisted by Kubelet -- even if did you properly configured a
PodSecurityPolicy and RBAC, Pods would remain in a pending state, with event
messages such as: `forbidden sysctl: "net.core.somaxconn" not whitelisted`.

Running Kubernetes, in such cases, you should edit your
`/etc/kubernetes/kubelet.env`, adding something like
`--allowed-unsafe-sysctls 'net.core.somaxconn'`, to the `KUBELET_ARGS` variable
definition, then restart kubelet. Proceed with any node likely to host Redis
containers.

Running OpenShift v4, we would instead add some MachineConfig and KubeletConfig:

```sh
$ oc label machineconfigpool worker unsafesysctl=true
$ cat <<EOF | oc apply -f-
apiVersion: machineconfiguration.openshift.io/v1
kind: KubeletConfig
metadata:
  name: custom-kubelet
spec:
  machineConfigPoolSelector:
    matchLabels:
      unsafesysctl: "true"
  kubeletConfig:
    allowedUnsafeSysctls:
    - net.core.somaxconn
EOF
```

Having allowed the `net.core.somaxconn` unsafe sysctl on your nodes, you may
then add the following variable to whichever custom resource relying on redis:

```yaml
allowed_unsafe_sysctls:
- net.core.somaxconn
```

#### Notes on s3 Integration

Components such as Backups, CodiMD, NextCloud, Nexus or Prometheus (Thanos) may
integrate with an s3 service.

This operator would allow you to deploy a MinIO cluster, or integrate with some
existing Ceph cluster, deploying a Rados Gateway.

To enable s3 integration, add to your CR:

```yaml
do_s3: True
```

##### s3 on MinIO

Integrating with MinIO, create a cluster - a sample is available in
`deploy/kubernetes/cr/minio.yaml`. Once that cluster is ready, the operator
would be able to provision s3 users and buckets for components having
s3 integration enabled.

##### s3 on RadosGW

Integrating with RadosGW, we would first create a keyring for our gateway
service, connecting to a Ceph node that has your admin keyring:

```sh
# ceph auth get-or-create client.radosgw.gateway osd 'allow rwx' \
    mon 'allow rwx' -o /etc/ceph/ceph.client.radosgw.gateway.keyring
# cat /etc/ceph/ceph.client.radosgw.gateway.keyring
[client.radosgw.gateway]
        key = abcdefxxx==
```

Grab the cluster ID and monitor addresses:

```sh
# grep -vE '^(#|[ ]*$)' /etc/ceph/ceph.conf
[global]
fsid = f980b615-746a-4e5e-b429-a364fd69838b
mon initial members = mon1,mon2,mon3
mon host = [v2:10.42.253.110:3300,v1:10.42.253.110:6789],[v2:10.42.253.111:3300,v1:10.42.253.111:6789],[v2:10.42.253.112:3300,v1:10.42.253.112:6789]
...
```

From these, edit the `deploy/kubernetes/cr/radosgw.yaml` file, entering your
own monitor addresses, fsid and radosgw keyring data. You may then deploy your
RadosGW.

Next, we need to create an s3 user, for our application.

```sh
# radosgw-admin user create --display-name=k8s-Backups --uid=k8s-backups
# radosgw-admin caps add --uid=k8s-backups "--caps=buckets=read,write"
```

We may then add the corresponding access and secret keys to the CR we would
integrate with s3. See `deploy/kubernetes/cr/backup-rados.yaml` for an example:

```yaml
backups_s3_access_key: access-key
backups_s3_bucket: bucket-name
backups_s3_flavor: radosgw   #otherwise defaults to minio
backups_s3_secret_key: secret-key
rgw_make_bucket: False       #otherwise creates bucket using credentials above
         #which is expected by MinIO, while not necessarily authorized by Ceph
```

#### Notes on Exposing Non-HTTP Services

A few services can not be exposed through cluster Ingresses. When integrating
with external services such as BlueMind, we could want to expose Rspamd,
OpenLDAP or OrangeAssassin TCP ports outside of the SDN.

When your cluster is integrating with a Cloud API - GKE, AKS, OpenStack, ...
or when MetalLB is otherwise deployed exposing LoadBalancer Services, then
you may set the `do_loadbalancer_services`, allowing the operator to expose
internal services:

```yaml
do_loadbalancer_services: True
```

Bare Metal deployments of OpenShift 3 could include the definition of some
`ingressIPNetworkCIDR`, which would allow for Load Balancer services definition
as well, though the process of routing these IPs to your OpenShift nodes is
left to you.

Nowadays, both OpenShift or Kubernetes bare-metal deployments may force the
usage of an ExternalIPs array - then, a ClusterIP Service is enough. Again, you
would have to ensure those addresses are routed to at least some of your
cluster nodes, to enter the SDN.

To allocate addresses for services that need to be exposed outside the SDN,
when LoadBalancer services can not be used, then we may use:

```yaml
ldap_external_ip: 1.2.3.4
rspamd_external_ip: 2.3.4.5
```

WARNING: I did notice Kubernetes Nginx Ingress Controller refusing to start,
due to some EADDRINUSE error. Checking with netstat, I did find an entry
listening on one of my external IPs, port 80:

```
tcp    0   0 172.21.0.102:80     0.0.0.0:*        LISTEN   1233/kube-proxy
```

This "should not" be an issue, as that external IP is in no way related to my
Kubernetes nodes. Then again, being my only suspect, I deleted the
corresponding Service, the Nginx container immediatly stopped complaining:
I guess something's wrong in the way Nginx binds its 80/443 ports.
Had I not re-deployed one of my ingress nodes, the other ones were properly
forwarding requests made to the node address, to the ingress controller, while
those made to my external IP were sent to some active Endpoints designated by my
Service.

TL;DR, unless using LoadBalancer Services, it's probably safer not to rely on
ports that could otherwise be used by hostNetwork based containers. Services
with External IPs would however do perfectly, dealing with UPNP, RTMP, ...

#### Notes on TLS Configuration

##### North-South

Most applications deployed by this operator would be exposed using a Route
or Ingress, serving applications in https.

Running on OpenShift, your router wildcard certificate would be used

On Kubernetes, a custom CA would be generated - or re-used, based on the CR
name - then used signing ingress certificates, though we could optionally
integrate with an existing CertManager operator deployment.

##### East-West

Some applications - and eventually, hopefully, most of them - may use
TLS certificates for internal communications.

Running on OpenShift, the `service.beta.openshift.io/serving-cert-secret-name`
would be used generating certificates signed by the OpenShift cluster CA.

On Kubernetes, as for Ingress certificates, the default would be to generate
a CA, based on your CR name, that we would use signing Service certificates.
The same CA would be used for both Ingress and Service certificates.

##### Certificates Generation Configuration

The following reflects default values you may customize:

```yaml
ca_country: FR
ca_keylen: 4096
ca_loc: Paris
ca_org: K8S
ca_ou: Opsperator
ca_st: France
ca_validity: 3650
internalca_renew_before: 1w1d
svc_keylen: 2048
svc_validity: 365
tls_provider: internalca
```

The `internalca_renew_before` should follow `timespec`, as described in
https://docs.ansible.com/ansible/latest/modules/openssl_certificate_info_module.html#parameters
Certificates rotation currently experimental.

###### Using Your Own CA

To use your own certificate authority, signing Kubernetes certificates, you
may provision a Secret.  The operator would look for CA within the
`kube-system` namespace - authorities may be shared with resources in different
namespaces. Authorities names would include a suffix `-root-ca`. To initialize
the Opsperator default certificate authority, we would use:

```sh
$ kubectl create secret generic -n kube-system opsperator-root-ca \
    --from-file=ca.crt=/path/to/ca.crt
    --from-file=ca.key=/path/to/ca.key
    --from-litteral=passphrase=passphrase-for-my-CA-key
```

Later on, to force the operator re-generating certificates signed by its CA:

```sh
$ kubectl get secrets -n my-namespace | awk '/-kube-tls /{print $1}' \
    | while read secret; do kubectl delete -n my-namespace secret $secret; done
```

To use more than one authority, we would set the `opsperator_ca_name` variable
into our custom resources. Then, in kube-system, you may import your own CA
into a Secret named `${opsperator_ca_name}-root-ca` -- or let the operator
generate a new CA.

###### Using CertManager

Deploying CertManager to Kubernetes:

```sh
$ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.4.1/cert-manager.crds.yaml
$ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.4.1/cert-manager.yaml
```

Then, deploying services, we would add to our CR:

```yaml
ca_org: K8S
tls_provider: cert-manager
```

As for operator-generated certificates, authorithies would be stored within
the kube-system Namespace, then imported into namespaces hosting operated
components. You may import your own CA, using:

```sh
$ kubectl create secret generic -n kube-system certmanager-opsperator-root-ca \
    --from-file=tls.crt=/path/to/ca.crt
    --from-file=tls.key=/path/to/ca.key
```

To use more than one authority, we would set the `certmanager_ca_name` variable
into our custom resources. Then, in kube-system, you may import your own CA
into a Secret named `${certmanager_ca_name}-root-ca` -- or let the operator
generate a new CA.

ACME integration remains in my TODO ...

### Deploy Components

Until further notice, look at `deploy/openshift/cr` or
`deploy/kubernetes/cr` for sample objects to deploy. Check out
`deploy/openshift/crd-v[34]` or `deploy/kubernetes/crd` for a full
inventory of which variables may be tuned.

Eventually, `roles/<crdname>/defaults/main.yaml` and
`roles/commons/defaults/main.yaml` for an exhaustive list of all
defaults - some of which that may not yet be tunable for custom
resource objects: PR or issue welcome.

The critical ones being Jenkins (OpenShift only, old version, to
be deprecated) or Tekton (maybe less capable right now, though quite
sufficient for what we would need).

The Registry might be required, if you do not have one already, while
it would require reconfiguring your Kubernetes nodes container
runtime, so they would ignore the TLS verification error pulling
images, or trust the operator-generated CA.

Then, look for the Ci - meta component, that would orchestrate the
deployment of gitea, nexus or sonarqube. Of course, you may deploy
those components individually.

## Repositories & Images Index

### Custom Images Git Repositories

| Repository                                                        | Image Size | Based on                                           |
| :---------------------------------------------------------------- | :--------: | :------------------------------------------------: |
| gitlab.com/synacksynack/opsperator/docker-airsonic                | 914MB      | opsperator/java (jre15)                            |
| gitlab.com/synacksynack/opsperator/docker-apache                  | 211MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-ara                     | 316MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-authproxy               | 210MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-backups                 | 698MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-bkpexporter             | 125MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-cassandra (3)           | 380MB      | adoptopenjdk:8-jre-hotspot-bionic                  |
| gitlab.com/synacksynack/opsperator/docker-cassandra (4)           | 501MB      | opsperator/java (jre15)                            |
| gitlab.com/synacksynack/opsperator/docker-csdexporter             | 420MB      | opsperator/java (jre15)                            |
| gitlab.com/synacksynack/opsperator/docker-curator                 | 129MB      | alpine:3.6                                         |
| gitlab.com/synacksynack/opsperator/docker-cyrus (el8)             | 390MB      | centos:8.4.2105                                    |
| gitlab.com/synacksynack/opsperator/docker-cyrus (alma)            | 325MB      | almalinux:8.5                                      |
| gitlab.com/synacksynack/opsperator/docker-cyrus (rocky)           | 330MB      | rockylinux:8.5                                     |
| gitlab.com/synacksynack/opsperator/docker-diaspora                | 659MB      | ruby:2.6-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-dokuwiki                | 491MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-draw                    | 648MB      | tomcat:9-jre11-slim                                |
| gitlab.com/synacksynack/opsperator/docker-elasticsearch           | 991MB      | opsperator/java (jre15)                            |
| gitlab.com/synacksynack/opsperator/docker-errbit                  | 347MB      | ruby:2.7.6-alpine                                  |
| gitlab.com/synacksynack/opsperator/docker-escomrade               | 247MB      | python:3.7-slim-buster                             |
| gitlab.com/synacksynack/opsperator/docker-esearchexporter         | 6.78MB     | scratch                                            |
| gitlab.com/synacksynack/opsperator/docker-fusiondirectory         | 522MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-gitea                   | 158MB      | alpine:3.15                                        |
| gitlab.com/synacksynack/opsperator/docker-gerrit                  | 676MB      | opsperator/java (jre15)                            |
| gitlab.com/synacksynack/opsperator/docker-gogs                    | 88.7M      | alpine:3.14                                        |
| gitlab.com/synacksynack/opsperator/docker-greenlight              | 339MB      | ruby:2.7.6-alpine                                  |
| gitlab.com/synacksynack/opsperator/docker-httpexporter            | 14MB       | scratch                                            |
| gitlab.com/synacksynack/opsperator/docker-java (adoptopenjdk-15)  | 393MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-java (adoptopenjdk-17)  | 406MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-java (oracle)           | 414MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-jenkins-agent-centos    | 1.21GB     | quay.io/openshift/origin-jenkins-agent-base:4.8.0  |
| gitlab.com/synacksynack/opsperator/docker-jenkins-agent-debian    | 765MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-jenkins-agent-gitleaks  | 881MB      | quay.io/openshift/origin-jenkins-agent-base:4.8.0  |
| gitlab.com/synacksynack/opsperator/docker-jenkins-agent-klar      | 805MB      | quay.io/openshift/origin-jenkins-agent-base:4.8.0  |
| gitlab.com/synacksynack/opsperator/docker-jenkins-agent-signer    | 946MB      | quay.io/openshift/origin-jenkins-agent-base:4.8.0  |
| gitlab.com/synacksynack/opsperator/docker-jenkins-agent-sonar     | 1.38G      | quay.io/openshift/origin-jenkins-agent-base:4.8.0  |
| gitlab.com/synacksynack/opsperator/docker-jenkins-agent-trivy     | 802MB      | quay.io/openshift/origin-jenkins-agent-base:4.8.0  |
| gitlab.com/synacksynack/opsperator/docker-jenkins-agent-truffle   | 771MB      | quay.io/openshift/origin-jenkins-agent-base:4.8.0  |
| gitlab.com/synacksynack/opsperator/docker-kibana                  | 1.1GB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-kiwiirc                 | 236MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-ldapexporter            | 284MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-lemonldap               | 380MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-libreofficeonline       | 2.02GB     | libreoffice/online:master                          |
| gitlab.com/synacksynack/opsperator/docker-mailcatcher             | 162MB      | ruby:2.7-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-mailhog                 | 17.3MB     | alpine:3                                           |
| gitlab.com/synacksynack/opsperator/docker-mastodon                | 1.94GB     | opsperator/node:master (14)                        |
| gitlab.com/synacksynack/opsperator/docker-matomo                  | 643MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-mattermost              | 630MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-medusa                  | 410MB      | python:3.9-slim-buster                             |
| gitlab.com/synacksynack/opsperator/docker-mongodb (deb10)         | 471MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-mongodb (ubuntu20.04)   | 486MB      | ubuntu:20.04                                       |
| gitlab.com/synacksynack/opsperator/docker-mongoexporter           | 94.7MB     | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-munin                   | 319MB      | opsperator/apache                                  |
| gitlab.com/synacksynack/opsperator/docker-mysqlexporter           | 17.4MB     | scratch                                            |
| gitlab.com/synacksynack/opsperator/docker-ncappscache             | 214MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-newznab                 | 882MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-nextcloud               | 1.44GB     | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-nextcloudlookupserver   | 764MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-nexus (el8)             | 726MB      | centos:8.4.2105                                    |
| gitlab.com/synacksynack/opsperator/docker-nexus (alma)            | 652MB      | almalinux:8.5                                      |
| gitlab.com/synacksynack/opsperator/docker-nexus (rocky)           | 658MB      | rockylinux:8.5                                     |
| gitlab.com/synacksynack/opsperator/docker-nexusexporter           | 48.3MB     | python:3.7-alpine                                  |
| gitlab.com/synacksynack/opsperator/docker-nodejs (deb10/node10)   | 630MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-nodejs (deb10/node12)   | 638MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-nodejs (deb10/node14)   | 663MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-nodejs (deb10/node16)   | 677MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-nodejs (rhel8/node14)   | 969MB      | registry.access.redhat.com/ubi8/s2i-base:1         |
| gitlab.com/synacksynack/opsperator/docker-openldap                | 164MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-opensearch              | 788MB      | opsperator/java (jre15)                            |
| gitlab.com/synacksynack/opsperator/docker-opensearchdashboards    | 1.17G      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-operator                | 521MB      | quay.io/operator-framework/ansible-operator:1.15.0 |
| gitlab.com/synacksynack/opsperator/docker-operator (with ara)     | 881MB      | quay.io/operator-framework/ansible-operator:1.15.0 |
| gitlab.com/synacksynack/opsperator/docker-operator (el8)          | 561MB      | centos:8.4.2105                                    |
| gitlab.com/synacksynack/opsperator/docker-operator (el8+with ara) | 852MB      | centos:8.4.2105                                    |
| gitlab.com/synacksynack/opsperator/docker-operator (alma)         | 503MB      | almalinux:8.5                                      |
| gitlab.com/synacksynack/opsperator/docker-operator (alma+ara)     | 778MB      | almalinux:8.5                                      |
| gitlab.com/synacksynack/opsperator/docker-operator (rocky)        | 510MB      | rockylinux:8.5                                     |
| gitlab.com/synacksynack/opsperator/docker-operator (rocky+ara)    | 785MB      | rockylinux:8.5                                     |
| gitlab.com/synacksynack/opsperator/docker-orangeassassin          | 153MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-peertube                | 1.42GB     | node:16.15.1-buster-slim                           |
| gitlab.com/synacksynack/opsperator/docker-peertube (nginx)        | 837MB      | nginx:1.21.6                                       |
| gitlab.com/synacksynack/opsperator/docker-peertubeexporter        | 123MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-percona                 | 325MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-pgexporter              | 12.4MB     | scratch                                            |
| gitlab.com/synacksynack/opsperator/docker-php (7.4)               | 485MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-php (8.0)               | 488MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-php (8.1)               | 493MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-phpldapadmin            | 703MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-postfix                 | 167MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-postfixexporter         | 83.5MB     | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-radosgw                 | 650MB      | rockylinux:8.5                                     |
| gitlab.com/synacksynack/opsperator/docker-redis                   | 101MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-redisexporter           | 9.99MB     | scratch                                            |
| gitlab.com/synacksynack/opsperator/docker-registryexporter (alpn) | 69.9MB     | python:3.7-alpine                                  |
| gitlab.com/synacksynack/opsperator/docker-registryexporter (deb)  | 143MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-rgwexporter             | 133MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-rocketchat              | 966MB      | node:12.22.6-buster-slim                           |
| gitlab.com/synacksynack/opsperator/docker-roundcube               | 638MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-rspamd                  | 149MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-s3bucketexporter        | 92.4MB     | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-s3explorer              | 218MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-sabexporter             | 125MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-sabnzbd                 | 564MB      | python:3-slim-buster                               |
| gitlab.com/synacksynack/opsperator/docker-scalelite (ruby)        | 90.5MB     | alpine:3.13                                        |
| gitlab.com/synacksynack/opsperator/docker-scalelite (nginx)       | 140MB      | nginx:1.21.1                                       |
| gitlab.com/synacksynack/opsperator/docker-selfservicepassword     | 723MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-servicedesk             | 479MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-sogo                    | 495MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-sonarqube               | 1.19GB     | opsperator/java                                    |
| gitlab.com/synacksynack/opsperator/docker-soplanning              | 602MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-squid                   | 173MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-squidexporter           | 11.9MB     | scratch                                            |
| gitlab.com/synacksynack/opsperator/docker-sshd                    | 106MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-sympa                   | 414MB      | opsperator/apache (2.4)                            |
| gitlab.com/synacksynack/opsperator/docker-syspass                 | 828MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-tinytinyrss             | 988MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-transmission            | 169MB      | debian:buster-slim                                 |
| gitlab.com/synacksynack/opsperator/docker-transmissionexporter    | 19.7MB     | alpine:latest                                      |
| gitlab.com/synacksynack/opsperator/docker-wekan                   | 579MB      | node:14.19.1-buster-slim                           |
| gitlab.com/synacksynack/opsperator/docker-whitepages              | 479MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-wordpress               | 652MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/docker-zpush                   | 529MB      | opsperator/php (7.4)                               |
| gitlab.com/synacksynack/opsperator/s2i-bbbcsp (deb10)             | 711MB      | opsperator/node:master (16)                        |
| gitlab.com/synacksynack/opsperator/s2i-codimd (deb10)             | 929MB      | opsperator/node:master (10)                        |
| gitlab.com/synacksynack/opsperator/s2i-etherpad (deb10)           | 775MB      | opsperator/node:master (14)                        |
| gitlab.com/synacksynack/opsperator/s2i-maildropapi (deb10)        | 980MB      | opsperator/node:master (16)                        |
| gitlab.com/synacksynack/opsperator/s2i-maildropsmtp (deb10)       | 961MB      | opsperator/node:master (16)                        |
| gitlab.com/synacksynack/opsperator/s2i-maildropweb (deb10)        | 1.18GB     | opsperator/node:master (16)                        |
| gitlab.com/synacksynack/opsperator/s2i-releasebell (deb10)        | 734MB      | opsperator/node:master (16)                        |

### Third-Party Images

| Image Reference                                                                                                  | Component                   |
| :--------------------------------------------------------------------------------------------------------------- | :-------------------------- |
| docker.bintray.io/jfrog/artifactory-oss:7.27.3                                                                   | Artifactory                 |
| docker.io/aauzid/bigbluebutton-livestreaming:latest                                                              | BigBlueButton RTMP Streams  |
| docker.io/ananace/skopeo:latest                                                                                  | Skopeo (tekton)             |
| docker.io/aquasec/trivy:0.28.0                                                                                   | Trivy                       |
| docker.io/bobrik/curator:5.8.1                                                                                   | ElasticSearch Curator       |
| docker.io/docker:stable                                                                                          | Tekton Docker DIND Builder  |
| docker.io/docker:dind                                                                                            | Tekton Docker DIND Server   |
| docker.io/dslim/docker-slim:latest                                                                               | Tekton Docker Dslim Builder |
| docker.io/fluent/fluentd-kubernetes-daemonset:v1.14.5-debian-elasticsearch7-1.1                                  | Fluentd                     |
| docker.io/grafana/grafana:9.3.6                                                                                  | Grafana                     |
| docker.io/grafana/loki:2.6.1                                                                                     | Loki                        |
| docker.io/greenstatic/bigbluebutton-exporter:v0.6.0                                                              | Scalelite Exporter          |
| docker.io/haproxy:2.5.5                                                                                          | HAProxy                     |
| docker.io/jitsi/jicofo:stable-5963                                                                               | Jitsi / Jicofo              |
| docker.io/jitsi/jvb:stable-5963                                                                                  | Jitsi Video Bridge          |
| docker.io/jitsi/prosody:stable-5963                                                                              | Jitsi / Prosody             |
| docker.io/jitsi/web:stable-5963                                                                                  | Jitsi Meet                  |
| docker.io/joeelliott/cert-exporter:v2.7.0                                                                        | Cert-Manager Exporter       |
| docker.io/joxit/docker-registry-ui:2.3.3-debian                                                                  | Registry Console            |
| docker.io/mariadb:10.7                                                                                           | MariaDB                     |
| docker.io/memcached:1.6.12                                                                                       | Memcached                   |
| docker.io/minio/console:v0.20.5                                                                                  | MinIO (cli)                 |
| docker.io/minio/mc:RELEASE.2022-05-09T04-08-26Z                                                                  | MinIO (cli)                 |
| docker.io/minio/minio:RELEASE.2022-09-25T15-44-53Z                                                               | MinIO (server)              |
| docker.io/moby/buildkit:v0.6.2                                                                                   | Builkit (tekton)            |
| docker.io/nginx/nginx-prometheus-exporter:0.9.0                                                                  | Nginx Exporter              |
| docker.io/openshift/oauth-proxy:latest                                                                           | OpenShift OAuth Proxy       |
| docker.io/postgres:12                                                                                            | PostgreSQL Server           |
| docker.io/prom/alertmanager:v0.25.0                                                                              | AlertManager                |
| docker.io/prom/prometheus:v2.42.0                                                                                | Prometheus                  |
| docker.io/registry:2.8.1                                                                                         | Docker Registry             |
| docker.io/systemli/prometheus-jitsi-meet-exporter:1.1.6                                                          | Jitsi Meet Exporter         |
| docker.io/xperimental/nextcloud-exporter:0.5.1                                                                   | NextCloud Exporter          |
| gcr.io/cloud-builders/kubectl:latest                                                                             | Kubectl client (tekton)     |
| gcr.io/kaniko-project/executor:v1.7.0                                                                            | Kaniko (tekton)             |
| gcr.io/tekton-releases/github.com/tektoncd/chains/cmd/controller:v0.9.0                                          | Tekton Chains               |
| gcr.io/tekton-releases/github.com/tektoncd/dashboard/cmd/dashboard:v0.27.0                                       | Tekton Dashboard            |
| gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/controller:v0.37.0                                       | Tekton Pipelines            |
| gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/git-init:v0.37.0                                         | Tekton Git-Clone Task       |
| gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/webhook:v0.37.0                                          | Tekton Pipelines            |
| gcr.io/tekton-releases/github.com/tektoncd/triggers/cmd/controller:v0.20.1                                       | Tekton Trigger              |
| gcr.io/tekton-releases/github.com/tektoncd/triggers/cmd/interceptors:v0.20.1                                     | Tekton Interceptors         |
| gcr.io/tekton-releases/github.com/tektoncd/triggers/cmd/webhook:v0.20.1                                          | Tekton Trigger              |
| index.docker.io/kubevirt/virt-api@sha256:aeac5f2d7a99472e7fc456d3abed26c2fd9c824562574ca54e12ced49de9d4f7        | KubeVirt API Server         |
| index.docker.io/kubevirt/virt-controller@sha256:c477e3fcc3ffeeb2fac6ca19e6bf9ea75062ae69d3d19978839695e0f2412f94 | KubeVirt Controller         |
| index.docker.io/kubevirt/virt-operator@sha256:7338d1812a1f2e44b832b83cf35fc6085de0a1ac385f730a1a779a69872bebb4   | KubeVirt Operator           |
| index.docker.io/kubevirt/virt-handler@sha256:37e2939cb64e902e153298c7a775c3d2d3fd455ed8cae0c2c312f8b723bd83fb    | KubeVirt Handler            |
| k8s.gcr.io/kube-state-metrics/kube-state-metrics:v2.5.0                                                          | Kube State Metrics          |
| quay.io/ansible/ansible-runner:stable-2.11-devel                                                                 | Ansible Runner              |
| quay.io/ansible/awx:19.4.0                                                                                       | Ansible AWX                 |
| quay.io/buildah/stable:v1.21.0                                                                                   | Buildah (tekton)            |
| quay.io/coreos/clair:v2.1.4                                                                                      | CoreOS Clair (deprec.)      |
| quay.io/coreos/etcd:v3.5.0                                                                                       | Etcd                        |
| quay.io/openshift/origin-cli:latest                                                                              | OC client (tekon)           |
| quay.io/openshift/origin-jenkins:4.8.0                                                                           | Jenkins                     |
| quay.io/openshift-pipeline/s2i:v0.11                                                                             | S2I (tekton)                |
| quay.io/operator-framework/ansible-operator:v1.9.0                                                               | Ansible Operator (deprec.)  |
| quay.io/prometheus/memcached-exporter:v0.9.0                                                                     | Memcached Exporter          |
| quay.io/prometheus/node-exporter:v1.3.1                                                                          | Kubernetes Node Exporter    |
| quay.io/prometheusmsteams/prometheus-msteams:v1.5.0                                                              | Prometheus Teams Proxy      |
| quay.io/thanos/thanos:v0.26.0                                                                                    | Thanos                      |
| quay.io/vqcomms/alpine-tools:3.8                                                                                 | Misc init/sidecar           |
| r.j3ss.co/img:latest                                                                                             | Img (tekton)                |

### Third-Party Configurations

Building images on Kubernetes, we would deploy a custom copy of:

- Tekton Pipeline. Upstream: https://github.com/tektoncd/pipeline/releases
  Last version included: 0.37.0
- Tekton Dashboard. Upstream: https://github.com/tektoncd/dashboard/releases
  Last version included: 0.27.0
- Tekton Trigger. Upstream: https://github.com/tektoncd/triggers/releases
  Last version included: 0.20.1
- Tekton Chains. Upstream: https://github.com/tektoncd/chains/releases
  Last version included: 0.9.0

Prometheus checks largely inspired by https://awesome-prometheus-alerts.grep.to/rules.html
(last updated 20220410).

| Gafana Dashboard Source                                          | Revision | Component                     |
| :--------------------------------------------------------------- | :------: | :---------------------------- |
| https://grafana.com/grafana/dashboards/3894                      | v5       | Apache                        |
| https://github.com/greenstatic/bigbluebutton-exporter            | 20200911 | BigBlueButton                 |
| https://grafana.com/grafana/dashboards/10849                     | v1       | Cassandra                     |
| https://github.com/rhcs-dashboard/ceph                           | 20200816 | Ceph Cluster / MDS            |
| https://grafana.com/grafana/dashboards/9966                      | v1       | Ceph Cluster Details          |
| https://github.com/rhcs-dashboard/ceph                           | 20200816 | Ceph OSD / OSD Performance    |
| https://github.com/rhcs-dashboard/ceph                           | 20200816 | Ceph Pool / Pool Details      |
| https://github.com/rhcs-dashboard/ceph                           | 20200816 | Ceph RBD / RGW                |
| https://github.com/rhcs-dashboard/ceph                           | 20200816 | Ceph RGW / RGW Details        |
| https://github.com/joe-elliott/cert-exporter                     | 20210704 | Cert Manager                  |
| https://grafana.com/grafana/dashboards/12214                     | v10      | CodiMD                        |
| https://grafana.com/grafana/dashboards/1229                      | 20210426 | Docker Engine                 |
| https://grafana.com/grafana/dashboards/2322                      | v4       | ElasticSearch                 |
| https://grafana.com/grafana/dashboards/13042                     | v2       | Fluentd                       |
| https://grafana.com/grafana/dashboards/240                       | v3       | Go Applications               |
| https://grafana.com/grafana/dashboards/3590                      | v3       | Grafana                       |
| https://grafana.com/grafana/dashboards/2428                      | v7       | HAProxy                       |
| https://github.com/engesin/Jitsi-Prometheus-Exporter             | 20210822 | Jitsi Meet                    |
| https://github.com/chamilad/kibana-prometheus-exporter           | 20210206 | Kibana                        |
| https://github.com/rahulinux/ansible-k8s-monitoring              | 20200728 | Kubernetes Capacity Planning  |
| https://github.com/rahulinux/ansible-k8s-monitoring              | 20200728 | Kubernetes Clusters Health    |
| https://grafana.com/grafana/dashboards/6417                      | v1       | Kubernetes Clusters Metrics   |
| https://github.com/rahulinux/ansible-k8s-monitoring              | 20200728 | Kubernetes Clusters Status    |
| https://grafana.com/grafana/dashboards/12123                     | v1       | Kubernetes Kubelet            |
| https://grafana.com/grafana/dashboards/12124                     | v1       | Kubernetes Networking         |
| https://grafana.com/grafana/dashboards/12125                     | v1       | Kubernetes Networking Details |
| https://grafana.com/grafana/dashboards/3140                      | v1       | Kubernetes Nodes              |
| https://github.com/rahulinux/ansible-k8s-monitoring              | 20200728 | Kubernetes Pods               |
| https://grafana.com/grafana/dashboards/2539                      | v2       | Mattermost KPI                |
| https://grafana.com/grafana/dashboards/2542                      | v2       | Mattermost Performance        |
| https://grafana.com/grafana/dashboards/2545                      | v2       | Mattermost Performance Bonus  |
| https://grafana.com/grafana/dashboards/3063                      | v1       | Memcached                     |
| https://grafana.com/grafana/dashboards/12063                     | v1       | MinIO                         |
| https://grafana.com/grafana/dashboards/2583                      | v2       | MongoDB                       |
| https://github.com/percona/grafana-dashboards                    | 20200705 | MongoDB / Percona             |
| https://github.com/percona/grafana-dashboards                    | 20200705 | MySQL / Percona               |
| https://grafana.com/grafana/dashboards/9632                      | v1       | NextCloud                     |
| https://grafana.com/grafana/dashboards/11702                     | v2       | Nexus                         |
| https://grafana.com/grafana/dashboards/11159                     | v1       | NodeJS Applications           |
| https://grafana.com/grafana/dashboards/1387                      | v1       | NodeJS Metrics                |
| https://grafana.wikimedia.org/d/000000181/openldap-labs?orgId=1  | 20200716 | OpenLDAP                      |
| https://github.com/ClusterLabs/ha_cluster_exporter               | 20210421 | PaceMaker / Corosync / DRBD   |
| https://grafana.com/grafana/dashboards/10013                     | v2       | Postfix                       |
| https://github.com/prometheus-community/postgres_exporter        | 20210413 | Postgres                      |
| https://grafana.com/grafana/dashboards/3662                      | v2       | Prometheus                    |
| https://grafana.com/grafana/dashboards/763                       | v3       | Redis                         |
| https://github.com/RocketChat/Rocket.Chat.Metrics                | 20200710 | RocketChat                    |
| https://github.com/molu8bits/s3bucket_exporter                   | 20201007 | S3 Bucket                     |
| https://github.com/juaalta/sonarqube-prometheus-exporter         | 20220220 | SonarQube                     |
| https://github.com/boynux/squid-exporter                         | 20201124 | Squid                         |
| https://github.com/thanos-io/thanos                              | 20200823 | Thanos                        |
| https://grafana.com/grafana/dashboards/11462                     | v1       | Traefik                       |

## Notes

### FIXME

 - operator-sdk:
   - k8s hangs / https://github.com/operator-framework/operator-sdk/issues/2112
     while that case was closed, I'm not sure I've seen any improvement. The
     only way to guarantee k8s won't hang is to place quite high or no cpu
     limitations on operator deployments. We could also mention memory usage
     that looks leeaky the first 48hours, until somewhat stabilizing around 4Gi
   - prometheus / https://github.com/operator-framework/operator-sdk/issues/3387
     waiting for answers that might enlighten us on the meaning or utility for
     their metrics ...
   - concurrency issues, see:
     https://github.com/operator-framework/operator-sdk/issues/3585
     still an issue in 1.15.0. Unlikely to ever get fixed... `my_dlock` kinda
     addresses this - though plays on hold would still retain resources ...

 - s3:
   - nextcloud/s3 integration has not been tested in a while
   - codimd/s3 integration theoretical
   - when terminating a MinIO (and maybe a RadosGW) deployment, we should also
     delete any s3 bucket exporter Deployment and bucket provisioning Job in
     cascade -- though these resources ownerReference points to the CR of the
     deployment using s3, thus are not terminated when purging MinIO/RadosGW.
     maybe I should look into keeping track of dependent resources. Either
     fail when the purge playbook is called for a MinIO/RadosGW in use, or
     have the purge process update dependent applications CR removing that
     dependency and waiting for applications reconfiguration (arguably ... I
     don't really like the idea of the operator updating a CR `.spec`. Then
     again, a purge playbook failing on a loop doesn't really stand out, unless
     you're looking for it ...)
   - buckets retention policy & jobs evicting a bucket -- or at least its data
     (poc in Backups / to generalize / keep optional)

 - tekton replacing buildconfigs
   - tekton dashboard can not manage ssh secrets? On the other hand, there seem
     to be some accessToken, which I did not see last I checked ...
   - should look into building some equivalent for the test steps of our Jenkins
     pipelines: at the very least, deploy a demo from the generated image, make
     sure all components start properly, eventually/hopefully run a few tests.
     Or not? GitLab CI now running some basic integration tests, which could be
     enough dealing with my own repositories. Still relevant hosting CI working
     with end-user's repositories.
   - running on k8s, getting buildah to run in an unprivileged container is a
     pain in my ass. using VFS on openshift did the trick, though regardless
     of storage driver, using chrooted builds, ... I can't manage to get
     something that would work everywhere unless running as root right now. OKD
     builds using Tekton would run unprivileged though.
   - dslim / buildkit / img little tested.
   - upgrading tekton/jenkins agents, when those agents are pulled from
     self-hosted registry: should figure out a way to enforce image being
     build (and scanned, ....) BEFORE new tags are introduced into pipelines
     configurations. Also: before upgrading trivy server, might want to
     add some check for image+pipelines assets being up to date.

 - ocp:
   - not reapplying DC configuration unless source imagestream, triggers,
     replicas count, containers count or order changed, due to a bug in
     ansible k8s plugin handling of deploymentconfiguration (does apply a new
     version even if there were no changes to apply, debugs suggest we are
     updating the `lastTriggeredImage` field from the `imageChangeParams`
     triggers, which is usually generated and only updated by OpenShift
     internals upon imagestream changes). Same remark regarding BuildConfig
     having an imageChange trigger.
   - self-hosted registry performs poorly, seen several tekton tasks failing
     due to the registry being unavailable (all fine using openshift registry,
     only concerns the docker registry deployed by our operator in OpenShift
     (only in v4?) / not an issue in vanilla Kubenetes).
   - considering removing all ocp/okd support ... after two years of beta
     testing and troubleshooting openshift components, v4 still isn't ready.
     Which is a shame: v3 used to be a good alternative to Kubernetes.

 - k8s:
   - ingress configuration currently relies on nginx or traefik ingress
     controllers specifics, should look into alternatives, ambassador,
     haproxy, ...

 - artifactory:
   - containers restart can take quite a long time (about 60 minutes). pending
     further investigations.

 - snapshots/upgrades:
   - snapshotting a DB or fs, we create a PVC which size matches sources. could
     be overkill, especially with demos or less-loaded deployments. should
     figure out a way of estimating volume size appropriately (or snaphot data
     to some s3 bucket ... if minio/radosgw were deployed, see
     `./roles/commons/tasks/snapshots/fstos3.yaml`)
   - what about snapshots timeouts? Despite working fine with my single-user
     deployments, those would definitely be too small for data duplication jobs
     on larger installations.
   - as an alternative for snapshot size and timeout issues, cluster with
     VolumeSnapshotClasses whose provisioner matches a StorageClass driver would
     be able to create VolumeSnapshot, avoiding copy jobs, speeding up the
     snapshot process, ... Note Kubernetes Snapshots controller requires
     Kubernetes v1.17+, CSI, while support is still alpha. FIXME: we should
     also compare VolumeSnapshots/StorageClass parameters.clusterID (ceph-csi,
     there should be other params to look at, with other implementations ...
     to make sure we would not create invalid volumesnapshots when multiple
     StorageClasses are defined, say we're using several ceph pools, ...)

 - grafana:
   - mysql advanced innodb metrics: one of the plotted data may end up dividing
     by zero, which brakes a graph. currently disabled / pending further
     investigations.  more generally, several metrics still seem to be missing.
   - the process of generating `.status.dashboards[]` is quite long, on the
     account of having to scan all existing dashboards resolving titles for
     UIDs, which I'ld rather keep, to avoid having variables potentially out
     of sync with the configurations shipping with our operator. Thus, it is
     disabled by default (set `do_status_dashboards: True` in your CR to
     enable it). Anyway, that feature is quite decorative, not to say useless.
   - look into exporting php metrics for Prometheus (and corresponding
     dashboards)?

 - logging:
   - create elasticsearch index template including `geo_point` typed keys

 - As of using small resources allocations by default, some applications may be
   pretty slow to start up (rocketchat, nextcloud, sonarqube, airsonic, ...). I
   should look into using variables for `livenessProbe.failureThreshold`, if not
   for most liveness/readiness params

 - fix statefulset images that do not allow for data reset/re-join
   cluster (I know I fixed openldap lately, otherwise the "initial master"
   would have re-applied initial schema/config/...)

 - redis statefulsets: when a pod gets killed and re-scheduled, its neighbors
   would still try to join the previous IP address. In a 3 members master/slave
   deployment, once the two first pods were restarted, the third one remains
   stuck, unable to connect any of its neighbors. Quorum configuration prevents
   it from taking over as a master. Then again, LemonLDAP-NG sessions are all
   fucked, until that third pod gets restarted .... should rework the way
   sentinel updates its configuration, figure out how to properly acknowledge
   such IP changes ...
   (2020-12-05: could not reproduce?)
   (2021-01-17: still there! unclear how the f this happened though)

 - mails:
   - postfix:
     - smtps everywhere:
       - OK+TLS verify: airsonic, alertmanager, awx, diaspora, errbit, gerrit,
         gitea, gogs, greenlight, jenkins, mastodon, matomo, medusa, nextcloud,
         peertube, releasebell, roundcube, sabnzbd, self-service-password, sogo,
         soplanning, sympa to postfix & postfix to sympa, tinytinyrss, wordpress
       - submit PR to https://github.com/pymedusa/Medusa/issues/8920
       - TODO: grafana, rocketchat & wekan still using `smtp://`
     - test: submission starttls+auth (used by cyrus)
   - cyrus:
     - sharding? see:
       https://www.cyrusimap.org/imap/reference/admin/murder/murder-installation.html
     - what about only exposing imaps/pops publicly?
     - missing prometheus metrics, running Cyrus 3.0.7. Documented in 3.2,
       see https://www.cyrusimap.org/imap/reference/admin/monitoring.html
     - consider adding some toggle, such as mails sent to our domain, that did
       not match an existing user email, alias or group, ... could be diverted
       and kept into some generic mailbox, ideally having some sieve filter
       routing mails to a folder named after original recipient (? mosty useful
       while migrating mailboxes in between two email solutions ... maybe too
       much of an edge case for now)
     - cyrus quota can not exceed 21/22G (?), cyradm doesn't acknowledge
       changing quota, further commands won't return anything, until imapd would
       crash. syslog would show something like `Fatal error: num too big`.
     - test: sieve filters properly sort spam into Junk subfolder
     - oauth2? https://github.com/moriyoshi/cyrus-sasl-xoauth2
   - maildrop:
     - haraka:
       - prometheus metrics
       - looks like process restarting in logs, as of enabling stats dashboard?
     - metrics for serve / static website?
   - mailcatcher:
     - metrics for smtp+web?
   - mailhog:
     - metrics for smtp+web?
   - sympa:
     - test soap
   - zpush: test

 - directory:
   - ldaps everywhere. current status:
     - OK+TLS verify: airsonic, alertmanager, ara, bluemind, codimd, cyrus
       (saslauthd+jobs), dokuwiki, draw, etherpad, fusiondirectory, gerrit,
       gitea, gogs, grafana, jitsi, kiwiirc, llng (config+sessions), mastodon,
       matomo, mattermost, nextcloud, nexus, openldap, peertube, phpldapadmin,
       postfix (saslauthd+job), prometheus, releasebell, rocketchat,
       self-service-password, service-desk, SOGo, sonarqube, soplanning, sympa,
       syspass, tinytinyrss, transmission, wekan, white-pages, wordpress
     - OK+TLS verification disabled in greenlight (by design,
       see: https://github.com/omniauth/omniauth-ldap/issues/58), and minio
       (should figure out a way to fix /etc/pki permissions ...)
     - TODO: syncrepl & readiness still uses ldap://:1389.
     - TO CHECK: Z-Push
   - upgrades:
     - should consider spliting init and run (liveness/readiness should stay
       down until directory is fully initialized -- and ideally, done
       replicating from remotes, if any)
     - having done so, should implement ldap backends migration (hdb to mbd
       especially). if replicas == 1, scale to 2 while applying the new
       backend option, wait for 2nd replica to start with new backend, then
       delete the PVC from 1st replica, then delete 1st pod, ... if
       replicas > 1, delete PVC N when applying your new backend configuration,
       and follow the statefulset (from last to first index), re-provisioning
       fresh volumes for each replica.
     - that same process could eventually make sense upgrading OpenLDAP, eg with
       2.4.59 to 2.5+: modules such as back_hbd are gone, they should no longer
       be loaded in cn=schema,cn=config. Not sure upgrade scripts would catch
       all cases that requires such configuration changes during init ... while
       re-creating PVs and re-syncing database from a previous copy may be
       more reliable. If we can be certain that Pods would not show "ready"
       until done syncing their database from prior existing remotes.
   - CAS TLS verify:
     - OK: bluemind, wordpress
   - OIDC TLS verify:
     - keys managed by operator: lemonldap
     - OK: ara, alertmanager, draw, errbit, escomrade, fusiondirectory, grafana,
       greenlight, kibana, kiwiirc, matomo, mattermost, medusa, munin,
       prometheus, registry-console, sabnzbd, servicedesk, ssp, thanos (bucket,
       query, queryfrontend), tinytinyrss, transmission, wekan, whitepages
     - OKD CA (?)
     - TODO: https://lemonldap-ng.org/documentation/2.0/applications/gitea.html
   - SAML verify signatures:
     - certificates managed by operator: lemonldap, peertube, rocketchat
     - app checks lemon signature: greenlight, nextcloud, peertube, rocketchat
     - lemon checks app signature: none
   - test LLNG postgres sessions

 - WIP:
   - when some environment variable is optional (eg: placed only if
     `do_lemon` or `do_ldap`), we should ensure it would still be passed, empty,
     when those conditions are not met / make sure they get remove, when
     previously set.
   - implement tag-by-tag upgrades / including assets backups, on components
     relying on persisting data ...
     - PoC VolumeSnapshots: openldap, nextcloud, roundcube, sogo, sympa
     - FIXME backups: artifactory? awx?
     - FIXME/to investigate: Directory.
       - LLNG can't be below LDAP/LLNG embedded version
       - FusionDirectory version should match LDAP/FD embedded version
       - SSP / ServiceDesk / PHPLDAPAdmin: doesn't really matter, could even
         downgrade ...
     - TODO/backup not needed+should still prevent downgrades: jenkins, jitsi,
       kubevirt
   - GitLab:
     - CI test: mostly DONE
       - FIXME: backups+jenkins-agents+operator (k8s api required), radosgw
         (ceph cluster required)
       - should add to all gitlab-ci some tasks running an image scan for each
         asset that was build (see PeerTube / PoC using Trivy). Also: retry=2
         on tests (docker inspect especially) and sometimes build (or just
         tests?)
     - multi-arch images build: mostly DONE
       - TIMEOUTS: mastodon, percona (unless deploying my own gitlab runner,
         the ones provided by GitLab would limit jobs to 3h, which is not
         enough building mastodon on arm64, or percona even on amd64). until
         further notice, would force deployment method to CI (for mastodon
         components), or disable HA (for percona/swapping to mariadb)
       - FIXME: jitsi, libreofficeonline, wekan
     - Upgrades:
       - trying to build Debian 11 (bullseye) based images on GitLab CI,
         we would usually fail due to some libc-bin dpkg trigger. Unclear what
         could be done to fix (tried updating builder image getting last qemu
         on bullseye, bookwork and hirsute: no luck so far). Deb11 based images
         upgrade is on hold until further notice - and AFAIR, we have similar
         issues with ubuntu 21.04, see radosgw pipeline logs ...
   - 1-N, N-N resources dependencies: say we run something like a public
     NextCloud and a private one, or a shared setup involving NextCloud Server,
     then we need to integrate several NextCloud servers against the same
     Directory. Same goes for chats, eventually Cyrus+murder, ... Although we
     could have several NextCloud authenticating against a single Directory
     while using LDAP logins, this is not the case when SAML login is enabled,
     we would need to patch LemonLDAP configuration, provisioning several
     VirtualHost of a given product, with distinct FQDNs. At some point,
     having the whole LDAP+Lemon configuration described as environment
     variables could overcomplicate things .... Should really have another look
     at refactoring LemonLDAP-NG sites provisioning. Should make an exhaustive
     list of which components integrates with another, and wether that
     integration is 1-to-1, 1-to-N or N-to-N.
   - PoC PVC resize based on StorageClass's allowVolumeExpansion / should check
     for rszvol variable registration after updating PVs, scheduling Pods
     restarts, finishing up volume resize. Should figure out a way to automate
     resizing StatefulSets-created PVC.
   - arm64:
     - TODO: awx, coreos-clair, jitsi, libreofficeonline, peertube, percona,
       sogo, wekan
       - AWX only ships with amd64 images
       - CoreOS clair, although Dockerfile seems to handle arm builds, images
         were not published. Don't want to re-build them, especially since I
         don't want to use Clair versions that came up since CoreOS is part
         of RedHat/IBM, ... Switching to Trivy, as they do it better, and
         already ship with arm images. Pending Clair removal from operator.
       - jitsi (still a PoC/did not check arm support yet)
       - libreofficeonline parent image only exists for amd64. AFAIR, rebuilding
         it was a pain, ... might want to take another look...
       - peertube doesn't build on arm64: yarn would fail while pulling libxmljs
         (building from sources) or node.bcrypt.js (building from releases)
       - percona, only releases amd64 & i386 packages. been trying to build
         from source: cloning their main git repository and initializing
         submodules creates a ~5G layer, takes about an hour, then I have to
         build percona-xtrabackup (two versions, otherwise percona build script
         would fail), next build percona-xtradb-cluster. all in all, about 3h
         for building percona itself, on x86, 4CPU/16G RAM: GitLab won't ever be
         able to finish that build, even if I stick to x64. We might still allow
         percona builds from sources on ARM/forget about pulling images from
         gitlab, though this would again be quite painful, nevermindn potential
         disk pressures
       - SOGo nightlies don't ship with arm64? pretty sure I used to have it
         working at some point ... ?!?
       - wekan relies on meteor, which is not currently available on arm64, see:
         https://github.com/meteor/meteor/issues/442
         https://github.com/meteor/meteor/issues/6033
         https://github.com/meteor/meteor-feature-requests/issues/130
         (although: wekan does offer with arm64 images, might want to look into
         it: https://github.com/wekan/wekan/blob/master/Dockerfile.arm64v8)

Among many, many others ...

### TODO

 - kubevirt little tested
 - re-do all passwords/ssh keys inputs, such as we would not include plaintext
   secrets into custom resources, and instead point to some Secrets object
 - refactor lemonldap configuration provisioning: should be triggered by
   operator somehow, such as we may use "dynamic" urls, eventually allow
   end-user to force their deployment Ingress FQDN, which would in turn be
   taken into consideration setting up SAML/OIDC/..., or Lemon portal index
   links.
 - add resources req/limit checks (?), make sure cluster nodes have enough
   resources starting a given component, remove those configurations when we
   know nodes are unlikely to have enough resources scheduling such deployments.
 - should introduce an `opsfacts` resource, where we would store main operator
   facts (that can be shared among cluster / careful ...) in an effort to no
   longer go through the k8s-flavor tasks. not sure / lots of vars are meant to
   be CR-local/still need overrides, ... might slow down things more than
   current implementation. Another take on it could be to ensure better
   concurrency, dealing with multiple loops, avoid starting 10s of playbooks
   at once. See https://github.com/operator-framework/operator-sdk/issues/3585.
   In the meantime, a dirty workaround would consist in using some shell script,
   implementing some global distributed lock -- the first task of all playbooks,
   would sleep until a lock is acquired. This definitely isn't ideal, though
   would hopefully lower the load on my test setup.
   On second thought: what about running k8s-facts only when we don't find
   sane defaults / some vars are undefined. Have cluster facts gathering end
   with writing down detected data into the corresponding CR spec and/or status?
   Re-loading them for next executions?
 - pivatebin, (nzbdrone?), floccus, webdav, caldav (stick with SOGo or
   nextcloud?  or build our own? maybe for rcube+zpush? contacts+calendars?)
 - AWX LDAP & SAML or OIDC integration
 - consider refactoring Prometheus using a cluster scoped resource, similar to
   the Logging one. Might still want to deploy project-specific Prometheus,
   AlertManager or Grafana - eventually federating cluster metrics out of the
   main Prometheus deployment. Though this answers a very specific use case, not
   sure it's worth implementing here. bare in minds it would complicate
   Directory & MinIO integrations, and implies refactoring dashboard discoveries
 - parallel image builds (some way to wait for a bunch of jobs, rather than
   current all-sequential scheme ...)
 - Jitsi: SAML/OIDC, unprivileged containers, ARM images
 - Postgres upgrade, see: https://github.com/tianon/docker-postgres-upgrade
 - OpenSearch Dashboards Prometheus Exporter won't be released by Kibana
   Prometheus Exporter, should look into forking and maintaining this, see:
   https://github.com/pjhampton/kibana-prometheus-exporter/issues/235
   https://github.com/pjhampton/kibana-prometheus-exporter/issues/236
   Or figure out of another way to do this ...
 - PeerTube / s3 integration (new in 3.4.1), not usable yet, see:
   https://github.com/Chocobozzz/PeerTube/issues/4455. Partial implementation:
   https://github.com/Chocobozzz/PeerTube/pull/4510 (sounds like tests are
   failing due to s3 host URL being returned / PT should act as a proxy!). We
   now have scripts migrating videos from filesystem to s3:
   https://github.com/Chocobozzz/PeerTube/pull/4481

### Other Considerations

 - Errbit LDAP integration could be done patching their code, see:
   https://github.com/cschiewek/devise_ldap_authenticatable
   https://github.com/RalfHerzog/docker-errbit-ldap/blob/master/Dockerfile
 - Kiwi IRC: consider serving static assets using Apache/Nginx?
 - Matomo OIDC integration does not delegate privileges to users, see:
   https://github.com/dominik-th/matomo-plugin-LoginOIDC/issues/17
 - Mattermost LDAP is a Pro feature / requires E10 sub
   Operator should do the job, though I could not test it yet
 - Mattermost Prometheus Exporter as well / requires E20 sub
   Operator should do the job, though I could not test it yet
 - Mattermost SAML integration & LDAP Groups Sync also require E20 sub
   Operator would setup OIDC login instead - though meant for GitLab
 - RocketChat is locking LDAP/SAML features to some Enterprise Edition,
   as of v4 - which I'm considering not upgrading to ...
 - NextCloud Lookup Server & NextCloud GlobalSiteSelector still does not work,
   see https://github.com/nextcloud/globalsiteselector/pull/35,
   https://github.com/nextcloud/globalsiteselector/issues/33,
   https://github.com/nextcloud/server/pull/25300, and
   https://help.nextcloud.com/t/global-site-selector-installation-with-lookup-server/44824
 - Nexus SSO is a Pro feature / not available in their OSS release.
   We could use headers passed by some LLNG-capable proxy, though it is yet
   unclear how to enable RUT ("remote user token") via the API. See
   https://help.sonatype.com/repomanager3/system-configuration/user-authentication/authentication-via-remote-user-token
 - Roundcube + memcached out of order: https://serverfault.com/a/958702/293779
 - SonarQube h2 database doesn't allow for admin password reset. might be done
   with: https://stackoverflow.com/questions/10675768/executing-script-file-in-h2-database
 - Syspass would eventually implement oauth, while currently only allowing for
   LDAP/AD, as authentication methods.
   It's in their roadmap: https://github.com/nuxsmin/sysPass/issues/422
   Owner re-affirms he wants to do this:
   https://github.com/nuxsmin/sysPass/issues/455
   Though both ticket are quite old.... don't hold your breath...
 - Diaspora does not ship with any ldap/oidc/saml integration - though could
   itself be used as an oidc provider. despite several issues and pull requests,
   everything was refused so far. shame. also, pod takes about 50minutes to
   start up, between assets generation and db init (with 200m cpu limit). A
   complete shit show...
 - CoreOS Clair used to be fine, though I'm less convinced by the last Clair
   releases (RH), their sample docker-compose comes with some additional
   pgadmin, activemq, rabbitmq, and several go workers (notifier, indexer,
   indexer-quay, matcher, swagger-ui, jaeger, ...) unclear yet which are
   mandatory, which are specific to Quay integration, ... Deprecated. Trivy
   would be used as a replacement. Recently, klar moved away to Grype.
 - consider introducing SeaweedFS as an alternative to MinIO (and maybe more?
   there's some mention of a CSI driver? while I was initially looking for
   object storage...). MinIO license changed. Other candidate: CubeFS.
