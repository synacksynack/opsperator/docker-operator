- apiVersion: apiextensions.k8s.io/v1
  kind: CustomResourceDefinition
  metadata:
    name: diasporas.wopla.io
  spec:
    group: wopla.io
    names:
      kind: Diaspora
      listKind: DiasporaList
      plural: diasporas
      singular: diaspora
    scope: Namespaced
    versions:
    - name: v1beta1
      additionalPrinterColumns:
      - name: Domain
        type: string
        description: Diaspora Domain
        jsonPath: .status.diaspora_public
      - name: Age
        type: date
        jsonPath: .metadata.creationTimestamp
      schema:
        openAPIV3Schema:
          description: Orchestrates the deployment of Diaspora
          properties:
            apiVersion:
              description: 'APIVersion defines the versioned schema of this representation
                of an object. Servers should convert recognized schemas to the latest
                internal value, and may reject unrecognized values. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
              type: string
            kind:
              description: 'Kind is a string value representing the REST resource this
                object represents. Servers may infer this from the endpoint the client
                submits requests to. Cannot be updated. In CamelCase. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
              type: string
            spec:
              properties:
                allow_rwx_storage:
                  description: Allows usage for auto-detected RWX StorageClass - defaults to True
                  type: boolean
                allowed_unsafe_sysctls:
                  description: List of Unsafe Sysctls that were administratively enabled in your cluster, and which may then be used - defaults to [ ]
                  items:
                    type: string
                  type: array
                backups_daily_start_hour:
                  description: Kubernetes Daily Backups Start Hour - defaults to 15
                  format: int64
                  type: integer
                backups_hourly_start_minute:
                  description: Kubernetes Hourly Backups Start Minute - defaults to 15
                  format: int64
                  type: integer
                backups_interval:
                  description: Kubernetes Hourly Backups Interval - defaults to daily
                  type: string
                backups_weekly_start_day:
                  description: Kubernetes Weekly Backups Start Day - defaults to 2
                  format: int64
                  type: integer
                cluster_domain:
                  description: Kubernetes internal root domain - defaults to cluster.local
                  type: string
                default_namespace_match:
                  description: NetworkPolicy Default Namespace Match label - defaults to default
                  type: string
                deploy_pod_cpu_limit:
                  description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                  type: string
                deploy_pod_cpu_request:
                  description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                  type: string
                deploy_pod_memory_limit:
                  description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                  type: string
                deploy_pod_memory_request:
                  description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                  type: string
                deploy_revision_history:
                  description: DeploymentConfigs Revision History Limit - defaults to 3
                  type: string
                diaspora_cache_size:
                  description: Diaspora Cache Size - defaults to 10Gi
                  type: string
                diaspora_country:
                  description: Diaspora Instance Country Code - defaults to fr
                  type: string
                diaspora_cpu_limit:
                  description: Diaspora CPU Limit - defaults to 600m
                  type: string
                diaspora_cpu_request:
                  description: Diaspora CPU Request - defaults to 50m
                  type: string
                diaspora_data_capacity:
                  description: Diaspora PersistentVolumeClaim requested size - defaults to 32Gi
                  type: string
                diaspora_db_cpu_limit:
                  description: Diaspora Database CPU Limit - defaults to 250m
                  type: string
                diaspora_db_data_capacity:
                  description: Diaspora Database PersistentVolumeClaim requested size - defaults to 8Gi
                  type: string
                diaspora_db_memory_limit:
                  description: Diaspora Database Memory Limit - defaults to 512Mi
                  type: string
                diaspora_db_type:
                  description: Diaspora Database Type - defaults to postgres
                  type: string
                diaspora_hpa_cpu_target:
                  description: Diaspora HPA CPU Target - defaults to hpa_cpu_target
                  format: int64
                  type: integer
                diaspora_hpa_mem_target:
                  description: Diaspora HPA Memory Target - defaults to hpa_mem_target
                  format: int64
                  type: integer
                diaspora_image_source:
                  description: Diaspora Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-diaspora
                  type: string
                diaspora_matomo_site_id:
                  description: Diaspora Matomo Site ID - defaults to 8
                  format: int64
                  type: integer
                diaspora_memory_limit:
                  description: Diaspora Memory Limit - defaults to 1280Mi
                  type: string
                diaspora_memory_request:
                  description: Diaspora Memory Request - defaults to 768Mi
                  type: string
                diaspora_replicas:
                  description: Diaspora Replicas Count - defaults to 1
                  format: int64
                  type: integer
                diaspora_sidekiq_workers:
                  description: Diaspora Sidekiq Workers Count - defaults to 5
                  format: int64
                  type: integer
                diaspora_site_name:
                  description: Diaspora Site Name - defaults to KubeTube
                  type: string
                diaspora_tag:
                  description: Diaspora Image Sources Ref - defaults to 0.7.15.0-1
                  type: string
                diaspora_unicorn_workers:
                  description: Diaspora Unicorn Workers Count - defaults to 2
                  format: int64
                  type: integer
                do_affinities:
                  description: Enables AntiAffinity rules scheduling Pods - defaults to True
                  type: boolean
                do_backups:
                  description: Enables Backups Integration - defaults to False
                  type: boolean
                do_build_trigger:
                  description: Enables BuildConfig/Tekton Triggers - defaults to True
                  type: boolean
                do_debugs:
                  description: Toggles Debug on all applications - defaults to False
                  type: boolean
                do_exporters:
                  description: Toggles Prometheus Exporters deployment - defaults to False
                  type: boolean
                do_hpa:
                  description: Enables Horizontal Pods Autoscalers configuration - defaults to False
                  type: boolean
                do_network_policy:
                  description: Enables NetworkPolicies configuration - defaults to False
                  type: boolean
                do_pdb:
                  description: Enables PodDisruptionBudgets configuration - defaults to False
                  type: boolean
                do_status_dashboards:
                  description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                    dashboards when Prometheus integration is enabled - defaults to False"
                  type: boolean
                do_topology_spread_constraints:
                  description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                  type: boolean
                do_triggers:
                  description: "Configures triggers redeploying containers when new images
                    are pushed to its source ImageStream - defaults to False"
                  type: boolean
                docker_registry:
                  description: "Source Registry hosting Images - defaults to docker-registry
                    if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                    a registry would be deployed by the operator"
                  type: string
                ensure_dbs_has_guaranteed_qosclass:
                  description: "Sets Resources Requests & Limits such as Databases would always
                    use Kubernetes Guranteed QoS Class - defaults to True"
                  type: boolean
                execution_affinity_required:
                  description: Should Execution Affinity Rules be Mandatory - defaults to True
                  type: boolean
                exporter_cpu_limit:
                  description: Prometheus Exporter container CPU Limit - defaults to 100m
                  type: string
                exporter_cpu_request:
                  description: Prometheus Exporter container CPU Request - defaults to 50m
                  type: string
                exporter_memory_limit:
                  description: Prometheus Exporter container Memory Limit - defaults to 128Mi
                  type: string
                exporter_memory_request:
                  description: Prometheus Exporter container Memory Request - defaults to 64Mi
                  type: string
                hpa_cpu_target:
                  description: Default HPA CPU Target - defaults to 75
                  format: int64
                  type: integer
                hpa_max_replicas:
                  description: Default HPA Max Replicas - defaults to 4, or min_replicas + 1
                  format: int64
                  type: integer
                hpa_min_replicas:
                  description: Default HPA Min Replicas - defaults to 2
                  format: int64
                  type: integer
                hpa_memory_target:
                  description: Default HPA Memory Target - defaults to 80
                  format: int64
                  type: integer
                images_builds_namespace:
                  description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                  type: string
                ingress_filter_method:
                  description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                  type: string
                ingress_nodes_selector:
                  description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                  type: string
                k8s_affinity_zone_label:
                  description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                  type: string
                k8s_namespace_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/os=linux ]
                  items:
                    type: string
                  type: array
                k8s_registry_ns:
                  description: Default Kubernetes integrated registry Namespace - defaults to registry
                  type: string
                k8s_registry_svc:
                  description: Default Kubernetes integrated registry Service Name - defaults to registry
                  type: string
                k8s_resource_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                  items:
                    type: string
                  type: array
                mariadb_image_source:
                  description: MariaDB source image (amd64) - defaults to docker.io/mariadb
                  type: string
                mariadb_tag:
                  description: MariaDB Image Tag - defaults to 10.7
                  type: string
                matomo_namespace:
                  description: Namespace hosting your Matomo deployment, if any - defaults to Diaspora namespace
                  type: string
                matomo_selectors:
                  description: List of Label Selectors matching Matomo CR to integrate with - defaults to [ opsperator.io/Diaspora=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                mysqlexporter_clone_secret:
                  description: MySQL Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                  type: string
                mysqlexporter_image_source:
                  description: MySQL Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-mysqlexporter
                  type: string
                mysqlexporter_tag:
                  description: MySQL Prometheus Exporter Image Sources Ref - defaults to 0.13.0
                  type: string
                k8s_unprivileged_userid:
                  description: Default Kubernetes User ID running USER-less containers - defaults to 1000
                  format: int64
                  type: integer
                network_policy_deployment_label:
                  description: NetworkPolicy Pod Label Selector Name - defaults to name
                  type: string
                network_policy_namespace_label:
                  description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                    communications - defaults to netpol"
                  type: string
                no_proxy:
                  description: "HTTP Proxy Exclusions. Note cluster internal domain names, as well as
                    ingresses root domain would always be part of those exclusions - defaults to []"
                  items:
                    type: string
                  type: array
                opsperator_images_registry:
                  description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                    Kubernetes/OpenShift integrated registries"
                  type: string
                pause:
                  description: Pauses operator, stops reconciling child resources - defaults to False
                  type: boolean
                percona_clone_secret:
                  description: Percona Base Image Sources Clone Secret - defaults to undefined
                  type: string
                percona_image_source:
                  description: Percona Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-percona
                  type: string
                percona_replicas:
                  description: Percona Replicas Count - defaults to 3
                  format: int64
                  type: integer
                percona_tag:
                  description: Percona Image Sources Ref - defaults to 5.7.36-31.55-1
                  type: string
                pgexporter_clone_secret:
                  description: Postgres Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                  type: string
                pgexporter_image_source:
                  description: Postgres Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-pgexporter
                  type: string
                pgexporter_tag:
                  description: Postgres Prometheus Exporter Image Sources Ref - defaults to 0.10.0
                  type: string
                postfix_namespace:
                  description: Namespace hosting your Postfix deployment, if any - defaults to Diaspora namespace
                  type: string
                postfix_selectors:
                  description: List of Label Selectors matching Postfix CR to integrate with - defaults to [ opsperator.io/Diaspora=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                postgres_max_connections:
                  description: Postgres Max Connections - defaults to 100
                  format: int64
                  type: integer
                postgres_shared_buffers:
                  description: Postgres Shared Buffers - defaults to 12MB
                  type: string
                prometheus_match:
                  description: Prometheus Service Label Value - defaults to scrape-me
                  type: string
                prometheus_namespace:
                  description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                  type: string
                prometheus_namespace_match:
                  description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                  type: string
                prometheus_scrape_timeout:
                  description: Prometheus Scrape Timeout - defaults to 10s
                  type: string
                prometheus_service_label:
                  description: Prometheus Service Label Name - defaults to k8s-app
                  type: string
                pull_policy:
                  description: Images Pull Policy - defaults to IfNotPresent
                  type: string
                proxy_host:
                  description: HTTP Proxy Host - defaults to undefined
                  type: string
                proxy_port:
                  description: HTTP Proxy Host - defaults to 3128
                  format: int64
                  type: integer
                redis_clone_secret:
                  description: Redis Base Image Sources Clone Secret - defaults to undefined
                  type: string
                redis_cpu_limit:
                  description: Redis Cache CPU Limit - defaults to 100m
                  type: string
                redis_data_capacity:
                  description: Redis PersistentVolumeClaim requested size - defaults to 8Gi
                  type: string
                redis_image_source:
                  description: Redis Base Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-redis
                  type: string
                redis_memory_limit:
                  description: Redis Cache Memory Limit - defaults to 256Mi
                  type: string
                redis_tag:
                  description: Redis Base Image Tag - defaults to 6.2.5
                  type: string
                redisexporter_clone_secret:
                  description: Redis Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                  type: string
                redisexporter_image_source:
                  description: Redis Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-redisexporter
                  type: string
                redisexporter_tag:
                  description: Redis Prometheus Exporter Image Tag - defaults to 1.27.0
                  type: string
                root_domain:
                  description: Customer Root Domain - defaults to demo.local
                  type: string
                rwo_needs_fsgroup:
                  description: Optional securityContext.fsGroup numeric ID, to force on containers mounting RWO storage - defaults to False
                  format: int64
                  type: integer
                rwx_needs_fsgroup:
                  description: Optional securityContext.fsGroup numeric ID, to force on containers mounting RWX storage - defaults to False
                  format: int64
                  type: integer
                scheduling_affinity_required:
                  description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                  type: boolean
                smtp_port:
                  description: SMTP Relay Port - defaults to 25
                  format: int64
                  type: integer
                smtp_relay:
                  description: SMTP Relay - defaults to undefined, discovers neighbor Postfix CR
                  type: string
                sshd_clone_secret:
                  description: Sshd Image Sources Clone Secret - defaults to undefined
                  type: string
                sshd_cpu_limit:
                  description: Sshd container CPU Limit - defaults to 100m
                  type: string
                sshd_cpu_request:
                  description: Sshd container CPU Request - defaults to 10m
                  type: string
                sshd_image_source:
                  description: Sshd sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-sshd
                  type: string
                sshd_memory_limit:
                  description: Sshd container Memory Limit - defaults to 512Mi
                  type: string
                sshd_memory_request:
                  description: Sshd container Memory Request - defaults to 128Mi
                  type: string
                sshd_tag:
                  description: Sshd Image Tag - defaults to 7.9p1-10-20220424
                  type: string
                squid_namespace:
                  description: Namespace hosting your Squid deployment, if any - defaults to Diaspora namespace
                  type: string
                squid_selectors:
                  description: List of Label Selectors matching Squid CR to integrate with - defaults to [ opsperator.io/Diaspora=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                svc_keylen:
                  description: TLS Certificates Key Length - defaults to 2048
                  format: int64
                  type: integer
                svc_validity:
                  description: TLS Certificates Validity, in days - defaults to 365
                  format: int64
                  type: integer
                tekton_buildah_image:
                  description: Tekton Buildah Image - defaults to quay.io/buildah/stable:v1.21.0
                  type: string
                tekton_docker_build_cpu_limit:
                  description: Tekton Docker Build CPU Limit - defaults to 400m
                  type: string
                tekton_docker_build_cpu_request:
                  description: Tekton Docker Build CPU Request - defaults to 200m
                  type: string
                tekton_docker_build_memory_limit:
                  description: Tekton Docker Build Memory Limit - defaults to 4Gi
                  type: string
                tekton_docker_build_memory_request:
                  description: Tekton Docker Build Memory Request - defaults to 2Gi
                  type: string
                tekton_img_image:
                  description: Tekton GenuineTools Img Image - defaults to r.j3ss.co/img
                  type: string
                tekton_k8s_version:
                  description: Tekton Pipelines Version to Deploy (on Kubernetes, use OLM with OpenShift) - defaults to 0.19.0
                  type: string
                tekton_kaniko_image:
                  description: Tekton Kaniko Image - defaults to gcr.io/kaniko-project/executor:v1.7.0
                  type: string
                tekton_oc_client_image:
                  description: Tekton OC Client Image - defaults to quay.io/openshift/origin-cli:latest
                  type: string
                tekton_preferred_builder:
                  description: Tekton Preferred Docker Builder method - defaults to buildah
                  type: string
                tekton_s2i_build_cpu_limit:
                  description: Tekton S2I Build CPU Limit - defaults to 400m
                  type: string
                tekton_s2i_build_cpu_request:
                  description: Tekton S2I Build CPU Request - defaults to 200m
                  type: string
                tekton_s2i_build_memory_limit:
                  description: Tekton S2I Build Memory Limit - defaults to 4Gi
                  type: string
                tekton_s2i_build_memory_request:
                  description: Tekton S2I Build Memory Request - defaults to 2Gi
                  type: string
                tekton_s2i_image:
                  description: Tekton S2i Builder Image - defaults to quay.io/openshift-pipeline/s2i
                  type: string
                tekton_skopeo_image:
                  description: Tekton Skopeo Image - defaults to docker.io/ananace/skopeo
                  type: string
                timezone:
                  description: Customer TimeZone - defaults to Europe/Paris
                  type: string
                topology_spread_max_skew:
                  description: TopologySpreadConstraints MaxSkew - defaults to 1
                  format: int64
                  type: integer
                use_rwo_storage:
                  description: Forces RWO StorageClass name - defaults to undefined
                  type: string
              type: object
            status:
              properties:
                dashboards:
                  description: List of Grafana Dashboards for this deployment
                  items:
                    type: string
                  type: array
                internal:
                  description: Diaspora Internal Address
                  type: string
                public:
                  description: Diaspora Public Address
                  type: string
                ready:
                  description: Marks Deployment to be Ready
                  type: boolean
              type: object
          type: object
      served: true
      storage: true
      subresources:
        status: {}
