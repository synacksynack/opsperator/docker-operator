- apiVersion: apiextensions.k8s.io/v1
  kind: CustomResourceDefinition
  metadata:
    name: libreofficeonlines.wopla.io
  spec:
    group: wopla.io
    names:
      kind: LibreOfficeOnline
      listKind: LibreOfficeOnlineList
      plural: libreofficeonlines
      shortNames:
      - lool
      singular: libreofficeonline
    scope: Namespaced
    versions:
    - name: v1beta1
      additionalPrinterColumns:
      - name: Domain
        type: string
        description: LibreOfficeOnline Domain
        jsonPath: .status.public
      - name: Age
        type: date
        jsonPath: .metadata.creationTimestamp
      schema:
        openAPIV3Schema:
          description: Orchestrates the deployment of LibreOfficeOnline
          properties:
            apiVersion:
              description: 'APIVersion defines the versioned schema of this representation
                of an object. Servers should convert recognized schemas to the latest
                internal value, and may reject unrecognized values. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
              type: string
            kind:
              description: 'Kind is a string value representing the REST resource this
                object represents. Servers may infer this from the endpoint the client
                submits requests to. Cannot be updated. In CamelCase. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
              type: string
            spec:
              properties:
                allow_rwx_storage:
                  description: Allows usage for auto-detected RWX StorageClass - defaults to True
                  type: boolean
                cluster_domain:
                  description: Kubernetes internal root domain - defaults to cluster.local
                  type: string
                default_namespace_match:
                  description: NetworkPolicy Default Namespace Match label - defaults to default
                  type: string
                deploy_pod_cpu_limit:
                  description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                  type: string
                deploy_pod_cpu_request:
                  description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                  type: string
                deploy_pod_memory_limit:
                  description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                  type: string
                deploy_pod_memory_request:
                  description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                  type: string
                deploy_revision_history:
                  description: DeploymentConfigs Revision History Limit - defaults to 3
                  type: string
                do_affinities:
                  description: Enables AntiAffinity rules scheduling Pods - defaults to True
                  type: boolean
                do_build_trigger:
                  description: Enables BuildConfig/Tekton Triggers - defaults to True
                  type: boolean
                do_debugs:
                  description: Toggles Debug on all applications - defaults to False
                  type: boolean
                do_hpa:
                  description: Enables Horizontal Pods Autoscalers configuration - defaults to False
                  type: boolean
                do_network_policy:
                  description: Enables NetworkPolicies configuration - defaults to False
                  type: boolean
                do_pdb:
                  description: Enables PodDisruptionBudgets configuration - defaults to False
                  type: boolean
                do_status_dashboards:
                  description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                    dashboards when Prometheus integration is enabled - defaults to False"
                  type: boolean
                do_topology_spread_constraints:
                  description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                  type: boolean
                do_triggers:
                  description: "Configures triggers redeploying containers when new images
                    are pushed to its source ImageStream - defaults to False"
                  type: boolean
                docker_registry:
                  description: "Source Registry hosting Images - defaults to docker-registry
                    if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                    a registry would be deployed by the operator"
                  type: string
                ensure_dbs_has_guaranteed_qosclass:
                  description: "Sets Resources Requests & Limits such as Databases would always
                    use Kubernetes Guranteed QoS Class - defaults to True"
                  type: boolean
                execution_affinity_required:
                  description: Should Execution Affinity Rules be Mandatory - defaults to True
                  type: boolean
                hpa_cpu_target:
                  description: Default HPA CPU Target - defaults to 75
                  format: int64
                  type: integer
                hpa_max_replicas:
                  description: Default HPA Max Replicas - defaults to 4, or min_replicas + 1
                  format: int64
                  type: integer
                hpa_min_replicas:
                  description: Default HPA Min Replicas - defaults to 2
                  format: int64
                  type: integer
                hpa_memory_target:
                  description: Default HPA Memory Target - defaults to 80
                  format: int64
                  type: integer
                images_builds_namespace:
                  description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                  type: string
                ingress_filter_method:
                  description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                  type: string
                ingress_nodes_selector:
                  description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                  type: string
                k8s_affinity_zone_label:
                  description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                  type: string
                k8s_namespace_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/arch=amd64 ]
                  items:
                    type: string
                  type: array
                k8s_registry_ns:
                  description: Default Kubernetes integrated registry Namespace - defaults to registry
                  type: string
                k8s_registry_svc:
                  description: Default Kubernetes integrated registry Service Name - defaults to registry
                  type: string
                k8s_resource_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                  items:
                    type: string
                  type: array
                k8s_unprivileged_userid:
                  description: Default Kubernetes User ID running USER-less containers - defaults to 1000
                  format: int64
                  type: integer
                lool_admin_password:
                  description: Default LibreOfficeOnline Password - generated if not set
                  type: string
                lool_cpu_limit:
                  description: LibreOfficeOnline CPU Limit - defaults to 500m
                  type: string
                lool_cpu_request:
                  description: LibreOfficeOnline CPU Request - defaults to 50m
                  type: string
                lool_hpa_cpu_target:
                  description: LibreOfficeOnline HPA CPU Target - defaults to hpa_cpu_target
                  format: int64
                  type: integer
                lool_hpa_mem_target:
                  description: LibreOfficeOnline HPA Memory Target - defaults to hpa_mem_target
                  format: int64
                  type: integer
                lool_image_source:
                  description: LibreOfficeOnline Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-libreofficeonline
                  type: string
                lool_log_level:
                  description: "LibreOfficeOnline Log Level - defaults to information,
                    valid values: fatal, critical, error, warning, notice, information,
                    debug, trace"
                  type: string
                lool_max_replicas:
                  description: LibreOfficeOnline HPA Max Replicas - defaults to hpa_max_replicas
                  format: int64
                  type: integer
                lool_memory_limit:
                  description: LibreOfficeOnline Memory Limit - defaults to 1Gi
                  type: string
                lool_memory_request:
                  description: LibreOfficeOnline Memory Request - defaults to 768Mi
                  type: string
                lool_min_replicas:
                  description: LibreOfficeOnline HPA Min Replicas - defaults to lool_replicas
                  format: int64
                  type: integer
                lool_replicas:
                  description: LibreOfficeOnline Replicas Count - defaults to 1
                  format: int64
                  type: integer
                lool_tag:
                  description: LibreOfficeOnline Image Sources Ref - defaults to 20210919
                  type: string
                network_policy_deployment_label:
                  description: NetworkPolicy Pod Label Selector Name - defaults to name
                  type: string
                network_policy_namespace_label:
                  description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                    communications - defaults to netpol"
                  type: string
                no_proxy:
                  description: "HTTP Proxy Exclusions. Note cluster internal domain names, as well as
                    ingresses root domain would always be part of those exclusions - defaults to []"
                  items:
                    type: string
                  type: array
                opsperator_images_registry:
                  description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                    Kubernetes/OpenShift integrated registries"
                  type: string
                pause:
                  description: Pauses operator, stops reconciling child resources - defaults to False
                  type: boolean
                prometheus_match:
                  description: Prometheus Service Label Value - defaults to scrape-me
                  type: string
                prometheus_namespace:
                  description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                  type: string
                prometheus_namespace_match:
                  description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                  type: string
                prometheus_s3_label:
                  description: Prometheus S3 Exporter Service Label Name - defaults to k8s-s3
                  type: string
                prometheus_scrape_timeout:
                  description: Prometheus Scrape Timeout - defaults to 10s
                  type: string
                prometheus_service_label:
                  description: Prometheus Service Label Name - defaults to k8s-app
                  type: string
                pull_policy:
                  description: Images Pull Policy - defaults to IfNotPresent
                  type: string
                root_domain:
                  description: Customer Root Domain - defaults to demo.local
                  type: string
                scheduling_affinity_required:
                  description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                  type: boolean
                svc_keylen:
                  description: TLS Certificates Key Length - defaults to 2048
                  format: int64
                  type: integer
                svc_validity:
                  description: TLS Certificates Validity, in days - defaults to 365
                  format: int64
                  type: integer
                tekton_buildah_image:
                  description: Tekton Buildah Image - defaults to quay.io/buildah/stable:v1.21.0
                  type: string
                tekton_docker_build_cpu_limit:
                  description: Tekton Docker Build CPU Limit - defaults to 400m
                  type: string
                tekton_docker_build_cpu_request:
                  description: Tekton Docker Build CPU Request - defaults to 200m
                  type: string
                tekton_docker_build_memory_limit:
                  description: Tekton Docker Build Memory Limit - defaults to 4Gi
                  type: string
                tekton_docker_build_memory_request:
                  description: Tekton Docker Build Memory Request - defaults to 2Gi
                  type: string
                tekton_img_image:
                  description: Tekton GenuineTools Img Image - defaults to r.j3ss.co/img
                  type: string
                tekton_k8s_version:
                  description: Tekton Pipelines Version to Deploy (on Kubernetes, use OLM with OpenShift) - defaults to 0.19.0
                  type: string
                tekton_kaniko_image:
                  description: Tekton Kaniko Image - defaults to gcr.io/kaniko-project/executor:v1.7.0
                  type: string
                tekton_oc_client_image:
                  description: Tekton OC Client Image - defaults to quay.io/openshift/origin-cli:latest
                  type: string
                tekton_preferred_builder:
                  description: Tekton Preferred Docker Builder method - defaults to buildah
                  type: string
                tekton_s2i_build_cpu_limit:
                  description: Tekton S2I Build CPU Limit - defaults to 400m
                  type: string
                tekton_s2i_build_cpu_request:
                  description: Tekton S2I Build CPU Request - defaults to 200m
                  type: string
                tekton_s2i_build_memory_limit:
                  description: Tekton S2I Build Memory Limit - defaults to 4Gi
                  type: string
                tekton_s2i_build_memory_request:
                  description: Tekton S2I Build Memory Request - defaults to 2Gi
                  type: string
                tekton_s2i_image:
                  description: Tekton S2i Builder Image - defaults to quay.io/openshift-pipeline/s2i
                  type: string
                tekton_skopeo_image:
                  description: Tekton Skopeo Image - defaults to docker.io/ananace/skopeo
                  type: string
                timezone:
                  description: Customer TimeZone - defaults to Europe/Paris
                  type: string
                topology_spread_max_skew:
                  description: TopologySpreadConstraints MaxSkew - defaults to 1
                  format: int64
                  type: integer
                use_rwo_storage:
                  description: Forces RWO StorageClass name - defaults to undefined
                  type: string
                use_rwx_storage:
                  description: Forces RWX StorageClass name - defaults to undefined
                  type: string
              type: object
            status:
              properties:
                dashboards:
                  description: List of Grafana Dashboards for this deployment
                  items:
                    type: string
                  type: array
                internal:
                  description: LibreOfficeOnline Internal Address
                  type: string
                public:
                  description: LibreOfficeOnline Public Address
                  type: string
                secret:
                  description: LibreOfficeOnline Secret Name
                  type: string
                ready:
                  description: Marks Deployment to be Ready
                  type: boolean
              type: object
          type: object
      served: true
      storage: true
      subresources:
        status: {}
