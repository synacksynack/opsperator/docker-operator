- apiVersion: apiextensions.k8s.io/v1
  kind: CustomResourceDefinition
  metadata:
    name: minios.wopla.io
  spec:
    group: wopla.io
    names:
      kind: MinIO
      listKind: MinIOList
      plural: minios
      singular: minio
    scope: Namespaced
    versions:
    - name: v1beta1
      additionalPrinterColumns:
      - name: Internal
        type: string
        description: MinIO Internal
        jsonPath: .status.internal
      - name: Public
        type: string
        description: MinIO Public Address
        jsonPath: .status.public
      - name: Age
        type: date
        jsonPath: .metadata.creationTimestamp
      schema:
        openAPIV3Schema:
          description: Orchestrates the deployment of MinIO
          properties:
            apiVersion:
              description: 'APIVersion defines the versioned schema of this representation
                of an object. Servers should convert recognized schemas to the latest
                internal value, and may reject unrecognized values. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
              type: string
            kind:
              description: 'Kind is a string value representing the REST resource this
                object represents. Servers may infer this from the endpoint the client
                submits requests to. Cannot be updated. In CamelCase. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
              type: string
            spec:
              properties:
                auth_namespace:
                  description: Namespace hosting your Directory deployment, if any - defaults to MinIO namespace
                  type: string
                cluster_domain:
                  description: Kubernetes internal root domain - defaults to cluster.local
                  type: string
                console_cpu_limit:
                  description: MinIO Console CPU Limit - defaults to 100m
                  type: string
                console_cpu_request:
                  description: MinIO Console CPU Request - defaults to 10m
                  type: string
                console_memory_limit:
                  description: MinIO Console Memory Limit - defaults to 512Mi
                  type: string
                console_memory_request:
                  description: MinIO Console Memory Limit - defaults to 64Mi
                  type: string
                console_replicas:
                  description: MinIO Console Replicas Count - defaults to 1
                  format: int64
                  type: integer
                default_namespace_match:
                  description: NetworkPolicy Default Namespace Match label - defaults to default
                  type: string
                directory_selectors:
                  description: List of Label Selectors matching Directory CR to integrate with - defaults to [ opsperator.io/MinIO=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                do_affinities:
                  description: Enables AntiAffinity rules scheduling Pods - defaults to True
                  type: boolean
                do_debugs:
                  description: Toggles Debug on all applications - defaults to False
                  type: boolean
                do_exporters:
                  description: Toggles Prometheus Exporters deployment - defaults to False
                  type: boolean
                do_network_policy:
                  description: Enables NetworkPolicies configuration - defaults to False
                  type: boolean
                do_pdb:
                  description: Enables PodDisruptionBudgets configuration - defaults to False
                  type: boolean
                do_status_dashboards:
                  description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                    dashboards when Prometheus integration is enabled - defaults to False"
                  type: boolean
                do_topology_spread_constraints:
                  description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                  type: boolean
                do_triggers:
                  description: "Configures triggers redeploying containers when new images
                    are pushed to its source ImageStream - defaults to False"
                  type: boolean
                docker_registry:
                  description: "Source Registry hosting Images - defaults to docker-registry
                    if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                    a registry would be deployed by the operator"
                  type: string
                ensure_dbs_has_guaranteed_qosclass:
                  description: "Sets Resources Requests & Limits such as Databases would always
                    use Kubernetes Guranteed QoS Class - defaults to True"
                  type: boolean
                execution_affinity_required:
                  description: Should Execution Affinity Rules be Mandatory - defaults to True
                  type: boolean
                expose_minio:
                  description: Exposes MinIO through Ingresses - defaults to False
                  type: boolean
                images_builds_namespace:
                  description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                  type: string
                ingress_filter_method:
                  description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                  type: string
                ingress_nodes_selector:
                  description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                  type: string
                k8s_affinity_zone_label:
                  description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                  type: string
                k8s_namespace_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/os=linux ]
                  items:
                    type: string
                  type: array
                k8s_registry_ns:
                  description: Default Kubernetes integrated registry Namespace - defaults to registry
                  type: string
                k8s_registry_svc:
                  description: Default Kubernetes integrated registry Service Name - defaults to registry
                  type: string
                k8s_resource_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                  items:
                    type: string
                  type: array
                k8s_unprivileged_userid:
                  description: Default Kubernetes User ID running USER-less containers - defaults to 1000
                  format: int64
                  type: integer
                network_policy_deployment_label:
                  description: NetworkPolicy Pod Label Selector Name - defaults to name
                  type: string
                network_policy_namespace_label:
                  description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                    communications - defaults to netpol"
                  type: string
                minio_console_image_source:
                  description: MinIO Console source image - defaults to docker.io/minio/console
                  type: string
                minio_console_tag:
                  description: MinIO Console Image Tag - defaults to v0.20.5
                  type: string
                minio_cpu_limit:
                  description: MinIO CPU Limit - defaults to 600m
                  type: string
                minio_cpu_request:
                  description: MinIO CPU Request - defaults to 50m
                  type: string
                minio_data_capacity:
                  description: MinIO PersistentVolumeClaim requested size - defaults to 32Gi
                  type: string
                minio_do_browser:
                  description: Toggles MinIO web based objects browser - defaults to False
                  type: boolean
                minio_do_console:
                  description: Toggles MinIO Console deployment - defaults to False
                  type: boolean
                minio_drives_per_replica:
                  description: How many drives should be provisioned, for each MinIO Pod - defaults to 4
                  format: int64
                  type: integer
                minio_image_source:
                  description: MinIO source image - defaults to docker.io/minio/minio
                  type: string
                minio_memory_limit:
                  description: MinIO Memory Limit - defaults to 3Gi
                  type: string
                minio_memory_request:
                  description: MinIO Memory Limit - defaults to 2Gi
                  type: string
                minio_pvc_single:
                  description: "When True, a single PVC would be allocated for each MinIO Pod,
                    minio_drives_per_replica is implemented using subPaths of the same PVC.
                    which is not recommended IRL, though could be useful coping with slow
                    provisioners, debugging, poc, lab, ... pretty much everything but prod -
                    defaults to False"
                  type: boolean
                minio_ready_deadline_seconds:
                  description: MinIO Ready Deadline Seconds - defaults to 5
                  format: int64
                  type: integer
                minio_region:
                  description: MinIO s3 Region - defaults to us-east-1
                  type: string
                minio_replicas:
                  description: MinIO Replicas Count - defaults to 3
                  format: int64
                  type: integer
                minio_tag:
                  description: MinIO Image Tag - defaults to RELEASE.2022-09-25T15-44-53Z
                  type: string
                opsperator_images_registry:
                  description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                    Kubernetes/OpenShift integrated registries"
                  type: string
                pause:
                  description: Pauses operator, stops reconciling child resources - defaults to False
                  type: boolean
                prometheus_match:
                  description: Prometheus Service Label Value - defaults to scrape-me
                  type: string
                prometheus_namespace:
                  description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                  type: string
                prometheus_namespace_match:
                  description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                  type: string
                prometheus_service_label:
                  description: Prometheus Service Label Name - defaults to k8s-app
                  type: string
                pull_policy:
                  description: Images Pull Policy - defaults to IfNotPresent
                  type: string
                root_domain:
                  description: Customer Root Domain - defaults to demo.local
                  type: string
                scheduling_affinity_required:
                  description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                  type: boolean
                svc_keylen:
                  description: TLS Certificates Key Length - defaults to 2048
                  type: integer
                svc_validity:
                  description: TLS Certificates Validity, in days - defaults to 365
                  type: integer
                timezone:
                  description: Customer TimeZone - defaults to Europe/Paris
                  type: string
                topology_spread_max_skew:
                  description: TopologySpreadConstraints MaxSkew - defaults to 1
                  format: int64
                  type: integer
                use_rwo_storage:
                  description: Forces RWO StorageClass name - defaults to undefined
                  type: string
              type: object
            status:
              properties:
                adminsecret:
                  description: MinIO Admin User Secret
                  type: string
                console:
                  description: MinIO Console Public Address - if minio_do_console set to True
                  type: string
                dashboards:
                  description: List of Grafana Dashboards for this deployment
                  items:
                    type: string
                  type: array
                intconsole:
                  description: MinIO Console Internal Address - if minio_do_console set to True
                  type: string
                internal:
                  description: MinIO Internal Address
                  type: string
                mbhelpers:
                  description: MinIO MakeBucket Helpers ConfigMap
                  type: string
                public:
                  description: MinIO Public Address - if expose_minio set to True
                  type: string
                ready:
                  description: Marks Deployment to be Ready
                  type: boolean
              type: object
          type: object
      served: true
      storage: true
      subresources:
        status: {}
