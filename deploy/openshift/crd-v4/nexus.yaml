- apiVersion: apiextensions.k8s.io/v1
  kind: CustomResourceDefinition
  metadata:
    name: nexuses.wopla.io
  spec:
    group: wopla.io
    names:
      kind: Nexus
      listKind: NexusList
      plural: nexuses
      shortNames:
      - nrm
      - nx
      singular: nexus
    scope: Namespaced
    versions:
    - name: v1beta1
      additionalPrinterColumns:
      - name: Domain
        type: string
        description: Nexus Domain
        jsonPath: .status.public
      - name: Age
        type: date
        jsonPath: .metadata.creationTimestamp
      schema:
        openAPIV3Schema:
          description: Orchestrates the deployment of Nexus
          properties:
            apiVersion:
              description: 'APIVersion defines the versioned schema of this representation
                of an object. Servers should convert recognized schemas to the latest
                internal value, and may reject unrecognized values. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
              type: string
            kind:
              description: 'Kind is a string value representing the REST resource this
                object represents. Servers may infer this from the endpoint the client
                submits requests to. Cannot be updated. In CamelCase. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
              type: string
            spec:
              properties:
                auth_namespace:
                  description: Namespace hosting your Directory deployment, if any - defaults to Nexus namespace
                  type: string
                cluster_domain:
                  description: Kubernetes internal root domain - defaults to cluster.local
                  type: string
                default_namespace_match:
                  description: NetworkPolicy Default Namespace Match label - defaults to default
                  type: string
                deploy_pod_cpu_limit:
                  description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                  type: string
                deploy_pod_cpu_request:
                  description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                  type: string
                deploy_pod_memory_limit:
                  description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                  type: string
                deploy_pod_memory_request:
                  description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                  type: string
                deploy_revision_history:
                  description: DeploymentConfigs Revision History Limit - defaults to 3
                  type: string
                directory_selectors:
                  description: List of Label Selectors matching Directory CR to integrate with - defaults to [ opsperator.io/Nexus=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                do_affinities:
                  description: Enables AntiAffinity rules scheduling Pods - defaults to True
                  type: boolean
                do_build_trigger:
                  description: Enables BuildConfig/Tekton Triggers - defaults to True
                  type: boolean
                do_debugs:
                  description: Toggles Debug on all applications - defaults to False
                  type: boolean
                do_exporters:
                  description: Toggles Prometheus Exporters deployment - defaults to False
                  type: boolean
                do_network_policy:
                  description: Enables NetworkPolicies configuration - defaults to False
                  type: boolean
                do_s3:
                  description: Enables Nexus s3 storage integration - defaults to False
                  type: boolean
                do_status_dashboards:
                  description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                    dashboards when Prometheus integration is enabled - defaults to False"
                  type: boolean
                do_topology_spread_constraints:
                  description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                  type: boolean
                do_triggers:
                  description: "Configures triggers redeploying containers when new images
                    are pushed to its source ImageStream - defaults to False"
                  type: boolean
                docker_registry:
                  description: "Source Registry hosting Images - defaults to docker-registry
                    if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                    a registry would be deployed by the operator"
                  type: string
                execution_affinity_required:
                  description: Should Execution Affinity Rules be Mandatory - defaults to True
                  type: boolean
                exporter_cpu_limit:
                  description: Prometheus Exporter container CPU Limit - defaults to 100m
                  type: string
                exporter_cpu_request:
                  description: Prometheus Exporter container CPU Request - defaults to 50m
                  type: string
                exporter_memory_limit:
                  description: Prometheus Exporter container Memory Limit - defaults to 128Mi
                  type: string
                exporter_memory_request:
                  description: Prometheus Exporter container Memory Request - defaults to 64Mi
                  type: string
                images_builds_namespace:
                  description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                  type: string
                ingress_filter_method:
                  description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                  type: string
                ingress_nodes_selector:
                  description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                  type: string
                k8s_affinity_zone_label:
                  description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                  type: string
                k8s_namespace_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/os=linux ]
                  items:
                    type: string
                  type: array
                k8s_registry_ns:
                  description: Default Kubernetes integrated registry Namespace - defaults to registry
                  type: string
                k8s_registry_svc:
                  description: Default Kubernetes integrated registry Service Name - defaults to registry
                  type: string
                k8s_resource_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                  items:
                    type: string
                  type: array
                minio_namespace:
                  description: Namespaces hosting MinIO - defaults to Nexus namespace
                  type: string
                network_policy_deployment_label:
                  description: NetworkPolicy Pod Label Selector Name - defaults to name
                  type: string
                network_policy_namespace_label:
                  description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                    communications - defaults to netpol"
                  type: string
                nexus_admin_password:
                  description: Nexus admin account password - defaults to secret
                  type: string
                nexus_blobstores:
                  description: List of Nexus Blob Store names to provision - defaults to [ ]
                  items:
                    type: string
                  type: array
                nexus_clone_secret:
                  description: Nexus Clone Secret, assuming private upstream - defaults to undefined, should match an items name in the clone_tokens array
                  type: string
                nexus_cpu_limit:
                  description: Nexus CPU Limit - defaults to 800m
                  type: string
                nexus_cpu_request:
                  description: Nexus CPU Request - defaults to 10m
                  type: string
                nexus_data_capacity:
                  description: Nexus PersistentVolumeClaim requested size - defaults to 50Gi
                  type: string
                nexus_image_source:
                  description: Nexus sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-nexus
                  type: string
                nexus_java_args:
                  description: List of Arguments to pass Nexus JVM - defaults to Xmx/Xms/MaxDirectMemorySize=2703m
                  items:
                    type: string
                  type: array
                nexus_ldap_user_objectclass:
                  description: Nexus LDAP Users ObjectClass, integrating with LDAP with no Fusion, Wsweet nor LLNG - defaults to inetOrgPerson
                  type: string
                nexus_memory_limit:
                  description: Nexus Memory Limit - defaults to 4200Mi
                  type: string
                nexus_memory_request:
                  description: Nexus Memory Request - defaults to 2800Mi
                  type: string
                nexus_replicas:
                  description: Nexus Replicas Count - defaults to 1
                  format: int64
                  type: integer
                nexus_repositories:
                  description: List of Nexus Repositories to provision - defaults to [ ]
                  items:
                    type: string
                  type: array
                nexus_s3_access_key:
                  description: "Nexus s3 Access Key - defaults to undefined, mandatory
                    integrating with RadosGW-backed s3, would be provisioned automatically when
                    using MinIO."
                  type: string
                nexus_s3_bucket:
                  description: Nexus s3 bucket name - defaults to nexus
                  type: string
                nexus_s3_flavor:
                  description: Nexus s3 flavor - defaults to minio, can switch to ceph/radosgw
                  type: string
                nexus_s3_region:
                  description: Nexus s3 region, unless using MinIO - defaults to us-east-1
                  type: string
                nexus_s3_secret_key:
                  description: "Nexus s3 Secret Key - defaults to undefined, mandatory
                    integrating with RadosGW-backed s3, would be provisioned automatically when
                    using MinIO."
                  type: string
                nexus_tag:
                  description: Nexus Image Tag - defaults to 3.41.1-01
                  type: string
                nexusexporter_image_source:
                  description: Nexus Exporter sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-nexusexporter
                  type: string
                nexusexporter_tag:
                  description: Nexus Exporter Image Tag - defaults to 0.2.3
                  type: string
                no_proxy:
                  description: "HTTP Proxy Exclusions. Note cluster internal domain names, as well as
                    ingresses root domain would always be part of those exclusions - defaults to []"
                  items:
                    type: string
                  type: array
                opsperator_images_registry:
                  description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                    Kubernetes/OpenShift integrated registries"
                  type: string
                pause:
                  description: Pauses operator, stops reconciling child resources - defaults to False
                  type: boolean
                prometheus_match:
                  description: Prometheus Service Label Value - defaults to scrape-me
                  type: string
                prometheus_namespace:
                  description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                  type: string
                prometheus_namespace_match:
                  description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                  type: string
                prometheus_s3_label:
                  description: Prometheus S3 Exporter Service Label Name - defaults to k8s-s3
                  type: string
                prometheus_service_label:
                  description: Prometheus Service Label Name - defaults to k8s-app
                  type: string
                proxy_host:
                  description: HTTP Proxy Host - defaults to undefined
                  type: string
                proxy_port:
                  description: HTTP Proxy Host - defaults to 3128
                  format: int64
                  type: integer
                pull_policy:
                  description: Images Pull Policy - defaults to IfNotPresent
                  type: string
                rgw_make_bucket:
                  description: Provisions Bucket when using s3 & RadosGW - defaults to True
                  type: boolean
                rgw_namespace:
                  description: Namespaces hosting RadosGW - defaults to Nexus namespace
                  type: string
                root_domain:
                  description: Customer Root Domain - defaults to demo.local
                  type: string
                rwo_needs_fsgroup:
                  description: Optional securityContext.fsGroup numeric ID, to force on containers mounting RWO storage - defaults to False
                  format: int64
                  type: integer
                s3exporter_cpu_limit:
                  description: Prometheus s3 Exporter container CPU Limit - defaults to 200m
                  type: string
                s3exporter_cpu_request:
                  description: Prometheus s3 Exporter container CPU Request - defaults to 100m
                  type: string
                s3exporter_image_source:
                  description: Prometheus s3 Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-s3bucketexporter
                  type: string
                s3exporter_memory_limit:
                  description: Prometheus s3 Exporter container Memory Limit - defaults to 768Mi
                  type: string
                s3exporter_memory_request:
                  description: Prometheus s3 Exporter container Memory Request - defaults to 256Mi
                  type: string
                s3exporter_tag:
                  description: Prometheus s3 Exporter Image Sources Ref - defaults to 1.0.0
                  type: string
                scheduling_affinity_required:
                  description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                  type: boolean
                squid_namespace:
                  description: Namespace hosting your Squid deployment, if any - defaults to Nexus namespace
                  type: string
                squid_selectors:
                  description: List of Label Selectors matching Squid CR to integrate with - defaults to [ opsperator.io/Nexus=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                svc_keylen:
                  description: TLS Certificates Key Length - defaults to 2048
                  format: int64
                  type: integer
                svc_validity:
                  description: TLS Certificates Validity, in days - defaults to 365
                  format: int64
                  type: integer
                tekton_buildah_image:
                  description: Tekton Buildah Image - defaults to quay.io/buildah/stable:v1.21.0
                  type: string
                tekton_docker_build_cpu_limit:
                  description: Tekton Docker Build CPU Limit - defaults to 400m
                  type: string
                tekton_docker_build_cpu_request:
                  description: Tekton Docker Build CPU Request - defaults to 200m
                  type: string
                tekton_docker_build_memory_limit:
                  description: Tekton Docker Build Memory Limit - defaults to 4Gi
                  type: string
                tekton_docker_build_memory_request:
                  description: Tekton Docker Build Memory Request - defaults to 2Gi
                  type: string
                tekton_img_image:
                  description: Tekton GenuineTools Img Image - defaults to r.j3ss.co/img
                  type: string
                tekton_k8s_version:
                  description: Tekton Pipelines Version to Deploy (on Kubernetes, use OLM with OpenShift) - defaults to 0.12.1
                  type: string
                tekton_kaniko_image:
                  description: Tekton Kaniko Image - defaults to gcr.io/kaniko-project/executor:v1.7.0
                  type: string
                tekton_oc_client_image:
                  description: Tekton OC Client Image - defaults to quay.io/openshift/origin-cli:latest
                  type: string
                tekton_preferred_builder:
                  description: Tekton Preferred Docker Builder method - defaults to buildah
                  type: string
                tekton_s2i_build_cpu_limit:
                  description: Tekton S2I Build CPU Limit - defaults to 400m
                  type: string
                tekton_s2i_build_cpu_request:
                  description: Tekton S2I Build CPU Request - defaults to 200m
                  type: string
                tekton_s2i_build_memory_limit:
                  description: Tekton S2I Build Memory Limit - defaults to 4Gi
                  type: string
                tekton_s2i_build_memory_request:
                  description: Tekton S2I Build Memory Request - defaults to 2Gi
                  type: string
                tekton_s2i_image:
                  description: Tekton S2i Builder Image - defaults to quay.io/openshift-pipeline/s2i
                  type: string
                tekton_skopeo_image:
                  description: Tekton Skopeo Image - defaults to docker.io/ananace/skopeo
                  type: string
                timezone:
                  description: Customer TimeZone - defaults to Europe/Paris
                  type: string
                topology_spread_max_skew:
                  description: TopologySpreadConstraints MaxSkew - defaults to 1
                  format: int64
                  type: integer
                use_rwo_storage:
                  description: Forces RWO StorageClass name - defaults to undefined
                  type: string
              type: object
            status:
              properties:
                adminsecret:
                  description: Nexus Admin User Secret
                  type: string
                artifactssecret:
                  description: Nexus Artifacts User Secret
                  type: string
                dashboards:
                  description: List of Grafana Dashboards for this deployment
                  items:
                    type: string
                  type: array
                deployersecret:
                  description: Nexus Deployer User Secret
                  type: string
                internal:
                  description: Nexus Internal Address
                  type: string
                public:
                  description: Nexus Public Address
                  type: string
                ready:
                  description: Marks Deployment to be Ready
                  type: boolean
              type: object
          type: object
      served: true
      storage: true
      subresources:
        status: {}
