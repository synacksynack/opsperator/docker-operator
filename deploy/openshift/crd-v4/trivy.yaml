- apiVersion: apiextensions.k8s.io/v1
  kind: CustomResourceDefinition
  metadata:
    name: trivies.wopla.io
  spec:
    group: wopla.io
    names:
      kind: Trivy
      listKind: TrivyList
      plural: trivies
      singular: trivy
    scope: Namespaced
    versions:
    - name: v1beta1
      additionalPrinterColumns:
      - name: Internal
        type: string
        description: Trivy Internal
        jsonPath: .status.internal
      - name: Public
        type: string
        description: Trivy Public Address
        jsonPath: .status.public
      - name: Age
        type: date
        jsonPath: .metadata.creationTimestamp
      schema:
        openAPIV3Schema:
          description: Orchestrates the deployment of Trivy
          properties:
            apiVersion:
              description: 'APIVersion defines the versioned schema of this representation
                of an object. Servers should convert recognized schemas to the latest
                internal value, and may reject unrecognized values. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
              type: string
            kind:
              description: 'Kind is a string value representing the REST resource this
                object represents. Servers may infer this from the endpoint the client
                submits requests to. Cannot be updated. In CamelCase. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
              type: string
            spec:
              properties:
                cluster_domain:
                  description: Kubernetes internal root domain - defaults to cluster.local
                  type: string
                default_namespace_match:
                  description: NetworkPolicy Default Namespace Match label - defaults to default
                  type: string
                deploy_pod_cpu_limit:
                  description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                  type: string
                deploy_pod_cpu_request:
                  description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                  type: string
                deploy_pod_memory_limit:
                  description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                  type: string
                deploy_pod_memory_request:
                  description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                  type: string
                deploy_revision_history:
                  description: DeploymentConfigs Revision History Limit - defaults to 3
                  type: string
                do_affinities:
                  description: Enables AntiAffinity rules scheduling Pods - defaults to True
                  type: boolean
                do_build_trigger:
                  description: Enables BuildConfig/Tekton Triggers - defaults to True
                  type: boolean
                do_debugs:
                  description: Toggles Debug on all applications - defaults to False
                  type: boolean
                do_exporters:
                  description: Toggles Prometheus Exporters deployment - defaults to False
                  type: boolean
                do_hpa:
                  description: Enables Horizontal Pods Autoscalers configuration - defaults to False
                  type: boolean
                do_network_policy:
                  description: Enables NetworkPolicies configuration - defaults to False
                  type: boolean
                do_pdb:
                  description: Enables PodDisruptionBudgets configuration - defaults to False
                  type: boolean
                do_status_dashboards:
                  description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                    dashboards when Prometheus integration is enabled - defaults to False"
                  type: boolean
                do_topology_spread_constraints:
                  description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                  type: boolean
                do_triggers:
                  description: "Configures triggers redeploying containers when new images
                    are pushed to its source ImageStream - defaults to False"
                  type: boolean
                docker_registry:
                  description: "Source Registry hosting Images - defaults to docker-registry
                    if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                    a registry would be deployed by the operator"
                  type: string
                ensure_dbs_has_guaranteed_qosclass:
                  description: "Sets Resources Requests & Limits such as Databases would always
                    use Kubernetes Guranteed QoS Class - defaults to True"
                  type: boolean
                execution_affinity_required:
                  description: Should Execution Affinity Rules be Mandatory - defaults to True
                  type: boolean
                exporter_cpu_limit:
                  description: Prometheus Exporter container CPU Limit - defaults to 100m
                  type: string
                exporter_cpu_request:
                  description: Prometheus Exporter container CPU Request - defaults to 50m
                  type: string
                exporter_memory_limit:
                  description: Prometheus Exporter container Memory Limit - defaults to 128Mi
                  type: string
                exporter_memory_request:
                  description: Prometheus Exporter container Memory Request - defaults to 64Mi
                  type: string
                expose_trivy:
                  description: Exposes Trivy through Ingresses - defaults to False
                  type: boolean
                haproxy_cpu_limit:
                  description: Redis Proxy CPU Limit - defaults to 100m
                  type: string
                haproxy_cpu_request:
                  description: Redis Proxy CPU Request - defaults to 50m
                  type: string
                haproxy_memory_limit:
                  description: Redis Proxy Memory Limit - defaults to 256Mi
                  type: string
                haproxy_memory_request:
                  description: Redis Proxy Memory Request - defaults to 128Mi
                  type: string
                haproxy_replicas:
                  description: Redis Proxy Replicas - defaults to 2
                  format: int64
                  type: integer
                hpa_cpu_target:
                  description: Default HPA CPU Target - defaults to 75
                  format: int64
                  type: integer
                hpa_max_replicas:
                  description: Default HPA Max Replicas - defaults to 4, or min_replicas + 1
                  format: int64
                  type: integer
                hpa_min_replicas:
                  description: Default HPA Min Replicas - defaults to 2
                  format: int64
                  type: integer
                hpa_memory_target:
                  description: Default HPA Memory Target - defaults to 80
                  format: int64
                  type: integer
                images_builds_namespace:
                  description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                  type: string
                ingress_filter_method:
                  description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                  type: string
                ingress_nodes_selector:
                  description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                  type: string
                k8s_affinity_zone_label:
                  description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                  type: string
                k8s_namespace_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/os=linux ]
                  items:
                    type: string
                  type: array
                k8s_registry_ns:
                  description: Default Kubernetes integrated registry Namespace - defaults to registry
                  type: string
                k8s_registry_svc:
                  description: Default Kubernetes integrated registry Service Name - defaults to registry
                  type: string
                k8s_resource_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                  items:
                    type: string
                  type: array
                k8s_unprivileged_userid:
                  description: Default Kubernetes User ID running USER-less containers - defaults to 1000
                  format: int64
                  type: integer
                network_policy_deployment_label:
                  description: NetworkPolicy Pod Label Selector Name - defaults to name
                  type: string
                network_policy_namespace_label:
                  description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                    communications - defaults to netpol"
                  type: string
                no_proxy:
                  description: "HTTP Proxy Exclusions. Note cluster internal domain names, as well as
                    ingresses root domain would always be part of those exclusions - defaults to []"
                  items:
                    type: string
                  type: array
                opsperator_images_registry:
                  description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                    Kubernetes/OpenShift integrated registries"
                  type: string
                pause:
                  description: Pauses operator, stops reconciling child resources - defaults to False
                  type: boolean
                prometheus_match:
                  description: Prometheus Service Label Value - defaults to scrape-me
                  type: string
                prometheus_namespace:
                  description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                  type: string
                prometheus_namespace_match:
                  description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                  type: string
                prometheus_service_label:
                  description: Prometheus Service Label Name - defaults to k8s-app
                  type: string
                pull_policy:
                  description: Images Pull Policy - defaults to IfNotPresent
                  type: string
                proxy_host:
                  description: HTTP Proxy Host - defaults to undefined
                  type: string
                proxy_port:
                  description: HTTP Proxy Host - defaults to 3128
                  format: int64
                  type: integer
                redis_clone_secret:
                  description: Redis Base Image Sources Clone Secret - defaults to undefined
                  type: string
                redis_cpu_limit:
                  description: Redis Cache CPU Limit - defaults to 100m
                  type: string
                redis_data_capacity:
                  description: Redis PersistentVolumeClaim requested size - defaults to 8Gi
                  type: string
                redis_image_source:
                  description: Redis Base Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-redis
                  type: string
                redis_memory_limit:
                  description: Redis Cache Memory Limit - defaults to 256Mi
                  type: string
                redis_persistent:
                  description: Persist Redis Data - defaults to False
                  type: boolean
                redis_quorum:
                  description: Redis Quorum - defaults to 2, unless replicas lower than 3, then 1
                  format: int64
                  type: integer
                redis_replicas:
                  description: Redis Replicas Count - defaults to 3
                  format: int64
                  type: integer
                redis_tag:
                  description: Redis Base Image Tag - defaults to 6.2.5
                  type: string
                redisexporter_clone_secret:
                  description: Redis Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                  type: string
                redisexporter_image_source:
                  description: Redis Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-redisexporter
                  type: string
                redisexporter_tag:
                  description: Redis Prometheus Exporter Image Tag - defaults to 1.27.0
                  type: string
                root_domain:
                  description: Customer Root Domain - defaults to demo.local
                  type: string
                scheduling_affinity_required:
                  description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                  type: boolean
                squid_namespace:
                  description: Namespace hosting your Squid deployment, if any - defaults to Trivy namespace
                  type: string
                squid_selectors:
                  description: List of Label Selectors matching Squid CR to integrate with - defaults to [ opsperator.io/Trivy=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                svc_keylen:
                  description: TLS Certificates Key Length - defaults to 2048
                  format: int64
                  type: integer
                svc_validity:
                  description: TLS Certificates Validity, in days - defaults to 365
                  format: int64
                  type: integer
                timezone:
                  description: Customer TimeZone - defaults to Europe/Paris
                  type: string
                topology_spread_max_skew:
                  description: TopologySpreadConstraints MaxSkew - defaults to 1
                  format: int64
                  type: integer
                trivy_cache:
                  description: Trivy Server Cache Method - defaults to fs, redis experimental
                  type: string
                trivy_cpu_limit:
                  description: Trivy Server CPU Limit - defaults to 100m
                  type: string
                trivy_cpu_request:
                  description: Trivy Server CPU Request - defaults to 10m
                  type: string
                trivy_data_capacity:
                  description: Trivy Server Cache PersistentVolumeClaim requested size - defaults to 32Gi
                  type: string
                trivy_image_source:
                  description: Trivy Server source image - defaults to docker.io/aquasec/trivy
                  type: string
                trivy_max_replicas:
                  description: Trivy Server HPA Max Replicas - defaults to hpa_max_replicas
                  format: int64
                  type: integer
                trivy_memory_limit:
                  description: Trivy Server Memory Limit - defaults to 1Gi
                  type: string
                trivy_memory_request:
                  description: Trivy Server Memory Limit - defaults to 128Mi
                  type: string
                trivy_min_replicas:
                  description: Trivy Server HPA Min Replicas - defaults to trivy_replicas
                  format: int64
                  type: integer
                trivy_replicas:
                  description: Trivy Server Replicas Count - defaults to 1
                  format: int64
                  type: integer
                trivy_tag:
                  description: Trivy Server Image Tag - defaults to 0.28.0
                  type: string
                use_rwo_storage:
                  description: Forces RWO StorageClass name - defaults to undefined
                  type: string
              type: object
            status:
              properties:
                dashboards:
                  description: List of Grafana Dashboards for this deployment
                  items:
                    type: string
                  type: array
                internal:
                  description: Trivy Internal Address
                  type: string
                public:
                  description: Trivy Public Address - if expose_trivy set to True
                  type: string
                ready:
                  description: Marks Deployment to be Ready
                  type: boolean
              type: object
          type: object
      served: true
      storage: true
      subresources:
        status: {}
