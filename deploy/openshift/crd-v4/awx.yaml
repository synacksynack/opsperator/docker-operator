- apiVersion: apiextensions.k8s.io/v1
  kind: CustomResourceDefinition
  metadata:
    name: awxes.wopla.io
  spec:
    group: wopla.io
    names:
      kind: AWX
      listKind: AWXList
      plural: awxes
      singular: awx
    scope: Namespaced
    versions:
    - name: v1beta1
      additionalPrinterColumns:
      - name: Internal
        type: string
        description: AWX Internal
        jsonPath: .status.internal
      - name: Public
        type: string
        description: AWX Public Address
        jsonPath: .status.public
      - name: Age
        type: date
        jsonPath: .metadata.creationTimestamp
      schema:
        openAPIV3Schema:
          description: Orchestrates the deployment of AWX
          properties:
            apiVersion:
              description: 'APIVersion defines the versioned schema of this representation
                of an object. Servers should convert recognized schemas to the latest
                internal value, and may reject unrecognized values. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
              type: string
            kind:
              description: 'Kind is a string value representing the REST resource this
                object represents. Servers may infer this from the endpoint the client
                submits requests to. Cannot be updated. In CamelCase. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
              type: string
            spec:
              properties:
                allowed_unsafe_sysctls:
                  description: List of Unsafe Sysctls that were administratively enabled in your cluster, and which may then be used - defaults to [ ]
                  items:
                    type: string
                  type: array
                awx_db_cpu_limit:
                  description: Ansible AWX Database CPU Limit - defaults to 300m
                  type: string
                awx_db_data_capacity:
                  description: Ansible AWX Database PersistentVolumeClaim requested size - defaults to 8Gi
                  type: string
                awx_db_memory_limit:
                  description: Ansible AWX Database Memory Limit - defaults to 512Mi
                  type: string
                awx_image_source:
                  description: Ansible AWX source image - defaults to quay.io/ansible/awx
                  type: string
                awx_init_cpu_limit:
                  description: Ansible AWX Init CPU Limit - defaults to 500m
                  type: string
                awx_init_cpu_request:
                  description: Ansible AWX Init CPU Request - defaults to 50m
                  type: string
                awx_init_memory_limit:
                  description: Ansible AWX Init Memory Limit - defaults to 1500Mi
                  type: string
                awx_init_memory_request:
                  description: Ansible AWX Init Memory Limit - defaults to 768Mi
                  type: string
                awx_replicas:
                  description: Ansible AWX Replicas Count - defaults to 1
                  format: int64
                  type: integer
                awx_runner_image_source:
                  description: Ansible Runner source image - defaults to docker.io/ansible/ansible-runner
                  type: string
                awx_runner_tag:
                  description: Ansible Runner Image Tag - defaults to stable-2.11-devel
                  type: string
                awx_tag:
                  description: Ansible AWX Image Tag - defaults to 19.4.0
                  type: string
                awx_tasks_cpu_limit:
                  description: Ansible AWX Tasks CPU Limit - defaults to 2000m
                  type: string
                awx_tasks_cpu_request:
                  description: Ansible AWX Tasks CPU Request - defaults to 1000m
                  type: string
                awx_tasks_memory_limit:
                  description: Ansible AWX Tasks Memory Limit - defaults to 2500Mi
                  type: string
                awx_tasks_memory_request:
                  description: Ansible AWX Tasks Memory Limit - defaults to 1500Mi
                  type: string
                awx_tasks_sys_abs_cpu:
                  description: Ansible AWX Tasks Abs CPU - defaults to 4
                  format: int64
                  type: integer
                awx_tasks_sys_abs_mem:
                  description: Ansible AWX Tasks Abs Memory - defaults to 15
                  format: int64
                  type: integer
                awx_web_cpu_limit:
                  description: Ansible AWX Web CPU Limit - defaults to 800m
                  type: string
                awx_web_cpu_request:
                  description: Ansible AWX Web CPU Request - defaults to 50m
                  type: string
                awx_web_memory_limit:
                  description: Ansible AWX Web Memory Limit - defaults to 1280Mi
                  type: string
                awx_web_memory_request:
                  description: Ansible AWX Web Memory Limit - defaults to 768Mi
                  type: string
                cluster_domain:
                  description: Kubernetes internal root domain - defaults to cluster.local
                  type: string
                default_namespace_match:
                  description: NetworkPolicy Default Namespace Match label - defaults to default
                  type: string
                deploy_pod_cpu_limit:
                  description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                  type: string
                deploy_pod_cpu_request:
                  description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                  type: string
                deploy_pod_memory_limit:
                  description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                  type: string
                deploy_pod_memory_request:
                  description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                  type: string
                deploy_revision_history:
                  description: DeploymentConfigs Revision History Limit - defaults to 3
                  type: string
                do_affinities:
                  description: Enables AntiAffinity rules scheduling Pods - defaults to True
                  type: boolean
                do_build_trigger:
                  description: Enables BuildConfig/Tekton Triggers - defaults to True
                  type: boolean
                do_debugs:
                  description: Toggles Debug on all applications - defaults to False
                  type: boolean
                do_exporters:
                  description: Toggles Prometheus Exporters deployment - defaults to False
                  type: boolean
                do_network_policy:
                  description: Enables NetworkPolicies configuration - defaults to False
                  type: boolean
                do_status_dashboards:
                  description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                    dashboards when Prometheus integration is enabled - defaults to False"
                  type: boolean
                do_topology_spread_constraints:
                  description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                  type: boolean
                do_triggers:
                  description: "Configures triggers redeploying containers when new images
                    are pushed to its source ImageStream - defaults to False"
                  type: boolean
                docker_registry:
                  description: "Source Registry hosting Images - defaults to docker-registry
                    if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                    a registry would be deployed by the operator"
                  type: string
                ensure_dbs_has_guaranteed_qosclass:
                  description: "Sets Resources Requests & Limits such as Databases would always
                    use Kubernetes Guranteed QoS Class - defaults to True"
                  type: boolean
                execution_affinity_required:
                  description: Should Execution Affinity Rules be Mandatory - defaults to True
                  type: boolean
                exporter_cpu_limit:
                  description: Prometheus Exporter container CPU Limit - defaults to 100m
                  type: string
                exporter_cpu_request:
                  description: Prometheus Exporter container CPU Request - defaults to 50m
                  type: string
                exporter_memory_limit:
                  description: Prometheus Exporter container Memory Limit - defaults to 128Mi
                  type: string
                exporter_memory_request:
                  description: Prometheus Exporter container Memory Request - defaults to 64Mi
                  type: string
                images_builds_namespace:
                  description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                  type: string
                ingress_filter_method:
                  description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                  type: string
                ingress_nodes_selector:
                  description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                  type: string
                k8s_affinity_zone_label:
                  description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                  type: string
                k8s_namespace_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/arch=amd64 ]
                  items:
                    type: string
                  type: array
                k8s_registry_ns:
                  description: Default Kubernetes integrated registry Namespace - defaults to registry
                  type: string
                k8s_registry_svc:
                  description: Default Kubernetes integrated registry Service Name - defaults to registry
                  type: string
                k8s_resource_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                  items:
                    type: string
                  type: array
                k8s_unprivileged_userid:
                  description: Default Kubernetes User ID running USER-less containers - defaults to 1000
                  format: int64
                  type: integer
                network_policy_deployment_label:
                  description: NetworkPolicy Pod Label Selector Name - defaults to name
                  type: string
                network_policy_namespace_label:
                  description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                    communications - defaults to netpol"
                  type: string
                opsperator_images_registry:
                  description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                    Kubernetes/OpenShift integrated registries"
                  type: string
                pause:
                  description: Pauses operator, stops reconciling child resources - defaults to False
                  type: boolean
                pgexporter_clone_secret:
                  description: Postgres Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                  type: string
                pgexporter_image_source:
                  description: Postgres Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-pgexporter
                  type: string
                pgexporter_tag:
                  description: Postgres Prometheus Exporter Image Sources Ref - defaults to 0.10.0
                  type: string
                postfix_namespace:
                  description: Namespace hosting your Postfix deployment, if any - defaults to AWX namespace
                  type: string
                postfix_selectors:
                  description: List of Label Selectors matching Postfix CR to integrate with - defaults to [ opsperator.io/AWX=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                postgres_max_connections:
                  description: Postgres Max Connections - defaults to 100
                  format: int64
                  type: integer
                postgres_shared_buffers:
                  description: Postgres Shared Buffers - defaults to 12MB
                  type: string
                postgres_tag:
                  description: Postgres Image Tag - defaults to 12
                  type: string
                prometheus_match:
                  description: Prometheus Service Label Value - defaults to scrape-me
                  type: string
                prometheus_namespace:
                  description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                  type: string
                prometheus_namespace_match:
                  description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                  type: string
                prometheus_service_label:
                  description: Prometheus Service Label Name - defaults to k8s-app
                  type: string
                pull_policy:
                  description: Images Pull Policy - defaults to IfNotPresent
                  type: string
                redis_clone_secret:
                  description: Redis Base Image Sources Clone Secret - defaults to undefined
                  type: string
                redis_cpu_limit:
                  description: Redis Cache CPU Limit - defaults to 100m
                  type: string
                redis_image_source:
                  description: Redis Base Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-redis
                  type: string
                redis_memory_limit:
                  description: Redis Cache Memory Limit - defaults to 256Mi
                  type: string
                redis_tag:
                  description: Redis Base Image Tag - defaults to 6.2.5
                  type: string
                root_domain:
                  description: Customer Root Domain - defaults to demo.local
                  type: string
                scheduling_affinity_required:
                  description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                  type: boolean
                smtp_port:
                  description: SMTP Port - defaults to 25
                  format: int64
                  type: integer
                smtp_relay:
                  description: SMTP Relay - defaults to undefined, discovers neighbor Postfix CR
                  type: string
                svc_keylen:
                  description: TLS Certificates Key Length - defaults to 2048
                  format: int64
                  type: integer
                svc_validity:
                  description: TLS Certificates Validity, in days - defaults to 365
                  format: int64
                  type: integer
                timezone:
                  description: Customer TimeZone - defaults to Europe/Paris
                  type: string
                topology_spread_max_skew:
                  description: TopologySpreadConstraints MaxSkew - defaults to 1
                  format: int64
                  type: integer
                use_rwo_storage:
                  description: Forces RWO StorageClass name - defaults to undefined
                  type: string
              type: object
            status:
              properties:
                adminsecret:
                  description: Ansible AWX Admin User Secret
                  type: string
                dashboards:
                  description: List of Grafana Dashboards for this deployment
                  items:
                    type: string
                  type: array
                internal:
                  description: Ansible AWX Internal Address
                  type: string
                public:
                  description: Ansible AWX Public Address
                  type: string
                ready:
                  description: Marks Deployment to be Ready
                  type: boolean
              type: object
          type: object
      served: true
      storage: true
      subresources:
        status: {}
