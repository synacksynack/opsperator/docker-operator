- apiVersion: apiextensions.k8s.io/v1
  kind: CustomResourceDefinition
  metadata:
    name: airsonics.wopla.io
  spec:
    group: wopla.io
    names:
      kind: Airsonic
      listKind: AirsonicList
      plural: airsonics
      singular: airsonic
    scope: Namespaced
    versions:
    - name: v1beta1
      additionalPrinterColumns:
      - name: Domain
        type: string
        description: Airsonic Domain
        jsonPath: .status.public
      - name: Age
        type: date
        jsonPath: .metadata.creationTimestamp
      schema:
        openAPIV3Schema:
          description: Orchestrates the deployment of Airsonic
          properties:
            apiVersion:
              description: 'APIVersion defines the versioned schema of this representation
                of an object. Servers should convert recognized schemas to the latest
                internal value, and may reject unrecognized values. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
              type: string
            kind:
              description: 'Kind is a string value representing the REST resource this
                object represents. Servers may infer this from the endpoint the client
                submits requests to. Cannot be updated. In CamelCase. More info
                https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
              type: string
            spec:
              properties:
                airsonic_clone_secret:
                  description: Airsonic Image Sources Clone Secret - defaults to undefined
                  type: string
                airsonic_cpu_limit:
                  description: Airsonic CPU Limit - defaults to 700m
                  type: string
                airsonic_data_capacity:
                  description: Airsonic PersistentVolumeClaim requested size - defaults to 50Gi
                  type: string
                airsonic_db_cpu_limit:
                  description: Airsonic Database CPU Limit - defaults to 300m
                  type: string
                airsonic_db_data_capacity:
                  description: Airsonic Database PersistentVolumeClaim requested size - defaults to 8Gi
                  type: string
                airsonic_db_memory_limit:
                  description: Airsonic Database Memory Limit - defaults to 512Mi
                  type: string
                airsonic_db_type:
                  description: Airsonic Database Type - defaults to jdbc
                  type: string
                airsonic_do_dlna:
                  description: Exposes Airsonic DLNA Port - defaults to False
                  type: boolean
                airsonic_do_upnp:
                  description: Exposes Airsonic UPNP Port - defaults to False
                  type: boolean
                airsonic_cpu_request:
                  description: Airsonic CPU Request - defaults to 10m
                  type: string
                airsonic_dlna_external_ip:
                  description: Airsonic DLNA External IP - defaults to False
                  type: string
                airsonic_image_source:
                  description: Airsonic Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-airsonic
                  type: string
                airsonic_java_args:
                  description: List of Arguments to pass Airsonic JVM - defaults to Xmx/Xms
                  items:
                    type: string
                  type: array
                airsonic_memory_limit:
                  description: Airsonic Memory Limit - defaults to 1024Mi
                  type: string
                airsonic_memory_request:
                  description: Airsonic Memory Request - defaults to 768Mi
                  type: string
                airsonic_replicas:
                  description: Airsonic Replicas Count - defaults to 1
                  format: int64
                  type: integer
                airsonic_tag:
                  description: Airsonic Image Sources Ref - defaults to 11.0.0-SNAPSHOT.20220625052932
                  type: string
                airsonic_theme:
                  description: Airsonic Default Theme - defaults to default
                  type: string
                airsonic_upnp_external_ip:
                  description: Airsonic UPNP External IP - defaults to False
                  type: string
                auth_namespace:
                  description: Namespace hosting your Directory deployment, if any - defaults to Airsonic namespace
                  type: string
                backups_daily_start_hour:
                  description: Kubernetes Daily Backups Start Hour - defaults to 15
                  format: int64
                  type: integer
                backups_hourly_start_minute:
                  description: Kubernetes Hourly Backups Start Minute - defaults to 15
                  format: int64
                  type: integer
                backups_interval:
                  description: Kubernetes Hourly Backups Interval - defaults to daily
                  type: string
                backups_weekly_start_day:
                  description: Kubernetes Weekly Backups Start Day - defaults to 2
                  format: int64
                  type: integer
                cluster_domain:
                  description: Kubernetes internal root domain - defaults to cluster.local
                  type: string
                default_namespace_match:
                  description: NetworkPolicy Default Namespace Match label - defaults to default
                  type: string
                deploy_pod_cpu_limit:
                  description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                  type: string
                deploy_pod_cpu_request:
                  description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                  type: string
                deploy_pod_memory_limit:
                  description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                  type: string
                deploy_pod_memory_request:
                  description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                  type: string
                deploy_revision_history:
                  description: DeploymentConfigs Revision History Limit - defaults to 3
                  type: string
                directory_selectors:
                  description: List of Label Selectors matching Directory CR to integrate with - defaults to [ opsperator.io/Airsonic=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                do_affinities:
                  description: Enables AntiAffinity rules scheduling Pods - defaults to True
                  type: boolean
                do_backups:
                  description: Enables Backups Integration - defaults to False
                  type: boolean
                do_build_trigger:
                  description: Enables BuildConfig/Tekton Triggers - defaults to True
                  type: boolean
                do_debugs:
                  description: Toggles Debug on all applications - defaults to False
                  type: boolean
                do_exporters:
                  description: Toggles Prometheus Exporters deployment - defaults to False
                  type: boolean
                do_loadbalancer_services:
                  description: Enables LoadBalancers services configuration - defaults to False
                  type: boolean
                do_network_policy:
                  description: Enables NetworkPolicies configuration - defaults to False
                  type: boolean
                do_topology_spread_constraints:
                  description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                  type: boolean
                do_triggers:
                  description: "Configures triggers redeploying containers when new images
                    are pushed to its source ImageStream - defaults to False"
                  type: boolean
                docker_registry:
                  description: "Source Registry hosting Images - defaults to docker-registry
                    if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                    a registry would be deployed by the operator"
                  type: string
                execution_affinity_required:
                  description: Should Execution Affinity Rules be Mandatory - defaults to True
                  type: boolean
                exporter_cpu_limit:
                  description: Prometheus Exporter container CPU Limit - defaults to 100m
                  type: string
                exporter_cpu_request:
                  description: Prometheus Exporter container CPU Request - defaults to 50m
                  type: string
                exporter_memory_limit:
                  description: Prometheus Exporter container Memory Limit - defaults to 128Mi
                  type: string
                exporter_memory_request:
                  description: Prometheus Exporter container Memory Request - defaults to 64Mi
                  type: string
                images_builds_namespace:
                  description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                  type: string
                ingress_filter_method:
                  description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                  type: string
                ingress_nodes_selector:
                  description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                  type: string
                java_clone_secret:
                  description: Java JRE Base Image Sources Clone Secret - defaults to undefined
                  type: string
                java_image_source:
                  description: Java JRE Base Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-java
                  type: string
                java_tag:
                  description: Java JRE Base Image Sources Ref - defaults to 20211226
                  type: string
                k8s_affinity_zone_label:
                  description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                  type: string
                k8s_namespace_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/os=linux ]
                  items:
                    type: string
                  type: array
                k8s_registry_ns:
                  description: Default Kubernetes integrated registry Namespace - defaults to registry
                  type: string
                k8s_registry_svc:
                  description: Default Kubernetes integrated registry Service Name - defaults to registry
                  type: string
                k8s_resource_nodes_selectors:
                  description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                  items:
                    type: string
                  type: array
                lang_code:
                  description: Applications Language - defaults to en
                  type: string
                mariadb_image_source:
                  description: MariaDB source image (amd64) - defaults to docker.io/mariadb
                  type: string
                mariadb_tag:
                  description: MariaDB Image Tag - defaults to 10.7
                  type: string
                mysqlexporter_clone_secret:
                  description: MySQL Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                  type: string
                mysqlexporter_image_source:
                  description: MySQL Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-mysqlexporter
                  type: string
                mysqlexporter_tag:
                  description: MySQL Prometheus Exporter Image Sources Ref - defaults to 0.13.0
                  type: string
                network_policy_deployment_label:
                  description: NetworkPolicy Pod Label Selector Name - defaults to name
                  type: string
                network_policy_namespace_label:
                  description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                    communications - defaults to netpol"
                  type: string
                opsperator_images_registry:
                  description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                    Kubernetes/OpenShift integrated registries"
                  type: string
                pause:
                  description: Pauses operator, stops reconciling child resources - defaults to False
                  type: boolean
                percona_clone_secret:
                  description: Percona Base Image Sources Clone Secret - defaults to undefined
                  type: string
                percona_image_source:
                  description: Percona Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-percona
                  type: string
                percona_replicas:
                  description: Percona Replicas Count - defaults to 3
                  format: int64
                  type: integer
                percona_tag:
                  description: Percona Image Sources Ref - defaults to 5.7.36-31.55-1
                  type: string
                pgexporter_clone_secret:
                  description: Postgres Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                  type: string
                pgexporter_image_source:
                  description: Postgres Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-pgexporter
                  type: string
                pgexporter_tag:
                  description: Postgres Prometheus Exporter Image Sources Ref - defaults to 0.10.0
                  type: string
                postfix_namespace:
                  description: Namespace hosting your Postfix deployment, if any - defaults to Airsonic namespace
                  type: string
                postfix_selectors:
                  description: List of Label Selectors matching Postfix CR to integrate with - defaults to [ opsperator.io/Airsonic=cr-metadata-name ]
                  items:
                    type: string
                  type: array
                postgres_max_connections:
                  description: Postgres Max Connections - defaults to 100
                  format: int64
                  type: integer
                postgres_shared_buffers:
                  description: Postgres Shared Buffers - defaults to 12MB
                  type: string
                prometheus_match:
                  description: Prometheus Service Label Value - defaults to scrape-me
                  type: string
                prometheus_namespace:
                  description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                  type: string
                prometheus_namespace_match:
                  description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                  type: string
                prometheus_scrape_timeout:
                  description: Prometheus Scrape Timeout - defaults to 10s
                  type: string
                prometheus_service_label:
                  description: Prometheus Service Label Name - defaults to k8s-app
                  type: string
                pull_policy:
                  description: Images Pull Policy - defaults to IfNotPresent
                  type: string
                root_domain:
                  description: Customer Root Domain - defaults to demo.local
                  type: string
                scheduling_affinity_required:
                  description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                  type: boolean
                smtp_port:
                  description: SMTP Port - defaults to 25
                  format: int64
                  type: integer
                smtp_relay:
                  description: SMTP Relay - defaults to undefined, discovers neighbor Postfix CR
                  type: string
                sshd_clone_secret:
                  description: Sshd Image Sources Clone Secret - defaults to undefined
                  type: string
                sshd_cpu_limit:
                  description: Sshd container CPU Limit - defaults to 100m
                  type: string
                sshd_cpu_request:
                  description: Sshd container CPU Request - defaults to 10m
                  type: string
                sshd_image_source:
                  description: Sshd sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-sshd
                  type: string
                sshd_memory_limit:
                  description: Sshd container Memory Limit - defaults to 512Mi
                  type: string
                sshd_memory_request:
                  description: Sshd container Memory Request - defaults to 128Mi
                  type: string
                sshd_tag:
                  description: Sshd Image Tag - defaults to 7.9p1-10-20220424
                  type: string
                svc_keylen:
                  description: TLS Certificates Key Length - defaults to 2048
                  format: int64
                  type: integer
                svc_validity:
                  description: TLS Certificates Validity, in days - defaults to 365
                  format: int64
                  type: integer
                tekton_k8s_version:
                  description: Tekton Pipelines Version to Deploy (on Kubernetes, use OLM with OpenShift) - defaults to 0.19.0
                  type: string
                timezone:
                  description: Customer TimeZone - defaults to Europe/Paris
                  type: string
                topology_spread_max_skew:
                  description: TopologySpreadConstraints MaxSkew - defaults to 1
                  format: int64
                  type: integer
                use_rwo_storage:
                  description: Forces RWO StorageClass name - defaults to undefined
                  type: string
              type: object
            status:
              properties:
                internal:
                  description: Airsonic Internal Address
                  type: string
                public:
                  description: Airsonic Public Address
                  type: string
                ready:
                  description: Marks Deployment to be Ready
                  type: boolean
              type: object
          type: object
      served: true
      storage: true
      subresources:
        status: {}
