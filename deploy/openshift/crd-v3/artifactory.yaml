- apiVersion: apiextensions.k8s.io/v1beta1
  kind: CustomResourceDefinition
  metadata:
    name: artifactories.wopla.io
  spec:
    group: wopla.io
    names:
      kind: Artifactory
      listKind: ArtifactoryList
      plural: artifactories
      singular: artifactory
    additionalPrinterColumns:
    - name: Domain
      type: string
      description: Artifactory Domain
      jsonPath: .status.public
    - name: Age
      type: date
      jsonPath: .metadata.creationTimestamp
    scope: Namespaced
    subresources:
      status: {}
    validation:
      openAPIV3Schema:
        description: Orchestrates the deployment of Artifactory
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info
              https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info
              https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
            type: string
          metadata:
            description: ObjectMeta is metadata that all persisted resources must have,
              which includes all objects users must create.
            properties:
              annotations:
                additionalProperties:
                  type: string
                description: 'Annotations is an unstructured key value map stored with
                  a resource that may be set by external tools to store and retrieve
                  arbitrary metadata. They are not queryable and should be preserved
                  when modifying objects. More info
                  http://kubernetes.io/docs/user-guide/annotations'
                type: object
              creationTimestamp:
                description: "CreationTimestamp is a timestamp representing the server
                  time when this object was created. It is not guaranteed to be set
                  in happens-before order across separate operations. Clients may not
                  set this value. It is represented in RFC3339 form and is in UTC. \n
                  Populated by the system. Read-only. Null for lists. More info
                  https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata"
                format: date-time
                type: string
              deletionGracePeriodSeconds:
                description: Number of seconds allowed for this object to gracefully
                  terminate before it will be removed from the system. Only set when
                  deletionTimestamp is also set. May only be shortened. Read-only.
                format: int64
                type: integer
              deletionTimestamp:
                description: "DeletionTimestamp is RFC 3339 date and time at which this
                  resource will be deleted. This field is set by the server when a graceful
                  deletion is requested by the user, and is not directly settable by
                  a client. The resource is expected to be deleted (no longer visible
                  from resource lists, and not reachable by name) after the time in
                  this field, once the finalizers list is empty. As long as the finalizers
                  list contains items, deletion is blocked. Once the deletionTimestamp
                  is set, this value may not be unset or be set further into the future,
                  although it may be shortened or the resource may be deleted prior
                  to this time. For example, a user may request that a pod is deleted
                  in 30 seconds. The Kubelet will react by sending a graceful termination
                  signal to the containers in the pod. After that 30 seconds, the Kubelet
                  will send a hard termination signal (SIGKILL) to the container and
                  after cleanup, remove the pod from the API. In the presence of network
                  partitions, this object may still exist after this timestamp, until
                  an administrator or automated process can determine the resource is
                  fully terminated. If not set, graceful deletion of the object has
                  not been requested. \n Populated by the system when a graceful deletion
                  is requested. Read-only. More info
                  https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata"
                format: date-time
                type: string
              finalizers:
                description: Must be empty before the object is deleted from the registry.
                  Each entry is an identifier for the responsible component that will
                  remove the entry from the list. If the deletionTimestamp of the object
                  is non-nil, entries in this list can only be removed.
                items:
                  type: string
                type: array
              generation:
                description: A sequence number representing a specific generation of
                  the desired state. Populated by the system. Read-only.
                format: int64
                type: integer
              labels:
                additionalProperties:
                  type: string
                description: 'Map of string keys and values that can be used to organize
                  and categorize (scope and select) objects. May match selectors of
                  replication controllers and services. More info
                  http://kubernetes.io/docs/user-guide/labels'
                type: object
              name:
                description: 'Name must be unique within a namespace. Is required when
                  creating resources, although some resources may allow a client to
                  request the generation of an appropriate name automatically. Name
                  is primarily intended for creation idempotence and configuration definition.
                  Cannot be updated. More info
                  http://kubernetes.io/docs/user-guide/identifiers#names'
                type: string
              namespace:
                description: "Namespace defines the space within each name must be unique.
                  An empty namespace is equivalent to the \"default\" namespace, but
                  \"default\" is the canonical representation. Not all objects are required
                  to be scoped to a namespace - the value of this field for those objects
                  will be empty. \n Must be a DNS_LABEL. Cannot be updated. More info
                  http://kubernetes.io/docs/user-guide/namespaces"
                type: string
              ownerReferences:
                description: List of objects depended by this object. If ALL objects
                  in the list have been deleted, this object will be garbage collected.
                  If this object is managed by a controller, then an entry in this list
                  will point to this controller, with the controller field set to true.
                  There cannot be more than one managing controller.
                items:
                  description: OwnerReference contains enough information to let you
                    identify an owning object. An owning object must be in the same
                    namespace as the dependent, or be cluster-scoped, so there is no
                    namespace field.
                  properties:
                    apiVersion:
                      description: API version of the referent.
                      type: string
                    blockOwnerDeletion:
                      description: If true, AND if the owner has the "foregroundDeletion"
                        finalizer, then the owner cannot be deleted from the key-value
                        store until this reference is removed. Defaults to false. To
                        set this field, a user needs "delete" permission of the owner,
                        otherwise 422 (Unprocessable Entity) will be returned.
                      type: boolean
                    controller:
                      description: If true, this reference points to the managing controller.
                      type: boolean
                    kind:
                      description: 'Kind of the referent. More info
                        https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
                      type: string
                    name:
                      description: 'Name of the referent. More info
                        http://kubernetes.io/docs/user-guide/identifiers#names'
                      type: string
                    uid:
                      description: 'UID of the referent. More info
                        http://kubernetes.io/docs/user-guide/identifiers#uids'
                      type: string
                  required:
                  - apiVersion
                  - kind
                  - name
                  - uid
                  type: object
                type: array
              resourceVersion:
                description: "An opaque value that represents the internal version of
                  this object that can be used by clients to determine when objects
                  have changed. May be used for optimistic concurrency, change detection,
                  and the watch operation on a resource or set of resources. Clients
                  must treat these values as opaque and passed unmodified back to the
                  server. They may only be valid for a particular resource or set of
                  resources. \n Populated by the system. Read-only. Value must be treated
                  as opaque by clients. More info
                  https://git.k8s.io/community/contributors/devel/api-conventions.md#concurrency-control-and-consistency"
                type: string
              selfLink:
                description: SelfLink is a URL representing this object. Populated by
                  the system. Read-only.
                type: string
              uid:
                description: "UID is the unique in time and space value for this object.
                  It is typically generated by the server on successful creation of
                  a resource and is not allowed to change on PUT operations. \n Populated
                  by the system. Read-only. More info
                  http://kubernetes.io/docs/user-guide/identifiers#uids"
                type: string
            type: object
          spec:
            properties:
              allow_rwx_storage:
                description: Allows usage for auto-detected RWX StorageClass - defaults to True
                type: boolean
              alpine_image_source:
                description: Alpine Docker image - defaults to quay.io/vqcomms/alpine-tools
                type: string
              alpine_tag:
                description: Alpine Docker image tag - defaults to 3.8.0
                type: string
              artifactory_cpu_limit:
                description: Artifactory CPU Limit - defaults to 1
                type: string
              artifactory_cpu_request:
                description: Artifactory CPU Request - defaults to 10m
                type: string
              artifactory_data_capacity:
                description: Artifactory PersistentVolumeClaim requested size - defaults to 100Gi
                type: string
              artifactory_db_cpu_limit:
                description: Artifactory Database CPU Limit - defaults to 300m
                type: string
              artifactory_db_data_capacity:
                description: Artifactory Database PersistentVolumeClaim requested size - defaults to 16Gi
                type: string
              artifactory_db_memory_limit:
                description: Artifactory Database Memory Limit - defaults to 512Mi
                type: string
              artifactory_image_source:
                description: Artifactory Docker image - defaults to docker.bintray.io/jfrog/artifactory-oss
                type: string
              artifactory_java_options:
                description: List of Arguments to pass Artifactory JVM - defaults to Xmx/Xms
                items:
                  type: string
                type: array
              artifactory_memory_limit:
                description: Artifactory Memory Limit - defaults to 3Gi
                type: string
              artifactory_memory_request:
                description: Artifactory Memory Request - defaults to 1800Mi
                type: string
              artifactory_replicas:
                description: Artifactory Replicas Count - defaults to 1
                format: int64
                type: integer
              artifactory_tag:
                description: Artifactory Image Tag - defaults to 7.27.3
                type: string
              backups_daily_start_hour:
                description: Kubernetes Daily Backups Start Hour - defaults to 15
                format: int64
                type: integer
              backups_hourly_start_minute:
                description: Kubernetes Hourly Backups Start Minute - defaults to 15
                format: int64
                type: integer
              backups_interval:
                description: Kubernetes Hourly Backups Interval - defaults to daily
                type: string
              backups_weekly_start_day:
                description: Kubernetes Weekly Backups Start Day - defaults to 2
                format: int64
                type: integer
              cluster_domain:
                description: Kubernetes internal root domain - defaults to cluster.local
                type: string
              default_namespace_match:
                description: NetworkPolicy Default Namespace Match label - defaults to default
                type: string
              deploy_pod_cpu_limit:
                description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                type: string
              deploy_pod_cpu_request:
                description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                type: string
              deploy_pod_memory_limit:
                description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                type: string
              deploy_pod_memory_request:
                description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                type: string
              deploy_revision_history:
                description: DeploymentConfigs Revision History Limit - defaults to 3
                type: string
              do_affinities:
                description: Enables AntiAffinity rules scheduling Pods - defaults to True
                type: boolean
              do_backups:
                description: Enables Backups Integration - defaults to False
                type: boolean
              do_build_trigger:
                description: Enables BuildConfig/Tekton Triggers - defaults to True
                type: boolean
              do_debugs:
                description: Toggles Debug on all applications - defaults to False
                type: boolean
              do_exporters:
                description: Toggles Prometheus Exporters deployment - defaults to False
                type: boolean
              do_network_policy:
                description: Enables NetworkPolicies configuration - defaults to False
                type: boolean
              do_triggers:
                description: "Configures triggers redeploying containers when new images
                  are pushed to its source ImageStream - defaults to False"
                type: boolean
              docker_registry:
                description: "Source Registry hosting Images - defaults to docker-registry
                  if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                  a registry would be deployed by the operator"
                type: string
              ensure_dbs_has_guaranteed_qosclass:
                description: "Sets Resources Requests & Limits such as Databases would always
                  use Kubernetes Guranteed QoS Class - defaults to True"
                type: boolean
              execution_affinity_required:
                description: Should Execution Affinity Rules be Mandatory - defaults to True
                type: boolean
              exporter_cpu_limit:
                description: Prometheus Exporter container CPU Limit - defaults to 100m
                type: string
              exporter_cpu_request:
                description: Prometheus Exporter container CPU Request - defaults to 50m
                type: string
              exporter_memory_limit:
                description: Prometheus Exporter container Memory Limit - defaults to 128Mi
                type: string
              exporter_memory_request:
                description: Prometheus Exporter container Memory Request - defaults to 64Mi
                type: string
              images_builds_namespace:
                description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                type: string
              ingress_filter_method:
                description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                type: string
              ingress_nodes_selector:
                description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                type: string
              k8s_affinity_zone_label:
                description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                type: string
              k8s_namespace_nodes_selectors:
                description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/arch=amd64 ]
                items:
                  type: string
                type: array
              k8s_registry_ns:
                description: Default Kubernetes integrated registry Namespace - defaults to registry
                type: string
              k8s_registry_svc:
                description: Default Kubernetes integrated registry Service Name - defaults to registry
                type: string
              k8s_resource_nodes_selectors:
                description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                items:
                  type: string
                type: array
              mariadb_image_source:
                description: MariaDB source image (amd64) - defaults to docker.io/mariadb
                type: string
              mariadb_tag:
                description: MariaDB Image Tag - defaults to 10.7
                type: string
              mysqlexporter_clone_secret:
                description: MySQL Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                type: string
              mysqlexporter_image_source:
                description: MySQL Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-mysqlexporter
                type: string
              mysqlexporter_tag:
                description: MySQL Prometheus Exporter Image Sources Ref - defaults to 0.13.0
                type: string
              network_policy_deployment_label:
                description: NetworkPolicy Pod Label Selector Name - defaults to name
                type: string
              network_policy_namespace_label:
                description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                  communications - defaults to netpol"
                type: string
              opsperator_images_registry:
                description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                  Kubernetes/OpenShift integrated registries"
                type: string
              pause:
                description: Pauses operator, stops reconciling child resources - defaults to False
                type: boolean
              percona_clone_secret:
                description: Percona Base Image Sources Clone Secret - defaults to undefined
                type: string
              percona_image_source:
                description: Percona Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-percona
                type: string
              percona_replicas:
                description: Percona Replicas Count - defaults to 3
                format: int64
                type: integer
              percona_tag:
                description: Percona Image Sources Ref - defaults to 5.7.36-31.55-1
                type: string
              pgexporter_clone_secret:
                description: Postgres Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                type: string
              pgexporter_image_source:
                description: Postgres Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-pgexporter
                type: string
              pgexporter_tag:
                description: Postgres Prometheus Exporter Image Sources Ref - defaults to 0.10.0
                type: string
              postgres_max_connections:
                description: Postgres Max Connections - defaults to 100
                format: int64
                type: integer
              postgres_shared_buffers:
                description: Postgres Shared Buffers - defaults to 12MB
                type: string
              postgres_tag:
                description: Postgres Image Tag - defaults to 12
                type: string
              prometheus_match:
                description: Prometheus Service Label Value - defaults to scrape-me
                type: string
              prometheus_namespace:
                description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                type: string
              prometheus_namespace_match:
                description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                type: string
              prometheus_service_label:
                description: Prometheus Service Label Name - defaults to k8s-app
                type: string
              pull_policy:
                description: Images Pull Policy - defaults to IfNotPresent
                type: string
              root_domain:
                description: Customer Root Domain - defaults to demo.local
                type: string
              rwo_needs_fsgroup:
                description: Optional securityContext.fsGroup numeric ID, to force on containers mounting RWO storage - defaults to False
                format: int64
                type: integer
              scheduling_affinity_required:
                description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                type: boolean
              svc_keylen:
                description: TLS Certificates Key Length - defaults to 2048
                format: int64
                type: integer
              svc_validity:
                description: TLS Certificates Validity, in days - defaults to 365
                format: int64
                type: integer
              timezone:
                description: Customer TimeZone - defaults to Europe/Paris
                type: string
              use_rwo_storage:
                description: Forces RWO StorageClass name - defaults to undefined
                type: string
            type: object
          status:
            properties:
              internal:
                description: Artifactory Internal Address
                type: string
              public:
                description: Artifactory Public Address
                type: string
              ready:
                description: Marks Deployment to be Ready
                type: boolean
            type: object
        type: object
    versions:
    - name: v1beta1
      served: true
      storage: true
