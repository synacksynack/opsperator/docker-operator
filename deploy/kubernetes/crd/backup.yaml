---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: backups.wopla.io
spec:
  group: wopla.io
  names:
    kind: Backup
    listKind: BackupList
    plural: backups
    shortNames:
    - bkp
    singular: backup
  scope: Namespaced
  versions:
  - name: v1beta1
    additionalPrinterColumns:
    - name: Age
      type: date
      jsonPath: .metadata.creationTimestamp
    schema:
      openAPIV3Schema:
        description: Orchestrates services Backups
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info
              https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info
              https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
            type: string
          spec:
            properties:
              backups_clone_secret:
                description: Kubernetes Backups Image Sources Clone Secret - defaults to undefined
                type: string
              backups_cpu_limit:
                description: Kubernetes Backups CPU Limit - defaults to 500m
                type: string
              backups_data_capacity:
                description: Kubernetes Backups PersistentVolumeClaim requested size - defaults to 100Gi
                type: string
              backups_image_source:
                description: Kubernetes Backups Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-backups
                type: string
              backups_job_interval:
                description: Kubernetes Backups Interval, in seconds - defaults to 3600
                type: string
              backups_memory_limit:
                description: Kubernetes Backups Memory Limit - defaults to 1Gi
                type: string
              backups_prune_faulty:
                description: Kubernetes Backups should prune faulty backups - defaults to False
                type: boolean
              backups_s3_access_key:
                description: "Backups s3 Access Key - defaults to undefined, mandatory
                  integrating with RadosGW-backed s3, would be provisioned automatically when
                  using MinIO."
                type: string
              backups_s3_bucket:
                description: Backups s3 bucket name - defaults to backups
                type: string
              backups_s3_export_method:
                description: "S3 export method, rolling to rewrite the previous backup,
                  incremental to create date-based subdirectories - defaults to rolling"
                type: string
              backups_s3_flavor:
                description: Backups s3 flavor - defaults to minio, can switch to ceph/radosgw
                type: string
              backups_s3_region:
                description: Backups s3 region, unless using MinIO - defaults to us-east-1
                type: string
              backups_s3_retention:
                description: "Backups s3 retention, evicting objects (eg: 7d) - defaults to False"
                type: string
              backups_s3_secret_key:
                description: "Backups s3 Secret Key - defaults to undefined, mandatory
                  integrating with RadosGW-backed s3, would be provisioned automatically when
                  using MinIO."
                type: string
              backups_tag:
                description: Kubernetes Backups Image Sources Ref - defaults to 1.0.0-20210905
                type: string
              backupsexporter_clone_secret:
                description: Prometheus Kubernetes Backups Exporter Image Sources Clone Secret - defaults to undefined
                type: string
              backupsexporter_image_source:
                description: Prometheus Kubernetes Backups Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-bkpexporter
                type: string
              backupsexporter_tag:
                description: Prometheus Kubernetes Backups Exporter Image Sources Ref - defaults to 1.0.0-20210905
                type: string
              ca_country:
                description: Internal CA Country Code - defaults to FR
                type: string
              ca_keylen:
                description: Internal CA Key Length - defaults to 4096
                format: int64
                type: integer
              ca_loc:
                description: Internal CA Location - defaults to Paris
                type: string
              ca_org:
                description: Internal CA Organization - defaults to K8S
                type: string
              ca_ou:
                description: Internal CA Organizational Unit Name - defaults to Opsperator
                type: string
              ca_st:
                description: Internal CA Organizational State - defaults to France
                type: string
              ca_validity:
                description: Internal CA Validity, in days - defaults to 3650
                format: int64
                type: integer
              cluster_domain:
                description: Kubernetes internal root domain - defaults to cluster.local
                type: string
              default_namespace_match:
                description: NetworkPolicy Default Namespace Match label - defaults to default
                type: string
              deploy_pod_cpu_limit:
                description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                type: string
              deploy_pod_cpu_request:
                description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                type: string
              deploy_pod_memory_limit:
                description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                type: string
              deploy_pod_memory_request:
                description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                type: string
              deploy_revision_history:
                description: DeploymentConfigs Revision History Limit - defaults to 3
                type: string
              do_build_trigger:
                default: True
                description: Enables BuildConfig/Tekton Triggers - defaults to True
                type: boolean
              do_debugs:
                default: False
                description: Toggles Debug on all applications - defaults to False
                type: boolean
              do_exporters:
                default: False
                description: Toggles Prometheus Exporters deployment - defaults to False
                type: boolean
              do_network_policy:
                default: False
                description: Enables NetworkPolicies configuration - defaults to False
                type: boolean
              do_s3:
                default: False
                description: Enables Nexus s3 storage integration - defaults to False
                type: boolean
              do_status_dashboards:
                default: False
                description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                  dashboards when Prometheus integration is enabled - defaults to False"
                type: boolean
              do_triggers:
                default: False
                description: "Configures triggers redeploying containers when new images
                  are pushed to its source ImageStream - defaults to False"
                type: boolean
              docker_registry:
                description: "Source Registry hosting Images - defaults to docker-registry
                  if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                  a registry would be deployed by the operator"
                type: string
              exporter_cpu_limit:
                description: Prometheus Exporter container CPU Limit - defaults to 100m
                type: string
              exporter_cpu_request:
                description: Prometheus Exporter container CPU Request - defaults to 50m
                type: string
              exporter_memory_limit:
                description: Prometheus Exporter container Memory Limit - defaults to 128Mi
                type: string
              exporter_memory_request:
                description: Prometheus Exporter container Memory Request - defaults to 64Mi
                type: string
              images_builds_namespace:
                description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                type: string
              k8s_namespace_nodes_selectors:
                description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/os=linux ]
                items:
                  type: string
                type: array
              k8s_registry_ns:
                description: Default Kubernetes integrated registry Namespace - defaults to registry
                type: string
              k8s_registry_svc:
                description: Default Kubernetes integrated registry Service Name - defaults to registry
                type: string
              k8s_resource_nodes_selectors:
                description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                items:
                  type: string
                type: array
              minio_namespace:
                description: Namespaces hosting MinIO - defaults to Backup namespace
                type: string
              network_policy_deployment_label:
                description: NetworkPolicy Pod Label Selector Name - defaults to name
                type: string
              network_policy_namespace_label:
                description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                  communications - defaults to netpol"
                type: string
              opsperator_images_registry:
                description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                  Kubernetes/OpenShift integrated registries"
                type: string
              pause:
                default: False
                description: Pauses operator, stops reconciling child resources - defaults to False
                type: boolean
              prometheus_match:
                description: Prometheus Service Label Value - defaults to scrape-me
                type: string
              prometheus_namespace:
                description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                type: string
              prometheus_namespace_match:
                description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                type: string
              prometheus_s3_label:
                description: Prometheus S3 Exporter Service Label Name - defaults to k8s-s3
                type: string
              prometheus_service_label:
                description: Prometheus Service Label Name - defaults to k8s-app
                type: string
              pull_policy:
                description: Images Pull Policy - defaults to IfNotPresent
                type: string
              rgw_make_bucket:
                description: Provisions Bucket when using s3 & RadosGW - defaults to True
                type: boolean
              rgw_namespace:
                description: Namespaces hosting RadosGW - defaults to Backup namespace
                type: string
              rwo_needs_fsgroup:
                description: Optional securityContext.fsGroup numeric ID, to force on containers mounting RWO storage - defaults to False
                format: int64
                type: integer
              s3exporter_clone_secret:
                description: Prometheus s3 Exporter Image Sources Clone Secret - defaults to undefined
                type: string
              s3exporter_cpu_limit:
                description: Prometheus s3 Exporter container CPU Limit - defaults to 200m
                type: string
              s3exporter_cpu_request:
                description: Prometheus s3 Exporter container CPU Request - defaults to 100m
                type: string
              s3exporter_image_source:
                description: Prometheus s3 Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-s3bucketexporter
                type: string
              s3exporter_memory_limit:
                description: Prometheus s3 Exporter container Memory Limit - defaults to 768Mi
                type: string
              s3exporter_memory_request:
                description: Prometheus s3 Exporter container Memory Request - defaults to 256Mi
                type: string
              s3exporter_tag:
                description: Prometheus s3 Exporter Image Sources Ref - defaults to 1.0.0
                type: string
              scheduling_affinity_required:
                description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                type: boolean
              tekton_buildah_image:
                description: Tekton Buildah Image - defaults to quay.io/buildah/stable:v1.21.0
                type: string
              tekton_docker_build_cpu_limit:
                description: Tekton Docker Build CPU Limit - defaults to 400m
                type: string
              tekton_docker_build_cpu_request:
                description: Tekton Docker Build CPU Request - defaults to 200m
                type: string
              tekton_docker_build_memory_limit:
                description: Tekton Docker Build Memory Limit - defaults to 4Gi
                type: string
              tekton_docker_build_memory_request:
                description: Tekton Docker Build Memory Request - defaults to 2Gi
                type: string
              tekton_img_image:
                description: Tekton GenuineTools Img Image - defaults to r.j3ss.co/img
                type: string
              tekton_k8s_version:
                description: Tekton Pipelines Version to Deploy (on Kubernetes, use OLM with OpenShift) - defaults to 0.19.0
                type: string
              tekton_kaniko_image:
                description: Tekton Kaniko Image - defaults to gcr.io/kaniko-project/executor:v1.7.0
                type: string
              tekton_oc_client_image:
                description: Tekton OC Client Image - defaults to quay.io/openshift/origin-cli:latest
                type: string
              tekton_preferred_builder:
                description: Tekton Preferred Docker Builder method - defaults to buildah
                type: string
              tekton_s2i_build_cpu_limit:
                description: Tekton S2I Build CPU Limit - defaults to 400m
                type: string
              tekton_s2i_build_cpu_request:
                description: Tekton S2I Build CPU Request - defaults to 200m
                type: string
              tekton_s2i_build_memory_limit:
                description: Tekton S2I Build Memory Limit - defaults to 4Gi
                type: string
              tekton_s2i_build_memory_request:
                description: Tekton S2I Build Memory Request - defaults to 2Gi
                type: string
              tekton_s2i_image:
                description: Tekton S2i Builder Image - defaults to quay.io/openshift-pipeline/s2i
                type: string
              tekton_skopeo_image:
                description: Tekton Skopeo Image - defaults to docker.io/ananace/skopeo
                type: string
              timezone:
                description: Customer TimeZone - defaults to Europe/Paris
                type: string
              use_rwo_storage:
                description: Forces RWO StorageClass name - defaults to undefined
                type: string
            type: object
          status:
            properties:
              dashboards:
                description: List of Grafana Dashboards for this deployment
                items:
                  type: string
                type: array
              ready:
                description: Marks Deployment to be Ready
                type: boolean
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
