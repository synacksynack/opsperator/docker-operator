---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: mastodons.wopla.io
spec:
  group: wopla.io
  names:
    kind: Mastodon
    listKind: MastodonList
    plural: mastodons
    singular: mastodon
  scope: Namespaced
  versions:
  - name: v1beta1
    additionalPrinterColumns:
    - name: Domain
      type: string
      description: Mastodon Domain
      jsonPath: .status.mastodon_public
    - name: Age
      type: date
      jsonPath: .metadata.creationTimestamp
    schema:
      openAPIV3Schema:
        description: Orchestrates the deployment of Mastodon
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info
              https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info
              https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
            type: string
          spec:
            properties:
              allowed_unsafe_sysctls:
                description: List of Unsafe Sysctls that were administratively enabled in your cluster, and which may then be used - defaults to [ ]
                items:
                  type: string
                type: array
              allow_rwx_storage:
                description: Allows usage for auto-detected RWX StorageClass - defaults to True
                type: boolean
              auth_namespace:
                description: Namespace hosting your Directory deployment, if any - defaults to Mastodon namespace
                type: string
              backups_daily_start_hour:
                description: Kubernetes Daily Backups Start Hour - defaults to 15
                format: int64
                type: integer
              backups_hourly_start_minute:
                description: Kubernetes Hourly Backups Start Minute - defaults to 15
                format: int64
                type: integer
              backups_interval:
                description: Kubernetes Hourly Backups Interval - defaults to daily
                type: string
              backups_weekly_start_day:
                description: Kubernetes Weekly Backups Start Day - defaults to 2
                format: int64
                type: integer
              ca_country:
                description: Internal CA Country Code - defaults to FR
                type: string
              ca_keylen:
                description: Internal CA Key Length - defaults to 4096
                format: int64
                type: integer
              ca_loc:
                description: Internal CA Location - defaults to Paris
                type: string
              ca_org:
                description: Internal CA Organization - defaults to K8S
                type: string
              ca_ou:
                description: Internal CA Organizational Unit Name - defaults to Opsperator
                type: string
              ca_st:
                description: Internal CA Organizational State - defaults to France
                type: string
              ca_validity:
                description: Internal CA Validity, in days - defaults to 3650
                format: int64
                type: integer
              cluster_domain:
                description: Kubernetes internal root domain - defaults to cluster.local
                type: string
              default_namespace_match:
                description: NetworkPolicy Default Namespace Match label - defaults to default
                type: string
              deploy_pod_cpu_limit:
                description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                type: string
              deploy_pod_cpu_request:
                description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                type: string
              deploy_pod_memory_limit:
                description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                type: string
              deploy_pod_memory_request:
                description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                type: string
              deploy_revision_history:
                description: DeploymentConfigs Revision History Limit - defaults to 3
                type: string
              directory_selectors:
                description: List of Label Selectors matching Directory CR to integrate with - defaults to [ opsperator.io/Mastodon=cr-metadata-name ]
                items:
                  type: string
                type: array
              do_affinities:
                default: True
                description: Enables AntiAffinity rules scheduling Pods - defaults to True
                type: boolean
              do_backups:
                default: False
                description: Enables Backups Integration - defaults to False
                type: boolean
              do_build_trigger:
                default: True
                description: Enables BuildConfig/Tekton Triggers - defaults to True
                type: boolean
              do_debugs:
                default: False
                description: Toggles Debug on all applications - defaults to False
                type: boolean
              do_exporters:
                default: False
                description: Toggles Prometheus Exporters deployment - defaults to False
                type: boolean
              do_hpa:
                default: False
                description: Enables Horizontal Pods Autoscalers configuration - defaults to False
                type: boolean
              do_network_policy:
                default: False
                description: Enables NetworkPolicies configuration - defaults to False
                type: boolean
              do_pdb:
                default: False
                description: Enables PodDisruptionBudgets configuration - defaults to False
                type: boolean
              do_s3:
                default: False
                description: Enables Mastodon s3 storage integration - defaults to False
                type: boolean
              do_status_dashboards:
                default: False
                description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                  dashboards when Prometheus integration is enabled - defaults to False"
                type: boolean
              do_topology_spread_constraints:
                default: False
                description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                type: boolean
              do_triggers:
                default: False
                description: "Configures triggers redeploying containers when new images
                  are pushed to its source ImageStream - defaults to False"
                type: boolean
              docker_registry:
                description: "Source Registry hosting Images - defaults to docker-registry
                  if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                  a registry would be deployed by the operator"
                type: string
              ensure_dbs_has_guaranteed_qosclass:
                description: "Sets Resources Requests & Limits such as Databases would always
                  use Kubernetes Guranteed QoS Class - defaults to True"
                type: boolean
              execution_affinity_required:
                description: Should Execution Affinity Rules be Mandatory - defaults to True
                type: boolean
              exporter_cpu_limit:
                description: Prometheus Exporter container CPU Limit - defaults to 100m
                type: string
              exporter_cpu_request:
                description: Prometheus Exporter container CPU Request - defaults to 50m
                type: string
              exporter_memory_limit:
                description: Prometheus Exporter container Memory Limit - defaults to 128Mi
                type: string
              exporter_memory_request:
                description: Prometheus Exporter container Memory Request - defaults to 64Mi
                type: string
              images_builds_namespace:
                description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                type: string
              ingress_filter_method:
                description: NetworkPolicies Ingress filter method, either nodeips, labels or none - defaults to labels
                type: string
              ingress_nodes_selector:
                description: NetworkPolicies Ingress nodes filters - defaults to node-role.kubernetes.io/infra=true
                type: string
              internalca_renew_before:
                description: Minimum Certificate Validity before Renewal (using Operator Internal CA) - defaults to 1w1d
                type: string
              k8s_affinity_zone_label:
                description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                type: string
              k8s_namespace_nodes_selectors:
                description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/os=linux ]
                items:
                  type: string
                type: array
              k8s_registry_ns:
                description: Default Kubernetes integrated registry Namespace - defaults to registry
                type: string
              k8s_registry_svc:
                description: Default Kubernetes integrated registry Service Name - defaults to registry
                type: string
              k8s_resource_nodes_selectors:
                description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                items:
                  type: string
                type: array
              k8s_unprivileged_userid:
                description: Default Kubernetes User ID running USER-less containers - defaults to 1000
                format: int64
                type: integer
              mastodon_cache_size:
                description: Mastodon Cache Size - defaults to 10G
                type: string
              mastodon_data_capacity:
                description: Mastodon PersistentVolumeClaim requested size - defaults to 32Gi
                type: string
              mastodon_db_cpu_limit:
                description: Mastodon Database CPU Limit - defaults to 250m
                type: string
              mastodon_db_data_capacity:
                description: Mastodon Database PersistentVolumeClaim requested size - defaults to 8Gi
                type: string
              mastodon_db_memory_limit:
                description: Mastodon Database Memory Limit - defaults to 680Mi
                type: string
              mastodon_image_source:
                description: Mastodon Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-mastodon
                type: string
              mastodon_replicas:
                description: Mastodon Replicas Count - defaults to 1
                format: int64
                type: integer
              mastodon_s3_access_key:
                description: "Mastodon s3 Access Key - defaults to undefined, mandatory
                  integrating with RadosGW-backed s3, would be provisioned automatically when
                  using MinIO."
                type: string
              mastodon_s3_bucket:
                description: Mastodon s3 bucket name - defaults to mastodon
                type: string
              mastodon_s3_flavor:
                description: Mastodon s3 flavor - defaults to minio, can switch to ceph/radosgw
                type: string
              mastodon_s3_region:
                description: Mastodon s3 region, unless using MinIO - defaults to us-east-1
                type: string
              mastodon_s3_secret_key:
                description: "Mastodon s3 Secret Key - defaults to undefined, mandatory
                  integrating with RadosGW-backed s3, would be provisioned automatically when
                  using MinIO."
                type: string
              mastodon_sidekiq_cpu_limit:
                description: Mastodon Sidekiq CPU Limit - defaults to 100m
                type: string
              mastodon_sidekiq_cpu_request:
                description: Mastodon Sidekiq CPU Request - defaults to 50m
                type: string
              mastodon_sidekiq_memory_limit:
                description: Mastodon Sidekiq Memory Limit - defaults to 768Mi
                type: string
              mastodon_sidekiq_memory_request:
                description: Mastodon Sidekiq Memory Request - defaults to 128Mi
                type: string
              mastodon_streaming_cpu_limit:
                description: Mastodon Streaming CPU Limit - defaults to 250m
                type: string
              mastodon_streaming_cpu_request:
                description: Mastodon Streaming CPU Request - defaults to 50m
                type: string
              mastodon_streaming_memory_limit:
                description: Mastodon Streaming Memory Limit - defaults to 768Mi
                type: string
              mastodon_streaming_memory_request:
                description: Mastodon Streaming Memory Request - defaults to 128Mi
                type: string
              mastodon_tag:
                description: Mastodon Image Sources Ref - defaults to 3.4.3
                type: string
              mastodon_web_cpu_limit:
                description: Mastodon Web CPU Limit - defaults to 300m
                type: string
              mastodon_web_cpu_request:
                description: Mastodon Web CPU Request - defaults to 50m
                type: string
              mastodon_web_memory_limit:
                description: Mastodon Web Memory Limit - defaults to 1536Mi
                type: string
              mastodon_web_memory_request:
                description: Mastodon Web Memory Request - defaults to 128Mi
                type: string
              network_policy_deployment_label:
                description: NetworkPolicy Pod Label Selector Name - defaults to name
                type: string
              network_policy_namespace_label:
                description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                  communications - defaults to netpol"
                type: string
              no_proxy:
                description: "HTTP Proxy Exclusions. Note cluster internal domain names, as well as
                  ingresses root domain would always be part of those exclusions - defaults to []"
                items:
                  type: string
                type: array
              nodejs_clone_secret:
                description: NodeJS Base Image Sources Clone Secret - defaults to undefined
                type: string
              nodejs_image_source:
                description: NodeJS Base Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-nodejs
                type: string
              nodejs_tag:
                description: NodeJS Base Image Sources Ref - defaults to 20220213
                type: string
              nodejs_version:
                description: NodeJS Base Image Runtime Version - defaults to 14
                format: int64
                type: integer
              opsperator_images_registry:
                description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                  Kubernetes/OpenShift integrated registries"
                type: string
              pause:
                default: False
                description: Pauses operator, stops reconciling child resources - defaults to False
                type: boolean
              pgexporter_clone_secret:
                description: Postgres Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                type: string
              pgexporter_image_source:
                description: Postgres Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-pgexporter
                type: string
              pgexporter_tag:
                description: Postgres Prometheus Exporter Image Sources Ref - defaults to 0.10.0
                type: string
              postfix_namespace:
                description: Namespace hosting your Postfix deployment, if any - defaults to Mastodon namespace
                type: string
              postfix_selectors:
                description: List of Label Selectors matching Postfix CR to integrate with - defaults to [ opsperator.io/Mastodon=cr-metadata-name ]
                items:
                  type: string
                type: array
              postgres_max_connections:
                description: Postgres Max Connections - defaults to 100
                format: int64
                type: integer
              postgres_shared_buffers:
                description: Postgres Shared Buffers - defaults to 12MB
                type: string
              prometheus_match:
                description: Prometheus Service Label Value - defaults to scrape-me
                type: string
              prometheus_namespace:
                description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                type: string
              prometheus_namespace_match:
                description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                type: string
              prometheus_s3_label:
                description: Prometheus S3 Exporter Service Label Name - defaults to k8s-s3
                type: string
              prometheus_scrape_timeout:
                description: Prometheus Scrape Timeout - defaults to 10s
                type: string
              prometheus_service_label:
                description: Prometheus Service Label Name - defaults to k8s-app
                type: string
              pull_policy:
                description: Images Pull Policy - defaults to IfNotPresent
                type: string
              proxy_host:
                description: HTTP Proxy Host - defaults to undefined
                type: string
              proxy_port:
                description: HTTP Proxy Host - defaults to 3128
                format: int64
                type: integer
              redis_clone_secret:
                description: Redis Base Image Sources Clone Secret - defaults to undefined
                type: string
              redis_cpu_limit:
                description: Redis Cache CPU Limit - defaults to 100m
                type: string
              redis_data_capacity:
                description: Redis PersistentVolumeClaim requested size - defaults to 8Gi
                type: string
              redis_image_source:
                description: Redis Base Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-redis
                type: string
              redis_memory_limit:
                description: Redis Cache Memory Limit - defaults to 256Mi
                type: string
              redis_tag:
                description: Redis Base Image Tag - defaults to 6.2.5
                type: string
              redisexporter_clone_secret:
                description: Redis Prometheus Exporter Image Sources Clone Secret - defaults to undefined
                type: string
              redisexporter_image_source:
                description: Redis Prometheus Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-redisexporter
                type: string
              redisexporter_tag:
                description: Redis Prometheus Exporter Image Tag - defaults to 1.27.0
                type: string
              rgw_make_bucket:
                description: Provisions Bucket when using s3 & RadosGW - defaults to True
                type: boolean
              root_domain:
                description: Customer Root Domain - defaults to demo.local
                type: string
              rwo_needs_fsgroup:
                description: Optional securityContext.fsGroup numeric ID, to force on containers mounting RWO storage - defaults to False
                format: int64
                type: integer
              rwx_needs_fsgroup:
                description: Optional securityContext.fsGroup numeric ID, to force on containers mounting RWX storage - defaults to False
                format: int64
                type: integer
              s3exporter_cpu_limit:
                description: Prometheus s3 Exporter container CPU Limit - defaults to 200m
                type: string
              s3exporter_cpu_request:
                description: Prometheus s3 Exporter container CPU Request - defaults to 100m
                type: string
              s3exporter_image_source:
                description: Prometheus s3 Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-s3bucketexporter
                type: string
              s3exporter_memory_limit:
                description: Prometheus s3 Exporter container Memory Limit - defaults to 768Mi
                type: string
              s3exporter_memory_request:
                description: Prometheus s3 Exporter container Memory Request - defaults to 256Mi
                type: string
              s3exporter_tag:
                description: Prometheus s3 Exporter Image Sources Ref - defaults to 1.0.0
                type: string
              scheduling_affinity_required:
                description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                type: boolean
              smtp_port:
                description: SMTP Relay Port - defaults to 25
                format: int64
                type: integer
              smtp_relay:
                description: SMTP Relay - defaults to undefined, discovers neighbor Postfix CR
                type: string
              squid_namespace:
                description: Namespace hosting your Squid deployment, if any - defaults to Mastodon namespace
                type: string
              squid_selectors:
                description: List of Label Selectors matching Squid CR to integrate with - defaults to [ opsperator.io/Mastodon=cr-metadata-name ]
                items:
                  type: string
                type: array
              sshd_clone_secret:
                description: Sshd Image Sources Clone Secret - defaults to undefined
                type: string
              sshd_cpu_limit:
                description: Sshd container CPU Limit - defaults to 100m
                type: string
              sshd_cpu_request:
                description: Sshd container CPU Request - defaults to 10m
                type: string
              sshd_image_source:
                description: Sshd sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-sshd
                type: string
              sshd_memory_limit:
                description: Sshd container Memory Limit - defaults to 512Mi
                type: string
              sshd_memory_request:
                description: Sshd container Memory Request - defaults to 128Mi
                type: string
              sshd_tag:
                description: Sshd Image Tag - defaults to 7.9p1-10-20220424
                type: string
              svc_keylen:
                description: TLS Certificates Key Length - defaults to 2048
                format: int64
                type: integer
              svc_validity:
                description: TLS Certificates Validity, in days - defaults to 365
                format: int64
                type: integer
              tekton_buildah_image:
                description: Tekton Buildah Image - defaults to quay.io/buildah/stable:v1.21.0
                type: string
              tekton_docker_build_cpu_limit:
                description: Tekton Docker Build CPU Limit - defaults to 400m
                type: string
              tekton_docker_build_cpu_request:
                description: Tekton Docker Build CPU Request - defaults to 200m
                type: string
              tekton_docker_build_memory_limit:
                description: Tekton Docker Build Memory Limit - defaults to 4Gi
                type: string
              tekton_docker_build_memory_request:
                description: Tekton Docker Build Memory Request - defaults to 2Gi
                type: string
              tekton_img_image:
                description: Tekton GenuineTools Img Image - defaults to r.j3ss.co/img
                type: string
              tekton_k8s_version:
                description: Tekton Pipelines Version to Deploy (on Kubernetes, use OLM with OpenShift) - defaults to 0.19.0
                type: string
              tekton_kaniko_image:
                description: Tekton Kaniko Image - defaults to gcr.io/kaniko-project/executor:v1.7.0
                type: string
              tekton_oc_client_image:
                description: Tekton OC Client Image - defaults to quay.io/openshift/origin-cli:latest
                type: string
              tekton_preferred_builder:
                description: Tekton Preferred Docker Builder method - defaults to buildah
                type: string
              tekton_s2i_build_cpu_limit:
                description: Tekton S2I Build CPU Limit - defaults to 400m
                type: string
              tekton_s2i_build_cpu_request:
                description: Tekton S2I Build CPU Request - defaults to 200m
                type: string
              tekton_s2i_build_memory_limit:
                description: Tekton S2I Build Memory Limit - defaults to 4Gi
                type: string
              tekton_s2i_build_memory_request:
                description: Tekton S2I Build Memory Request - defaults to 2Gi
                type: string
              tekton_s2i_image:
                description: Tekton S2i Builder Image - defaults to quay.io/openshift-pipeline/s2i
                type: string
              tekton_skopeo_image:
                description: Tekton Skopeo Image - defaults to docker.io/ananace/skopeo
                type: string
              timezone:
                description: Customer TimeZone - defaults to Europe/Paris
                type: string
              tls_provider:
                description: TLS Provider, cert-manager or internalca - defaults to internalca
                type: string
              topology_spread_max_skew:
                description: TopologySpreadConstraints MaxSkew - defaults to 1
                format: int64
                type: integer
              use_rwo_storage:
                description: Forces RWO StorageClass name - defaults to undefined
                type: string
            type: object
          status:
            properties:
              dashboards:
                description: List of Grafana Dashboards for this deployment
                items:
                  type: string
                type: array
              internal:
                description: Mastodon Internal Web Address
                type: string
              public:
                description: Mastodon Public Address
                type: string
              ready:
                description: Marks Deployment to be Ready
                type: boolean
              streaming:
                description: Mastodon Internal Streaming Address
                type: string
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
