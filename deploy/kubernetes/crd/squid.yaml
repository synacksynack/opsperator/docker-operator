---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: squids.wopla.io
spec:
  group: wopla.io
  names:
    kind: Squid
    listKind: SquidList
    plural: squids
    singular: squid
  scope: Namespaced
  versions:
  - name: v1beta1
    additionalPrinterColumns:
    - name: Internal
      type: string
      description: Squid Internal
      jsonPath: .status.internal
    - name: Age
      type: date
      jsonPath: .metadata.creationTimestamp
    schema:
      openAPIV3Schema:
        description: Orchestrates the deployment of Squid
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info
              https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info
              https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
            type: string
          spec:
            properties:
              cluster_domain:
                description: Kubernetes internal root domain - defaults to cluster.local
                type: string
              default_namespace_match:
                description: NetworkPolicy Default Namespace Match label - defaults to default
                type: string
              deploy_pod_cpu_limit:
                description: DeploymentConfigs Deploy container CPU Limit - defaults to 50m
                type: string
              deploy_pod_cpu_request:
                description: DeploymentConfigs Deploy container CPU Request - defaults to 50m
                type: string
              deploy_pod_memory_limit:
                description: DeploymentConfigs Deploy container Memory Limit - defaults to 512Mi
                type: string
              deploy_pod_memory_request:
                description: DeploymentConfigs Deploy container Memory Request - defaults to 128Mi
                type: string
              deploy_revision_history:
                description: DeploymentConfigs Revision History Limit - defaults to 3
                type: string
              do_affinities:
                default: True
                description: Enables AntiAffinity rules scheduling Pods - defaults to True
                type: boolean
              do_build_trigger:
                default: True
                description: Enables BuildConfig/Tekton Triggers - defaults to True
                type: boolean
              do_debugs:
                default: False
                description: Toggles Debug on all applications - defaults to False
                type: boolean
              do_exporters:
                default: False
                description: Toggles Prometheus Exporters deployment - defaults to False
                type: boolean
              do_network_policy:
                default: False
                description: Enables NetworkPolicies configuration - defaults to False
                type: boolean
              do_status_dashboards:
                default: False
                description: "Enables Custom Resources .status.dashboards[] update, listing Grafana
                  dashboards when Prometheus integration is enabled - defaults to False"
                type: boolean
              do_topology_spread_constraints:
                default: False
                description: Enables topologySpreadConstraints rules scheduling Pods - defaults to False
                type: boolean
              do_triggers:
                default: False
                description: "Configures triggers redeploying containers when new images
                  are pushed to its source ImageStream - defaults to False"
                type: boolean
              docker_registry:
                description: "Source Registry hosting Images - defaults to docker-registry
                  if okd3, image-registry if okd4, k8s integreted registry if detected, otherwise
                  a registry would be deployed by the operator"
                type: string
              execution_affinity_required:
                description: Should Execution Affinity Rules be Mandatory - defaults to True
                type: boolean
              exporter_cpu_limit:
                description: Prometheus Exporter container CPU Limit - defaults to 100m
                type: string
              exporter_cpu_request:
                description: Prometheus Exporter container CPU Request - defaults to 50m
                type: string
              exporter_memory_limit:
                description: Prometheus Exporter container Memory Limit - defaults to 256Mi
                type: string
              exporter_memory_request:
                description: Prometheus Exporter container Memory Request - defaults to 128Mi
                type: string
              images_builds_namespace:
                description: Namespace hosting container images, using cluster integrated registry - defaults to .metadata.namespace
                type: string
              k8s_affinity_zone_label:
                description: Label Name distributing replicas of a deployment - defaults to kubernetes.io/hostname
                type: string
              k8s_namespace_nodes_selectors:
                description: List of Label Selectors matching Kubernetes Nodes hosting at the Namespace level - defaults to [ kubernetes.io/os=linux ]
                items:
                  type: string
                type: array
              k8s_registry_ns:
                description: Default Kubernetes integrated registry Namespace - defaults to registry
                type: string
              k8s_registry_svc:
                description: Default Kubernetes integrated registry Service Name - defaults to registry
                type: string
              k8s_resource_nodes_selectors:
                description: List of Label Selectors matching Kubernetes Nodes hosting at the Pod level - defaults to []
                items:
                  type: string
                type: array
              k8s_unprivileged_userid:
                description: Default Kubernetes User ID running USER-less containers - defaults to 1000
                format: int64
                type: integer
              network_policy_deployment_label:
                description: NetworkPolicy Pod Label Selector Name - defaults to name
                type: string
              network_policy_namespace_label:
                description: "NetworkPolicy Namespace Label Selector Name, allowing Ingresses and Prometheus
                  communications - defaults to netpol"
                type: string
              opsperator_images_registry:
                description: "External Registry Prefix pulling custom operator images - defaults to undefined, falls back to
                  Kubernetes/OpenShift integrated registries"
                type: string
              pause:
                default: False
                description: Pauses operator, stops reconciling child resources - defaults to False
                type: boolean
              prometheus_match:
                description: Prometheus Service Label Value - defaults to scrape-me
                type: string
              prometheus_namespace:
                description: Namespace hosting Prometheus - defaults to ${prometheus_namespace_match}
                type: string
              prometheus_namespace_match:
                description: Prometheus Namespace Label Value - defaults to prometheus-monitoring
                type: string
              prometheus_service_label:
                description: Prometheus Service Label Name - defaults to k8s-app
                type: string
              pull_policy:
                description: Images Pull Policy - defaults to IfNotPresent
                type: string
              root_domain:
                description: Customer Root Domain - defaults to demo.local
                type: string
              scheduling_affinity_required:
                description: Should Scheduling Affinity Rules be Mandatoryd - defaults to True
                type: boolean
              squidexporter_clone_secret:
                description: Squid Exporter Image Sources Clone Secret - defaults to undefined
                type: string
              squidexporter_image_source:
                description: Squid Exporter Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-squidexporter
                type: string
              squidexporter_tag:
                description: Squid Exporter Image Tag - defaults to 1.9.4
                type: string
              squid_clone_secret:
                description: Squid Image Sources Clone Secret - defaults to undefined
                type: string
              squid_cpu_limit:
                description: Squid CPU Limit - defaults to 100m
                type: string
              squid_cpu_request:
                description: Squid CPU Request - defaults to 50m
                type: string
              squid_data_capacity:
                description: Squid Database PersistentVolumeClaim requested size - defaults to 25Gi
                type: string
              squid_image_source:
                description: Squid Image Sources - defaults to https://gitlab.com/synacksynack/opsperator/docker-squid
                type: string
              squid_memory_limit:
                description: Squid Memory Limit - defaults to 768Mi
                type: string
              squid_memory_request:
                description: Squid Memory Limit - defaults to 512Mi
                type: string
              squid_replicas:
                description: Squid Replicas Count - defaults to 1
                format: int64
                type: integer
              squid_tag:
                description: Squid Image Tag - defaults to 4.6-1-deb10u6
                type: string
              svc_keylen:
                description: TLS Certificates Key Length - defaults to 2048
                format: int64
                type: integer
              svc_validity:
                description: TLS Certificates Validity, in days - defaults to 365
                format: int64
                type: integer
              timezone:
                description: Customer TimeZone - defaults to Europe/Paris
                type: string
              tls_provider:
                description: TLS Provider, cert-manager or internalca - defaults to internalca
                type: string
              topology_spread_max_skew:
                description: TopologySpreadConstraints MaxSkew - defaults to 1
                format: int64
                type: integer
              use_rwo_storage:
                description: Forces RWO StorageClass name - defaults to undefined
                type: string
            type: object
          status:
            properties:
              dashboards:
                description: List of Grafana Dashboards for this deployment
                items:
                  type: string
                type: array
              internal:
                description: Squid Internal Address
                type: string
              ready:
                description: Marks Deployment to be Ready
                type: boolean
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
