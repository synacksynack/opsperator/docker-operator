- block:
  - name: Scales Cyrus down to 0
    async: 30
    k8s:
      definition:
        apiVersion: "{{ deployment_apivers }}"
        kind: "{{ deployment_kind }}"
        metadata:
          name: "{{ cyrus_name }}"
          namespace: "{{ namespace }}"
        spec:
          replicas: 0
    poll: 5
  - include_role:
      name: commons
      tasks_from: snapshots/fs.yaml
    vars:
      data_capacity: "{{ cyrus_data_capacity }}"
      snap_mark: "{{ target }}"
      source_pvc: "{{ cyrus_name }}"
  - name: Lookups Cyrus Job
    async: 30
    changed_when: False
    k8s_info:
      api_version: batch/v1
      kind: Job
      namespace: "{{ namespace }}"
      name: "{{ cyrus_name }}-{{ target }}-snap"
    no_log: True
    poll: 5
    register: cy_snap
  - block:
    - name: Reverts Upgrade when snapshot failed
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ cyrus_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
    - name: Fails when snapshot failed
      fail:
        msg: "Failed upgrading Cyrus to {{ target }}"
    when:
    - cy_snap is defined
    - cy_snap.resources is defined
    - >
         cy_snap.resources | length == 0
         or (cy_snap.resources | length > 0
             and cy_snap.resources[0].status is defined
             and (cy_snap.resources[0].status.completionTime is not defined
                  or (not ((cy_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))
  when:
  - do_backups
- block:
  - include_role:
      name: commons
      tasks_from: helpers/deployment.yaml
    vars:
      cfour_cpu_limit: "{{ sshd_cpu_limit }}"
      cfour_cpu_request: "{{ sshd_cpu_request }}"
      cfour_idx: "{{ 4 if (do_backups)
                      else 0 }}"
      cfour_memory_limit: "{{ sshd_memory_limit }}"
      cfour_memory_request: "{{ sshd_memory_request }}"
      cfour_tag: "{{ sshd_tag }}"
      cone_cpu_limit: "{{ rsyslog_cpu_limit }}"
      cone_cpu_request: "{{ rsyslog_cpu_request }}"
      cone_idx: 1
      cone_memory_limit: "{{ rsyslog_memory_limit }}"
      cone_memory_request: "{{ rsyslog_memory_request }}"
      cone_tag: "{{ cyrus_tag }}"
      cpu_limit: "{{ cyrus_cpu_limit }}"
      cpu_request: "{{ cyrus_cpu_request }}"
      ctoo_cpu_limit: "{{ saslauthd_cpu_limit }}"
      ctoo_cpu_request: "{{ saslauthd_cpu_request }}"
      ctoo_idx: 2
      ctoo_memory_limit: "{{ saslauthd_memory_limit }}"
      ctoo_memory_request: "{{ saslauthd_memory_request }}"
      ctoo_tag: "{{ cyrus_tag }}"
      ctree_cpu_limit: "{{ cyrus_job_cpu_limit }}"
      ctree_cpu_request: "{{ cyrus_job_cpu_request }}"
      ctree_idx: 3
      ctree_memory_limit: "{{ cyrus_job_memory_limit }}"
      ctree_memory_request: "{{ cyrus_job_memory_request }}"
      ctree_tag: "{{ cyrus_tag }}"
      dc_data: "{{ lookup('template', 'cyrus.j2') }}"
      dc_name: "{{ cyrus_name }}"
      dc_replicas: 1
      dc_tag: "{{ cyrus_tag }}"
      memory_limit: "{{ cyrus_memory_limit }}"
      memory_request: "{{ cyrus_memory_request }}"
  - include_role:
      name: commons
      tasks_from: helpers/wait-for.yaml
    vars:
      check_with: "{{ lookup('k8s', api_version=deployment_apivers,
                             kind=deployment_kind, namespace=namespace,
                             resource_name=cyrus_name) | default('') }}"
      obj_name: "{{ cyrus_name }}"
      retries_for: 120
      wait_for: 10
    when:
    - dc_started is defined
    - dc_started is changed
  - name: Checks Cyrus Upgrade Status
    async: 30
    changed_when: False
    k8s_info:
      api_version: "{{ deployment_apivers }}"
      kind: "{{ deployment_kind }}"
      name: "{{ cyrus_name }}"
      namespace: "{{ namespace }}"
    no_log: True
    poll: 5
    register: has_cy
    when:
    - (snap_failed | default(False)) == False
  - block:
    - name: Fails when upgrade is still running
      fail:
        msg: "Failed upgrading Cyrus to {{ target }}"
      when:
      - has_cy.resources[0].status.readyReplicas == 0
    # If initial deployment was made using a RWO PVC, then we can not
    # honor dokuwiki_replicas, even though there is an RWX SC
    # check/replicas substitution implemented in deploy.yaml, which
    # would run right after upgrade steps: it can wait
    #- include_role:
    #    name: commons
    #    tasks_from: helpers/deployment.yaml
    #  vars:
    #    cfour_cpu_limit: "{{ sshd_cpu_limit }}"
    #    cfour_cpu_request: "{{ sshd_cpu_request }}"
    #    cfour_idx: "{{ 4 if (do_backups)
    #                    else 0 }}"
    #    cfour_memory_limit: "{{ sshd_memory_limit }}"
    #    cfour_memory_request: "{{ sshd_memory_request }}"
    #    cfour_tag: "{{ sshd_tag }}"
    #    cone_cpu_limit: "{{ rsyslog_cpu_limit }}"
    #    cone_cpu_request: "{{ rsyslog_cpu_request }}"
    #    cone_idx: 1
    #    cone_memory_limit: "{{ rsyslog_memory_limit }}"
    #    cone_memory_request: "{{ rsyslog_memory_request }}"
    #    cone_tag: "{{ cyrus_tag }}"
    #    cpu_limit: "{{ cyrus_cpu_limit }}"
    #    cpu_request: "{{ cyrus_cpu_request }}"
    #    ctoo_cpu_limit: "{{ saslauthd_cpu_limit }}"
    #    ctoo_cpu_request: "{{ saslauthd_cpu_request }}"
    #    ctoo_idx: 2
    #    ctoo_memory_limit: "{{ saslauthd_memory_limit }}"
    #    ctoo_memory_request: "{{ saslauthd_memory_request }}"
    #    ctoo_tag: "{{ cyrus_tag }}"
    #    ctree_cpu_limit: "{{ cyrus_job_cpu_limit }}"
    #    ctree_cpu_request: "{{ cyrus_job_cpu_request }}"
    #    ctree_idx: 3
    #    ctree_memory_limit: "{{ cyrus_job_memory_limit }}"
    #    ctree_memory_request: "{{ cyrus_job_memory_request }}"
    #    ctree_tag: "{{ cyrus_tag }}"
    #    dc_data: "{{ lookup('template', 'cyrus.j2') }}"
    #    dc_name: "{{ cyrus_name }}"
    #    dc_replicas: "{{ cyrus_replicas }}"
    #    dc_tag: "{{ cyrus_tag }}"
    #    memory_limit: "{{ cyrus_memory_limit }}"
    #    memory_request: "{{ cyrus_memory_request }}"
    #  when:
    #  - cyrus_replicas not in [ '1', 1 ]
    #  - has_cy.resources[0].status.readyReplicas > 0
    when:
    - has_cy is defined
    - has_cy.resources is defined
    - has_cy.resources | length > 0
    - has_cy.resources[0].status is defined
    - has_cy.resources[0].status.readyReplicas is defined
  when:
  - not (snap_failed | default(False))
