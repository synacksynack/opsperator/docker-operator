- include_role:
    name: backups
    tasks_from: wait.yaml
  vars:
    check_only: True
- include_role:
    name: postfix
    tasks_from: wait.yaml
  vars:
    check_only: True
    in_namespace: "{{ postfix_namespace | default(namespace) }}"
    obj_selectors: "{{ postfix_selectors | default([ 'opsperator.io/' ~ cr_kind ~ '=' ~ cr_name ]) }}"
  when:
  - smtp_relay is not defined
- include_role:
    name: squid
    tasks_from: wait.yaml
  vars:
    check_only: True
    in_namespace: "{{ squid_namespace | default(namespace) }}"
    obj_selectors: "{{ squid_selectors | default([ 'opsperator.io/' ~ cr_kind ~ '=' ~ cr_name ]) }}"
- block:
  - include_role:
      name: minio
      tasks_from: wait.yaml
    vars:
      in_namespace: "{{ minio_namespace | default(namespace) }}"
    when:
    - mastodon_s3_flavor == 'minio'
  - include_role:
      name: radosgw
      tasks_from: wait.yaml
    vars:
      in_namespace: "{{ rgw_namespace | default(namespace) }}"
    when:
    - mastodon_s3_flavor in [ 'ceph', 'rados', 'radosgw' ]
  when:
  - do_s3
- include_role:
    name: directory
    tasks_from: wait.yaml
  vars:
    check_only: True
    in_namespace: "{{ auth_namespace | default(namespace) }}"
    obj_selectors: "{{ directory_selectors | default([ 'opsperator.io/' ~ cr_kind ~ '=' ~ cr_name ]) }}"
- block:
  - include_role:
      name: prometheus
      tasks_from: check.yaml
    vars:
      check_only: True
  - block:
    - name: Sets Mastodon Prometheus Facts
      set_fact:
        dashboards_for_deployments:
        - deployments:
          - "{{ dspg_name }}"
          product: postgres
        - deployments:
          - "{{ rgw_name | default(False) }}"
          namespace: "{{ rgw_namespace | default(namespace) }}"
          product: radosgw
        - deployments:
          - "{{ cache_name }}"
          product: redis
        - deployments:
          - "{{ minio_name | default(False) }}"
          namespace: "{{ minio_namespace | default(namespace) }}"
          product: minio
        - deployments:
          - "{{ ('bucketxp-' ~ mastodon_name) if (do_s3) else False }}"
          product: s3
        dashboards_for_namespace: "{{ namespace }}"
    when:
    - grafana_public_address | default(False)
    - grafana_public_address != 'not deployed'
  when:
  - do_exporters
  - do_status_dashboards | default(False)
- block:
  - name: Lookups Mastodon PersistentVolumeClaim
    async: 30
    changed_when: False
    failed_when: False
    k8s_info:
      api_version: v1
      kind: PersistentVolumeClaim
      namespace: "{{ namespace }}"
      name: "{{ mastodon_name }}"
    no_log: True
    poll: 5
    register: ms_pvc
  - name: Preserves RWO Volume
    set_fact:
      do_s3: False
      rwo_storage: False
    when:
    - ms_pvc is defined
    - ms_pvc.resources is defined
    - ms_pvc.resources | length > 0
    - ms_pvc.resources[0].spec is defined
    - ms_pvc.resources[0].spec.accessModes is defined
    - ms_pvc.resources[0].spec.accessModes | length > 0
    - ms_pvc.resources[0].spec.accessModes[0] == 'ReadWriteOnce'
  when:
  - >
      rwx_storage | default(False)
      or do_s3
- name: Sets Mastodon Directory Facts
  set_fact:
    do_fusion: "{{ True if (fusion_address is defined
                            and fusion_address != 'not deployed'
                            and directory_crd.resources | length > 0
                            and directory_crd.resources[0].spec is defined
                            and directory_crd.resources[0].spec.do_fusion | default(False)
                            and directory_crd.resources[0].spec.do_mastodon | default(False))
                        else False }}"
    do_ldap: "{{ True if (ldap_address is defined
                          and directory_crd.resources | length > 0
                          and directory_crd.resources[0].spec is defined
                          and directory_crd.resources[0].spec.do_mastodon | default(False))
                      else False }}"
    do_lemon: "{{ True if (lemon_manager is defined
                           and lemon_manager != 'not deployed'
                           and directory_crd.resources | length > 0
                           and directory_crd.resources[0].spec is defined
                           and directory_crd.resources[0].spec.do_mastodon | default(False))
                       else False }}"
    mastodon_fqdn: "{{ (mastodon_force_fqdn | default(False))
                         if (mastodon_force_fqdn | default(False))
                         else ((directory_crd.resources[0].spec.directory_fqdn_mastodon | default(False))
                               if (directory_cr is defined
                                   and directory_crd.resources is defined
                                   and directory_crd.resources | length > 0
                                   and directory_crd.resources[0].spec is defined
                                   and directory_crd.resources[0].spec.directory_fqdn_mastodon is defined
                                   and directory_crd.resources[0].spec.do_mastodon | default(False))
                               else ('mastodon.' ~ root_domain)) }}"
- block:
  - name: Fixes Directory Mastodon FQDN
    async: 30
    k8s:
      definition:
        apiVersion: wopla.io/v1beta1
        kind: Directory
        metadata:
          name: "{{ directory_crd.resources[0].metadata.name }}"
          namespace: "{{ ldap_namespace }}"
        spec:
          directory_fqdn_mastodon: "{{ mastodon_fqdn }}"
    when:
    - ldap_root_domain is defined
    - ldap_root_domain != root_domain
    - directory_cr is defined
    - directory_crd.resources is defined
    - directory_crd.resources | length > 0
    - directory_crd.resources[0].spec is defined
    - (directory_crd.resources[0].spec.directory_fqdn_mastodon | default('mastodon.' ~ ldap_root_domain)) != mastodon_fqdn
  - block:
    - include_role:
        name: directory
        tasks_from: client-config.yaml
      vars:
        config_namespace: "{{ namespace }}"
    - include_role:
        name: directory
        tasks_from: secret-dup.yaml
      vars:
        get_keys:
        - directory-root
        - mastodon-password
    when:
    - ldap_namespace != namespace
  - include_role:
      name: commons
      tasks_from: ssl/ca-init.yaml
    vars:
      tls_provider: "{{ directory_tls_provider }}"
    when:
    - k8s_flavor == 'kubernetes'
    - directory_tls_provider != tls_provider
  when:
  - do_ldap or do_lemon
- include_role:
    name: commons
    tasks_from: ssl/ca-init.yaml
  when:
  - k8s_flavor == 'kubernetes'
- name: "Lookups Mastodon {{ deployment_kind }}"
  async: 30
  changed_when: False
  failed_when: False
  k8s_info:
    api_version: "{{ deployment_apivers }}"
    kind: "{{ deployment_kind }}"
    namespace: "{{ namespace }}"
    name: "{{ mastodon_name }}"
  no_log: True
  poll: 5
  register: mt_dc
- name: "Lookups Mastodon Job {{ deployment_kind }}"
  async: 30
  changed_when: False
  failed_when: False
  k8s_info:
    api_version: "{{ deployment_apivers }}"
    kind: "{{ deployment_kind }}"
    namespace: "{{ namespace }}"
    name: "{{ mastodon_name }}-job"
  no_log: True
  poll: 5
  register: mtback_dc
