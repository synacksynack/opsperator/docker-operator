- block:
  - include_role:
      name: commons
      tasks_from: helpers/volume.yaml
    vars:
      vol_name: "{{ mtpg_name }}"
      vol_size: "{{ mastodon_db_data_capacity }}"
  - name: Marks operation in progress
    set_fact:
      snap_failed: True
    when:
    - has_pvc is defined
    - has_pvc.resources is defined
    - (has_pvc.resources | length) > 0
  - block:
    - name: Scales Mastodon down to 0
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ mastodon_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 0
      poll: 5
    - name: Scales Mastodon Job down to 0
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ mastodon_name }}-job"
            namespace: "{{ namespace }}"
          spec:
            replicas: 0
      poll: 5
      when:
      - mtback_dc is defined
      - mtback_dc.resources is defined
      - mtback_dc.resources | length > 0
    when:
    - (snap_failed | default(False)) == False
  - include_role:
      name: commons
      tasks_from: snapshots/postgres.yaml
    vars:
      db_data_capacity: "{{ mastodon_db_data_capacity }}"
      db_host: "{{ dspg_name }}"
      db_secret_name: "{{ dspg_name }}"
      job_assume_pod_netpol: "{{ mastodon_name }}"
      provision_volume: False
      snap_mark: "{{ target }}"
      snap_wait_for: 30
      snap_wait_retries: 120
  - name: Lookups Postgres Snapshot Job
    async: 30
    changed_when: False
    k8s_info:
      api_version: batch/v1
      kind: Job
      namespace: "{{ namespace }}"
      name: "{{ dspg_name }}-{{ target }}-snap"
    no_log: True
    poll: 5
    register: pg_snap
    when:
    - (snap_failed | default(False)) == False
  - block:
    - include_role:
        name: commons
        tasks_from: snapshots/fs.yaml
      vars:
        data_capacity: "{{ mastodon_data_capacity }}"
        snap_mark: "{{ target }}"
        snap_wait_for: 30
        snap_wait_retries: 120
        source_pvc: "{{ mastodon_name }}"
    - name: Lookups Mastodon Job
      async: 30
      changed_when: False
      k8s_info:
        api_version: batch/v1
        kind: Job
        namespace: "{{ namespace }}"
        name: "{{ mastodon_name }}-{{ target }}-snap"
      no_log: True
      poll: 5
      register: mt_snap
      when:
      - (snap_failed | default(False)) == False
    - name: Disables snapshots for further Mastodon upgrades
      set_fact:
        mastodon_snapshot_data_on_upgrade: False
    when:
    - (snap_failed | default(False)) == False
    - mastodon_snapshot_data_on_upgrade | default(True)
  - block:
    - name: Reverts Upgrade when snapshot failed
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ mastodon_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
    - name: Fails when snapshot failed
      fail:
        msg: "Failed upgrading Mastodon to {{ target }}"
    when:
    - >
        snap_failed | default(False)
        or (pg_snap is defined
            and pg_snap.resources is defined
            and (pg_snap.resources | length == 0
                 or (pg_snap.resources[0].status is defined
                     and pg_snap.resources[0].status is defined
                     and (pg_snap.resources[0].status.completionTime is not defined
                          or (not ((pg_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
        or (mt_snap is defined
            and mt_snap.resources is defined
            and (mt_snap.resources | length == 0
                 or (mt_snap.resources | length > 0
                     and mt_snap.resources[0].status is defined
                     and (mt_snap.resources[0].status.completionTime is not defined
                          or (not ((mt_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
  when:
  - do_backups
- block:
  - include_role:
      name: commons
      tasks_from: helpers/deployment.yaml
    vars:
      cone_cpu_limit: "{{ mastodon_sidekiq_cpu_limit }}"
      cone_cpu_request: "{{ mastodon_sidekiq_cpu_request }}"
      cone_idx: 1
      cone_memory_limit: "{{ mastodon_sidekiq_memory_limit }}"
      cone_memory_request: "{{ mastodon_sidekiq_memory_request }}"
      cone_tag: "{{ mastodon_tag }}"
      cpu_limit: "{{ mastodon_web_cpu_limit }}"
      cpu_request: "{{ mastodon_web_cpu_request }}"
      ctoo_cpu_limit: "{{ mastodon_streaming_cpu_limit }}"
      ctoo_cpu_request: "{{ mastodon_streaming_cpu_request }}"
      ctoo_idx: 2
      ctoo_memory_limit: "{{ mastodon_streaming_memory_limit }}"
      ctoo_memory_request: "{{ mastodon_streaming_memory_request }}"
      ctoo_tag: "{{ mastodon_tag }}"
      ctree_cpu_limit: "{{ sshd_cpu_limit }}"
      ctree_cpu_request: "{{ sshd_cpu_request }}"
      ctree_idx: "{{ 3 if (do_backups)
                       else 0 }}"
      ctree_memory_limit: "{{ sshd_memory_limit }}"
      ctree_memory_request: "{{ sshd_memory_request }}"
      ctree_tag: "{{ sshd_tag }}"
      dc_data: "{{ lookup('template', 'mastodon-allinone.j2') }}"
      dc_name: "{{ mastodon_name }}"
      dc_replicas: 1
      dc_tag: "{{ mastodon_tag }}"
      memory_limit: "{{ mastodon_web_memory_limit }}"
      memory_request: "{{ mastodon_web_memory_request }}"
    when:
    - >
        not (mtback_dc is defined
             and mtback_dc.resources is defined
             and mtback_dc.resources | length > 0)
  - block:
    - include_role:
        name: commons
        tasks_from: helpers/deployment.yaml
      vars:
        cone_cpu_limit: "{{ mastodon_streaming_cpu_limit }}"
        cone_cpu_request: "{{ mastodon_streaming_cpu_request }}"
        cone_idx: 1
        cone_memory_limit: "{{ mastodon_streaming_memory_limit }}"
        cone_memory_request: "{{ mastodon_streaming_memory_request }}"
        cone_tag: "{{ target }}"
        cpu_limit: "{{ mastodon_web_cpu_limit }}"
        cpu_request: "{{ mastodon_web_cpu_request }}"
        dc_data: "{{ lookup('template', 'mastodon-front.j2') }}"
        dc_name: "{{ mastodon_name }}"
        dc_replicas: 1
        dc_tag: "{{ target }}"
        memory_limit: "{{ mastodon_web_memory_limit }}"
        memory_request: "{{ mastodon_web_memory_request }}"
    - include_role:
        name: commons
        tasks_from: helpers/deployment.yaml
      vars:
        cone_cpu_limit: "{{ sshd_cpu_limit }}"
        cone_cpu_request: "{{ sshd_cpu_request }}"
        cone_idx: "{{ 1 if (do_backups and not do_s3)
                        else 0 }}"
        cone_memory_limit: "{{ sshd_memory_limit }}"
        cone_memory_request: "{{ sshd_memory_request }}"
        cone_tag: "{{ sshd_tag }}"
        cpu_limit: "{{ mastodon_sidekiq_cpu_limit }}"
        cpu_request: "{{ mastodon_sidekiq_cpu_request }}"
        dc_data: "{{ lookup('template', 'mastodon-back.j2') }}"
        dc_name: "{{ mastodon_name }}-job"
        dc_replicas: 1
        dc_tag: "{{ target }}"
        memory_limit: "{{ mastodon_sidekiq_memory_limit }}"
        memory_request: "{{ mastodon_sidekiq_memory_request }}"
    when:
    - mtback_dc is defined
    - mtback_dc.resources is defined
    - mtback_dc.resources | length > 0
  - include_role:
      name: commons
      tasks_from: helpers/wait-for.yaml
    vars:
      check_with: "{{ lookup('k8s', api_version=deployment_apivers,
                             kind=deployment_kind, namespace=namespace,
                             resource_name=mastodon_name) | default('') }}"
      obj_name: "{{ mastodon_name }}"
      retries_for: 190
      wait_for: 10
    when:
    - dc_started is defined
    - dc_started is changed
  - name: Checks Mastodon Upgrade Status
    async: 30
    changed_when: False
    k8s_info:
      api_version: "{{ deployment_apivers }}"
      kind: "{{ deployment_kind }}"
      name: "{{ mastodon_name }}"
      namespace: "{{ namespace }}"
    no_log: True
    poll: 5
    register: has_mt
  - name: Fails when upgrade is still running
    fail:
      msg: "Failed upgrading Mastodon to {{ target }}"
    when:
    - has_mt is defined
    - has_mt.resources is defined
    - has_mt.resources | length > 0
    - has_mt.resources[0].status is defined
    - has_mt.resources[0].status.readyReplicas is defined
    - has_mt.resources[0].status.readyReplicas == 0
  when:
  - not (snap_failed | default(False))
