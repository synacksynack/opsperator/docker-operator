- block:
  - block:
    - include_role:
        name: commons
        tasks_from: helpers/volume.yaml
      vars:
        vol_name: "{{ zpshms_name if (zpush_db_type in [ 'mariadb', 'mysql' ])
                                else (zpshpc_name if (zpush_db_type == 'percona')
                                                else zpshpg_name) }}-{{ target }}-snap"
        vol_size: "{{ zpush_db_data_capacity }}"
    - name: Marks operation in progress
      set_fact:
        snap_failed: True
      when:
      - has_pvc is defined
      - has_pvc.resources is defined
      - (has_pvc.resources | length) > 0
    when:
    - not (rwo_can_snap | default(False))
  - name: Scales ZPush down to 0
    async: 30
    k8s:
      definition:
        apiVersion: "{{ deployment_apivers }}"
        kind: "{{ deployment_kind }}"
        metadata:
          name: "{{ zpush_name }}"
          namespace: "{{ namespace }}"
        spec:
          replicas: 0
    poll: 5
    when:
    - (snap_failed | default(False)) == False
  - block:
    - block:
      - include_role:
          name: commons
          tasks_from: snapshots/mysql.yaml
        vars:
          db_data_capacity: "{{ zpush_db_data_capacity }}"
          db_host: "{{ zpshms_name if (zpush_db_type != 'percona')
                                 else (zpshpc_name ~ '-client') }}"
          db_secret_name: "{{ zpshms_name if (zpush_db_type != 'percona')
                                        else zpshpc_name }}"
          job_assume_pod_netpol: "{{ zpush_name }}"
          provision_volume: False
          snap_mark: "{{ target }}"
          snap_wait_for: 30
          snap_wait_retries: 120
        when:
        - (snap_failed | default(False)) == False
      - name: Lookups MySQL Snapshot Job
        async: 30
        changed_when: False
        k8s_info:
          api_version: batch/v1
          kind: Job
          namespace: "{{ namespace }}"
          name: "{{ zpshms_name if (zpush_db_type != 'percona')
                                else (zpshpc_name ~ '-client') }}-{{ target }}-snap"
        no_log: True
        poll: 5
        register: mysql_snap
        when:
        - (snap_failed | default(False)) == False
      when:
      - zpush_db_type in [ 'mariadb', 'mysql', 'percona' ]
    - block:
      - include_role:
          name: commons
          tasks_from: snapshots/postgres.yaml
        vars:
          db_data_capacity: "{{ zpush_db_data_capacity }}"
          db_host: "{{ zpshpg_name }}"
          db_secret_name: "{{ zpshpg_name }}"
          job_assume_pod_netpol: "{{ zpush_name }}"
          provision_volume: False
          snap_mark: "{{ target }}"
          snap_wait_for: 30
          snap_wait_retries: 120
        when:
        - (snap_failed | default(False)) == False
      - name: Lookups Postgres Snapshot Job
        async: 30
        changed_when: False
        k8s_info:
          api_version: batch/v1
          kind: Job
          namespace: "{{ namespace }}"
          name: "{{ zpshpg_name }}-{{ target }}-snap"
        no_log: True
        poll: 5
        register: pg_snap
        when:
        - (snap_failed | default(False)) == False
      when:
      - zpush_db_type == 'postgres'
    when:
    - not (rwo_can_snap | default(False))
    - zpush_db_type in [ 'mariadb', 'mysql', 'percona', 'postgres' ]
  - block:
    - name: Scales Postgres down to 0
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ zpshpg_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 0
      poll: 5
      when:
      - zpush_db_type == 'postgres'
    - name: Scales MySQL down to 0
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ zpshms_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 0
      poll: 5
      when:
      - zpush_db_type in [ 'mariadb', 'mysql' ]
    - name: Scales Percona down to replicas-1
      async: 30
      k8s:
        definition:
          apiVersion: "{{ sts_apivers }}"
          kind: StatefulSet
          metadata:
            name: "{{ zpshpc_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: "{{ ((percona_replicas | default(3)) | int) - 1
                            if ((percona_replicas | default(3)) | int > 1)
                            else 0 }}"
      poll: 5
      when:
      - zpush_db_type == 'percona'
    - include_role:
        name: commons
        tasks_from: helpers/volumesnapshot.yaml
      vars:
        pvc_name: "{{ zpshms_name if (zpush_db_type != 'percona')
                                else ('data-' ~ zpshpc_name ~ '-'
                                      ~ (((percona_replicas | default(3)) | int) - 1)) }}"
        snap_mark: "{{ target }}"
    - name: Lookups VolumeSnapshot
      async: 30
      changed_when: False
      k8s_info:
        api_version: snapshot.storage.k8s.io/v1beta1
        kind: VolumeSnapshot
        namespace: "{{ namespace }}"
        name: "{{ zpshms_name if (zpush_db_type != 'percona')
                            else ('data-' ~ zpshpc_name ~ '-'
                                      ~ (((percona_replicas | default(3)) | int) - 1)) }}"
      no_log: True
      poll: 5
      register: dbvolume_snap
    - name: Checks for failures
      set_fact:
        snap_failed: True
      when:
      - >
          not (dbvolume_snap is defined
               and dbvolume_snap.resources is defined
               and dbvolume_snap.resources | length > 0
               and dbvolume_snap.resources[0].status is defined
               and dbvolume_snap.resources[0].status.readyToUse is defined
               and dbvolume_snap.resources[0].status.readyToUse == True)
    - name: Scales Postgres back up
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ zpshpg_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
      when:
      - zpush_db_type == 'postgres'
    - name: Scales MySQL back up
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ zpshms_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
      when:
      - zpush_db_type in [ 'mariadb', 'mysql' ]
    - name: Scales Percona back up
      async: 30
      k8s:
        definition:
          apiVersion: "{{ sts_apivers }}"
          kind: StatefulSet
          metadata:
            name: "{{ zpshpc_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: "{{ percona_replicas | default(3) }}"
      poll: 5
      when:
      - zpush_db_type == 'percona'
    when:
    - rwo_can_snap | default(False)
    - zpush_db_type in [ 'mariadb', 'mysql', 'percona', 'postgres' ]
  - block:
    - block:
      - include_role:
          name: commons
          tasks_from: snapshots/fs.yaml
        vars:
          data_capacity: "{{ zpush_data_capacity }}"
          snap_mark: "{{ target }}"
          snap_wait_for: 30
          snap_wait_retries: 360
          source_pvc: "{{ zpush_name }}"
      - name: Lookups ZPush Job
        async: 30
        changed_when: False
        k8s_info:
          api_version: batch/v1
          kind: Job
          namespace: "{{ namespace }}"
          name: "{{ zpush_name }}-{{ target }}-snap"
        no_log: True
        poll: 5
        register: zp_snap
        when:
        - (snap_failed | default(False)) == False
      when:
      - not (rwo_can_snap | default(False))
    - block:
      - include_role:
          name: commons
          tasks_from: helpers/volumesnapshot.yaml
        vars:
          pvc_name: "{{ zpush_name }}"
          snap_mark: "{{ target }}"
      - name: Lookups VolumeSnapshot
        async: 30
        changed_when: False
        k8s_info:
          api_version: snapshot.storage.k8s.io/v1beta1
          kind: VolumeSnapshot
          namespace: "{{ namespace }}"
          name: "{{ zpush_name }}"
        no_log: True
        poll: 5
        register: appvolume_snap
      - name: Checks for failures
        set_fact:
          snap_failed: True
        when:
        - >
            not (appvolume_snap is defined
                 and appvolume_snap.resources is defined
                 and appvolume_snap.resources | length > 0
                 and appvolume_snap.resources[0].status is defined
                 and appvolume_snap.resources[0].status.readyToUse is defined
                 and appvolume_snap.resources[0].status.readyToUse == True)
      when:
      - rwo_can_snap | default(False)
    when:
    - (snap_failed | default(False)) == False
    - zpush_db_type == 'file'
  - block:
    - name: Reverts Upgrade when snapshot failed
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ zpush_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
    - name: Fails when snapshot failed
      fail:
        msg: "Failed upgrading ZPush to {{ target }}"
      when:
      - (snap_failed | default(False)) == False
    when:
    - >
        snap_failed | default(False)
        or (zpush_db_type in [ 'mariadb', 'mysql', 'percona' ]
            and mysql_snap is defined
            and mysql_snap.resources is defined
            and (mysql_snap.resources | length == 0
                 or (mysql_snap.resources[0].status is defined
                     and mysql_snap.resources[0].status is defined
                     and (mysql_snap.resources[0].status.completionTime is not defined
                          or (not ((mysql_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
        or (zpush_db_type == 'postgres'
            and pg_snap is defined
            and pg_snap.resources is defined
            and (pg_snap.resources | length == 0
                 or (pg_snap.resources[0].status is defined
                     and pg_snap.resources[0].status is defined
                     and (pg_snap.resources[0].status.completionTime is not defined
                          or (not ((pg_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
        or (zpush_db_type == 'file'
            and zp_snap is defined
            and zp_snap.resources is defined
            and (zp_snap.resources | length == 0
                 or (zp_snap.resources | length > 0
                     and zp_snap.resources[0].status is defined
                     and (zp_snap.resources[0].status.completionTime is not defined
                          or (not ((zp_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
  when:
  - do_backups
- block:
  - include_role:
      name: commons
      tasks_from: helpers/deployment.yaml
    vars:
      cpu_limit: "{{ zpush_cpu_limit }}"
      cpu_request: "{{ zpush_cpu_request }}"
      dc_data: "{{ lookup('template', 'zpush.j2') }}"
      dc_name: "{{ zpush_name }}"
      dc_replicas: 1
      dc_tag: "{{ zpush_tag }}"
      exporter_idx: 1
      exporter_tag: "{{ httpexporter_tag }}"
      memory_limit: "{{ zpush_memory_limit }}"
      memory_request: "{{ zpush_memory_request }}"
  - include_role:
      name: commons
      tasks_from: helpers/wait-for.yaml
    vars:
      check_with: "{{ lookup('k8s', api_version=deployment_apivers,
                             kind=deployment_kind, namespace=namespace,
                             resource_name=zpush_name) | default('') }}"
      obj_name: "{{ zpush_name }}"
      retries_for: 220
      wait_for: 10
    when:
    - dc_started is defined
    - dc_started is changed
  - name: Checks ZPush Upgrade Status
    async: 30
    changed_when: False
    k8s_info:
      api_version: "{{ deployment_apivers }}"
      kind: "{{ deployment_kind }}"
      name: "{{ zpush_name }}"
      namespace: "{{ namespace }}"
    no_log: True
    poll: 5
    register: has_zp
  - block:
    - name: Fails when upgrade is still running
      fail:
        msg: "Failed upgrading ZPush to {{ target }}"
      when:
      - has_zp.resources[0].status.readyReplicas == 0
    #- include_role:
    #    name: commons
    #    tasks_from: helpers/deployment.yaml
    #  vars:
    #    cpu_limit: "{{ zpush_cpu_limit }}"
    #    cpu_request: "{{ zpush_cpu_request }}"
    #    dc_data: "{{ lookup('template', 'zpush.j2') }}"
    #    dc_name: "{{ zpush_name }}"
    #    dc_replicas: "{{ zpush_replicas }}"
    #    dc_tag: "{{ zpush_tag }}"
    #    exporter_idx: 1
    #    exporter_tag: "{{ httpexporter_tag }}"
    #    memory_limit: "{{ zpush_memory_limit }}"
    #    memory_request: "{{ zpush_memory_request }}"
    #  when:
    #  - zpush_replicas not in [ '1', 1 ]
    #  #- (has_pvc.resources | default([])) | length > 0
    #  #- has_pvc.resources[0].spec.accessModes[0] == 'ReadWriteMany'
    #  - has_zp.resources[0].status.readyReplicas > 0
    when:
    - has_zp is defined
    - has_zp.resources is defined
    - has_zp.resources | length > 0
    - has_zp.resources[0].status is defined
    - has_zp.resources[0].status.readyReplicas is defined
  when:
  - (snap_failed | default(False)) == False
