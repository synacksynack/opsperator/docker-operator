- include_role:
    name: commons
    tasks_from: helpers/network-policy.yaml
  vars:
    source:
    - name: "{{ matomo_name }}"
      namespace: "{{ namespace }}"
    policy_name: "{{ matomo_name }}-to-{{ postfix_name | default('postfix') }}"
    policy_namespace: "{{ postfix_namespace | default(namespace) }}"
    policy_state: "{{ 'present' if (do_network_policy
                                    and smtp_address | default(False))
                                else 'absent' }}"
    subject: "{{ postfix_name | default('postfix') }}"
    to:
    - number: "25"
    - number: "465"
- include_role:
    name: commons
    tasks_from: helpers/network-policy.yaml
  vars:
    source:
    - name: "{{ matomo_name }}"
      namespace: "{{ namespace }}"
    policy_name: "{{ matomo_name }}-to-{{ squid_name | default('squid') }}"
    policy_namespace: "{{ squid_namespace | default(namespace) }}"
    policy_state: "{{ 'present' if (squid_address | default(False)
                                    and do_network_policy)
                                else 'absent' }}"
    subject: "{{ squid_name | default('squid') }}"
    to:
    - number: "8080"
- include_role:
    name: commons
    tasks_from: helpers/network-policy.yaml
  vars:
    source:
    - name: "{{ matomo_name }}"
      namespace: "{{ namespace }}"
    policy_name: "{{ matomo_name }}-to-{{ ldap_name | default('ldap') }}"
    policy_namespace: "{{ ldap_namespace | default(namespace) }}"
    policy_state: "{{ 'present' if (do_ldap
                                    and do_network_policy)
                                else 'absent' }}"
    subject: "{{ ldap_name | default('ldap') }}"
    to:
    - number: "1389"
    - number: "1636"
- include_role:
    name: commons
    tasks_from: helpers/network-policy.yaml
  vars:
    source:
    - name: "{{ matomo_name }}"
      namespace: "{{ namespace }}"
    policy_name: "{{ matomo_name }}-to-{{ lemon_name | default('lemon') }}"
    policy_namespace: "{{ ldap_namespace | default(namespace) }}"
    policy_state: "{{ 'present' if (do_lemon
                                    and do_network_policy)
                                else 'absent' }}"
    subject: "{{ lemon_name | default('lemon') }}"
    to:
    - number: "8080"
- include_role:
    name: commons
    tasks_from: helpers/volume.yaml
  vars:
    vol_access_mode: "{{ 'ReadWriteOnce' if (rwx_storage is not defined)
                                         else 'ReadWriteMany' }}"
    vol_name: "{{ matomo_name }}"
    vol_size: "{{ matomo_data_capacity }}"
- include_role:
    name: commons
    tasks_from: helpers/deployment.yaml
  vars:
    cpu_limit: "{{ matomo_cpu_limit }}"
    cpu_request: "{{ matomo_cpu_request }}"
    dc_data: "{{ lookup('template', 'matomo.j2') }}"
    dc_name: "{{ matomo_name }}"
    dc_replicas: "{{ 1 if (((mt_dc.resources | default([])) | length == 0)
                           or ((has_pvc.resources | default([])) | length > 0
                               and has_pvc.resources[0].spec.accessModes[0] == 'ReadWriteMany'))
                       else matomo_replicas }}"
    dc_tag: "{{ matomo_tag }}"
    exporter_idx: 1
    exporter_tag: "{{ httpexporter_tag }}"
    memory_limit: "{{ matomo_memory_limit }}"
    memory_request: "{{ matomo_memory_request }}"
- include_role:
    name: commons
    tasks_from: helpers/service.yaml
  vars:
    ports:
    - 8080
    svc_name: "{{ matomo_name }}"
- include_role:
    name: commons
    tasks_from: helpers/route.yaml
  vars:
    route_custom_timeout: 2m
    route_host: "{{ matomo_fqdn }}"
    route_name: "{{ matomo_name }}"
    svc_name: "{{ matomo_name }}"
- include_role:
    name: commons
    tasks_from: helpers/network-policy.yaml
  vars:
    source:
    - namespace: "{{ default_namespace_match }}"
    policy_name: "public-to-{{ matomo_name }}"
    policy_state: "{{ 'present' if (do_network_policy)
                                else 'absent' }}"
    subject: "{{ matomo_name }}"
    to:
    - number: "8080"
- include_role:
    name: commons
    tasks_from: helpers/service.yaml
  vars:
    is_exporter: True
    ports:
    - 9113
    svc_name: "{{ matomo_name }}"
    svc_state: "{{ 'present' if (do_exporters)
                             else 'absent' }}"
- include_role:
    name: commons
    tasks_from: helpers/network-policy.yaml
  vars:
    source:
    - namespace: "{{ prometheus_namespace_match }}"
    policy_name: "prometheus-to-{{ matomo_name }}"
    policy_state: "{{ 'present' if (do_exporters
                                    and do_network_policy)
                                else 'absent' }}"
    subject: "{{ matomo_name }}"
    to:
    - number: "9113"
- include_role:
    name: commons
    tasks_from: helpers/wait-for.yaml
  vars:
    check_with: "{{ lookup('k8s', api_version=deployment_apivers,
                           kind=deployment_kind, namespace=namespace,
                           resource_name=matomo_name) | default('') }}"
    obj_name: "{{ matomo_name }}"
    retries_for: 60
    wait_for: 10
  when:
  - dc_started is defined
  - dc_started is changed
- include_role:
    name: commons
    tasks_from: helpers/autoscale.yaml
  vars:
    autoscale_cpu_target: "{{ matomo_hpa_cpu_target | default(hpa_cpu_target | default(75)) }}"
    autoscale_name: "{{ matomo_name }}"
    autoscale_state: "{{ 'present' if (matomo_replicas not in [ 0, '0' ]
                                       and rwx_storage is defined
                                       and ((has_pvc.resources | default([])) | length == 0
                                            or ((has_pvc.resources | default([])) | length > 0
                                                and has_pvc.resources[0].spec.accessModes[0] == 'ReadWriteMany')))
                                   else 'absent' }}"
    max_replicas: "{{ matomo_max_replicas | default(hpa_max_replicas | default(4)) }}"
    min_replicas: "{{ matomo_min_replicas | default(matomo_replicas) }}"
- include_role:
    name: commons
    tasks_from: helpers/poddisruptionbudget.yaml
  vars:
    pdb_name: "{{ matomo_name }}"
    pdb_min_available: 1
    pdb_state: "{{ 'present' if ((matomo_replicas | int) > 1
                                 and do_pdb)
                             else 'absent' }}"
