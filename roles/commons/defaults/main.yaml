# Operator Customer Setup
allow_rwx_storage: True
allowed_unsafe_sysctls: []
cluster_domain: cluster.local
execution_affinity_required: True
opsperator_images_registry: False
pull_policy: IfNotPresent
root_domain: demo.local
scheduling_affinity_required: True
timezone: Europe/Paris

# Kubernetes Backups Defaults
backups_daily_start_hour: 15
backups_hourly_start_minute: 15
backups_interval: daily
backups_weekly_start_day: 2

# Components Sources Tags
admin_tag: master
airsonic_tag: 11.0.0-SNAPSHOT.20220625052932
apache_tag: 2.4.52-20220918
ara_tag: 1.5.8
authproxy_tag: 2.4.52-20220918
backups_tag: 1.0.0-20210905
backupsexporter_tag: 1.0.0-20210905
bbbcsp_tag: 0.0.2-20220213
cassandra_tag: 4.0.1
codimd_tag: 2.4.1-20220213
console_tag: master
csdexporter_tag: 2.3.5
curator_tag: 5.8.1-20210919
cyrus_tag: 3.0.7-23.el8-1
davserver_tag: master
diaspora_tag: 0.7.15.0-1
dokuwiki_tag: 2022-07-31a-20220918
draw_tag: 20.2.3
elasticsearch_tag: 8.6.0
errbit_tag: 0.9.1-20220611
esearchexporter_tag: 1.1
escomrade_tag: 1.2.0-4
etherpad_tag: 1.8.6-20220213
fusion_tag: 1.3-20220918
gerrit_tag: 3.6.1-1
gitea_tag: 1.17.2
gogs_tag: 0.12.10
greenlight_tag: 2.12.6
httpexporter_tag: 1.1.0
java_tag: 20211226
kibana_tag: 8.6.0
kiwiirc_tag: 20.05.24.1-20220918
ldapexporter_tag: 1.0.1
ldap_tag: 2.6.3-1
lemon_tag: 2.0.15-20220918
lool_tag: '20210919'
mailcatcher_tag: 0.8.2
maildrop_tag: 3.0.1-20220213
mailhog_tag: 1.0.1-1
mastodon_tag: 3.4.3
matomo_tag: 4.11.0-20220918
mattermost_tag: 6.0.2
medusa_tag: 0.5.20
mongodb_tag: 4.4.10
mongoexporter_tag: 0.20.7
munin_tag: 2.0.49-20220227
mysqlexporter_tag: 0.13.0
nextcloudappscache_tag: 1.0.0
nextcloud_tag: '20220918'
#nextcloud_version: 22
nextcloudlookupserver_tag: 0.3.2-20220918
nexus_tag: 3.41.1-01
nexusexporter_tag: 0.2.3
newznab_tag: 0.2.3-20220918
nodejs_tag: 20220213
#nodejs_version: 14
opensearch_tag: 2.2.0
opensearchdashboards_tag: 2.2.0
orange_tag: 1.2b-20210905
peertube_tag: 4.3.0
peertubeexporter_tag: 1.0.0
percona_tag: 5.7.36-31.55-1
pgexporter_tag: 0.10.0
php_tag: '20220918'
phpldapadmin_tag: 1.2.6.3-20220918
postfix_tag: 3.4.14-0-deb10u1-3
postfixexporter_tag: 1.0.0-20210911
radosgw_tag: 17.2.5
redisexporter_tag: 1.27.0
redis_tag: 6.2.5
releasebell_tag: 1.7.1-20220213
rgwexporter_tag: 1.1.0
rocket_tag: 3.18.3
roundcube_tag: 1.6.0-20220918
rspamd_tag: 3.3.2
s3explorer_tag: 1.0.0-4
s3exporter_tag: 1.0.0
sabnzbd_tag: 3.6.0
sabexporter_tag: 1.0.1-20210926
scalelite_tag: 1.3.4
servicedesk_tag: 0.4-20220918
sogo_tag: 5.7.1-20220918
sonarqube_tag: 9.6.1.59531
soplanning_tag: 1.49.00-20220918
squid_tag: 4.6-1-deb10u6
squidexporter_tag: 1.9.4
sshd_tag: 7.9p1-10-20220424
ssp_tag: 1.5.1-20220918
sympa_tag: 6.2.40-20220227
syspass_tag: 3.2.2-20220918
tinytinyrss_tag: 1.0.0-20220918
transmission_tag: 2.94.2-20210918
transmissionexporter_tag: 1.0.0-20210926
wekan_tag: '6.51'
whitepages_tag: 0.3-20220918
wordpress_tag: 6.0.2-20220918
zookeeper_tag: master
zpush_tag: 2.6.4-20220918

# Base Images - may be build, as dependencies
# Could be built from a private git repository:
# - with Tekton, applying a Tekton object that includes a private ssh key,
#     that would then be used cloning those repositories
# - with BuildConfigs, setting `apache_clone_secret`, `php_clone_secret`,
#     and so on, and having defined proper clone token (uses http) in your
#     Ci object (`clone_tokens` array, see `deploy/openshift/cr/sweet-ci.yaml`)
# May not be used, if `opsperator_images_registry` was defined, then all images
#     would be pulled from that registry (eg, the apache image URL would
#     be set to `${opsperator_images_registry}/apache:${apache_tag}`, and so on)
# See `roles/commons/tasks/helpers/images-facts.yaml` for further details on
#     those URLs generation
apache_image_source: https://gitlab.com/synacksynack/opsperator/docker-apache
authproxy_image_source: https://gitlab.com/synacksynack/opsperator/docker-authproxy
cassandra_image_source: https://gitlab.com/synacksynack/opsperator/docker-cassandra
csdexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-csdexporter
davserver_image_source: git@gitlab.com:synacksynack/opsperator/docker-davserver
httpexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-httpexporter
elasticsearch_image_source: https://gitlab.com/synacksynack/opsperator/docker-elasticsearch
esearchexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-esearchexporter
java_image_source: https://gitlab.com/synacksynack/opsperator/docker-java
ldapexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-ldapexporter
mongodb_image_source: https://gitlab.com/synacksynack/opsperator/docker-mongodb
mongoexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-mongoexporter
mysqlexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-mysqlexporter
nodejs_image_source: https://gitlab.com/synacksynack/opsperator/docker-nodejs
opensearch_image_source: https://gitlab.com/synacksynack/opsperator/docker-opensearch
percona_image_source: https://gitlab.com/synacksynack/opsperator/docker-percona
pgexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-pgexporter
php_image_source: https://gitlab.com/synacksynack/opsperator/docker-php
postfixexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-postfixexporter
redis_image_source: https://gitlab.com/synacksynack/opsperator/docker-redis
redisexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-redisexporter
rgwexporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-rgwexporter
s3exporter_image_source: https://gitlab.com/synacksynack/opsperator/docker-s3bucketexporter
sshd_image_source: https://gitlab.com/synacksynack/opsperator/docker-sshd
zookeeper_image_source: git@gitlab.com:synacksynack/opsperator/docker-zookeeper

# Third-Party Images - will not be built
alertmanager_image_source: docker.io/prom/alertmanager
alertmanager_tag: v0.25.0
alpine_image_source: quay.io/vqcomms/alpine-tools
alpine_tag: 3.8.0
artifactory_image_source: docker.bintray.io/jfrog/artifactory-oss
artifactory_tag: 7.27.3
awx_image_source: quay.io/ansible/awx
awx_runner_image_source: quay.io/ansible/ansible-runner
awx_runner_tag: stable-2.11-devel
awx_tag: 19.4.0
bbblivestream_image_source: docker.io/aauzid/bigbluebutton-livestreaming
bbblivestream_tag: latest
busybox_image_source: docker.io/busybox
busybox_tag: latest
certmanagerexporter_image_source: docker.io/joeelliott/cert-exporter
certmanagerexporter_tag: v2.7.0
clair_image_source: quay.io/coreos/clair
clair_tag: v2.1.4
etcd_image_address: quay.io/coreos/etcd:v3.5.0
fluentd_image_source: docker.io/fluent/fluentd-kubernetes-daemonset
fluentd_tag: v1.14.5-debian-elasticsearch7-1.1
grafana_image_source: docker.io/grafana/grafana
grafana_tag: 9.3.6
haproxy_image_source: docker.io/haproxy
haproxy_tag: 2.5.5
jenkins_image_source: quay.io/openshift/origin-jenkins
jenkins_tag: 4.8.0
jicofo_image_source: docker.io/jitsi/jicofo
jicofo_tag: stable-5963
jitsi_image_source: docker.io/jitsi/web
jitsi_tag: stable-5963
jvb_image_source: docker.io/jitsi/jvb
jvb_tag: stable-5963
jvbexporter_image_source: docker.io/systemli/prometheus-jitsi-meet-exporter
jvbexporter_tag: 1.1.6
kubestatemetrics_image_source: k8s.gcr.io/kube-state-metrics/kube-state-metrics
kubestatemetrics_tag: v2.5.0
loki_image_source: docker.io/grafana/loki
loki_tag: 2.6.1
mariadb_image_source: docker.io/mariadb
mariadb_tag: 10.7
memcachedexporter_image_source: quay.io/prometheus/memcached-exporter
memcachedexporter_tag: v0.9.0
memcached_image_source: docker.io/memcached
memcached_tag: 1.6.12
minio_console_image_source: docker.io/minio/console
minio_console_tag: v0.20.5
minio_image_source: docker.io/minio/minio
minio_tag: RELEASE.2022-09-25T15-44-53Z
miniomc_image_source: docker.io/minio/mc
miniomc_tag: RELEASE.2022-05-09T04-08-26Z
ncexporter_image_source: docker.io/xperimental/nextcloud-exporter
ncexporter_tag: 0.5.1
nginxexporter_image_source: docker.io/nginx/nginx-prometheus-exporter
nginxexporter_tag: 0.9.0
nodeexporter_image_source: quay.io/prometheus/node-exporter
nodeexporter_tag: v1.3.1
oauthproxy_image_source: docker.io/openshift/oauth-proxy
oauthproxy_tag: latest
postgres_image_source: docker.io/postgres
postgres_tag: 12
prometheus_image_source: docker.io/prom/prometheus
prometheus_tag: v2.42.0
prosody_image_source: docker.io/jitsi/prosody
prosody_tag: stable-5963
registryexporter_image_source: registry.gitlab.com/synacksynack/opsperator/docker-registryexporter
registryexporter_tag: 1.0.1
registryconsole_image_source: docker.io/joxit/docker-registry-ui
registryconsole_tag: 2.3.3-debian
registry_image_source: docker.io/registry
registry_tag: 2.8.1
scaleliteexporter_image_source: docker.io/greenstatic/bigbluebutton-exporter
scaleliteexporter_tag: v0.6.0
teamsproxy_image_source: quay.io/prometheusmsteams/prometheus-msteams
teamsproxy_tag: v1.5.0
thanos_image_source: quay.io/thanos/thanos
thanos_tag: v0.26.0
trivy_image_source: docker.io/jitesoft/trivy
trivy_tag: 0.28.0

# BlueMind integration
bluemind_api_host: bluemind.demo.local
bluemind_proto: https

# Cassandra
cassandra_cluster_name: kube
cassandra_data_capacity: 20Gi
cassandra_dc: DC1
cassandra_max_heap_size: 256M
cassandra_new_heap_size: 64M
cassandra_rack: RACK1
cassandra_replicas: 3

# Loki
loki_auth_enabled: false
loki_data_capacity: 50Gi
loki_replicas: 1
loki_s3_bucket: loki
loki_s3_flavor: minio
loki_storage: fs
# needs s3 or cassandra storage for HA
# loki_storage currently controls both chunks & index storage configuration

# NetworkPolicies
default_namespace_match: default
ingress_filter_method: none
ingress_nodes_selector: node-role.kubernetes.io/infra=true
network_policy_deployment_label: name
network_policy_namespace_label: netpol

# OpenShift DeploymentConfigs
deploy_pod_cpu_limit: 50m
deploy_pod_cpu_request: 50m
deploy_pod_memory_limit: 512Mi
deploy_pod_memory_request: 128Mi
deploy_revision_history_limit: 3

# Main Toggles
do_backups: False
do_build_triggers: True
do_debugs: False
do_exporters: False
do_network_policy: False
do_pdb: False
do_podsecuritypolicy: True
do_triggers: False

# ElasticSearch
elasticsearch_data_volume_capacity: 50Gi
elasticsearch_default_index_replicas: 1
elasticsearch_replicas: 1
esjob_cpu_limit: 100m
esjob_cpu_request: 10m
esjob_memory_limit: 128Mi
esjob_memory_request: 64Mi

# Etcd
etcd_cpu_limit: 500m
etcd_data_volume_capacity: 8Gi
etcd_memory_limit: 1Gi
etcd_replicas: 3

# HAProxy
haproxy_cpu_limit: 100m
haproxy_cpu_request: 50m
haproxy_memory_limit: 256Mi
haproxy_memory_request: 128Mi
haproxy_replicas: 2

# Kubernetes
k8s_affinity_zone_label: kubernetes.io/hostname
k8s_namespace_nodes_selectors:
- kubernetes.io/os=linux
#- node-role.kubernetes.io/worker=
k8s_registry_ns: registry
k8s_priorityclass_base_priority: 500000000
k8s_resource_nodes_selectors: []
k8s_unprivileged_userid: 1000
registry_name: registry

# Memcached
memcached_replicas: 3

# MongoDB
mongodb_replicas: 1

# OpenSearch
opensearch_data_volume_capacity: 50Gi
opensearch_default_index_replicas: 1
opensearch_replicas: 1
osjob_cpu_limit: 100m
osjob_cpu_request: 10m
osjob_memory_limit: 128Mi
osjob_memory_request: 64Mi

# Percona
percona_replicas: 3

# Postgres
postgres_extensions: []
postgres_max_connections: "100"
postgres_shared_buffers: 12MB
postgres_upgrade_method: copy

# Prometheus Exporters
exporter_cpu_limit: 100m
exporter_cpu_request: 10m
exporter_memory_limit: 128Mi
exporter_memory_request: 64Mi
prometheus_match: scrape-me
prometheus_namespace_match: prometheus-monitoring
prometheus_s3_label: k8s-s3
prometheus_service_label: k8s-app
s3exporter_cpu_limit: 200m
s3exporter_cpu_request: 100m
s3exporter_memory_limit: 768Mi
s3exporter_memory_request: 256Mi

# Tekton
pipeline_sa: tkn-ci
tekton_preferred_builder: kaniko # should allow custom in ALL CRD
trigger_sa: tkn-trigger

# Redis
redis_cpu_limit: 100m
redis_data_capacity: 8Gi
redis_memory_limit: 256Mi
redis_quorum: 2
redis_replicas: 3
sentinel_cpu_limit: 100m
sentinel_memory_limit: 256Mi

# s3 Cleanup
s3cleanup_cpu_limit: 100m
s3cleanup_cpu_request: 10m
s3cleanup_memory_limit: 768Mi
s3cleanup_memory_request: 256Mi

# Services Topology Affinities
service_topology_node_label: kubernetes.io/hostname
service_topology_region_label: topology.kubernetes.io/region
service_topology_zone_label: topology.kubernetes.io/zone

# x509
ca_country: FR
ca_keylen: 4096
ca_loc: Paris
ca_org: K8S
ca_ou: Opsperator
ca_st: France
ca_validity: 3650
internalca_renew_before: 1w1d
svc_keylen: 2048
svc_validity: 365
selfsigned_renew_before: 1w1d
tls_provider: internalca

# Valid Ingress Controllers - definitely incomplete
valid_ingress_controllers:
  getambassador.io/ingress-controller: ambassador
  k8s.io/ingress-nginx: nginx
  traefik.io/ingress-controller: traefik

# Valid Storage Provisioners - definitely incomplete
valid_rwo_provisioners:
- ceph.com/rbd
- cinder.csi.openstack.org
- disk.csi.azure.com
- ebs.csi.aws.com
- kubernetes.io/aws-ebs
- kubernetes.io/azure-disk
- kubernetes.io/cinder
- kubernetes.io/gce-pd
- kubernetes.io/rbd
- org.gluster.glustervirtblock
- pd.csi.storage.gke.io
- rbd.csi.ceph.com
valid_rwx_provisioners:
- ceph.com/cephfs
- cephfs.csi.ceph.com
- kubernetes.io/azure-file
- org.gluster.glusterfs

# ZooKeeper
zookeeper_cpu_limit: 500m
zookeeper_data_capacity: 8Gi
zookeeper_memory_limit: 1Gi
zookeeper_purge_interval: 1
zookeeper_snap_retain_count: 3
zookeeper_init_limit: 5
zookeeper_max_clients: 60
zookeeper_sync_limit: 2
zookeeper_tick_time: 2000
zookeeper_heap_size:
- -Xmx960M
- -Xms960M
zookeeper_replicas: 3
