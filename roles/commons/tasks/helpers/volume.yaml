- block:
  - name: "Lookups {{ vol_name }} PVC"
    async: 30
    changed_when: False
    failed_when: False
    k8s_info:
      api_version: v1
      kind: PersistentVolumeClaim
      namespace: "{{ namespace }}"
      name: "{{ vol_name }}"
    no_log: True
    poll: 5
    register: has_pvc
  - block:
    - block:
      - name: "Installs PVC {{ vol_name }} ({{ vol_size }})"
        async: 30
        k8s:
          definition: "{{ lookup('template', 'volume.j2') }}"
        poll: 5
        register: newvol
      - include_role:
          name: commons
          tasks_from: helpers/wait-for.yaml
        vars:
          check_with: "{{ lookup('k8s', api_version='v1',
                                 kind='PersistentVolumeClaim',
                                 namespace=namespace,
                                 resource_name=vol_name)
                                 | default('') }}"
          obj_name: "{{ vol_name }}"
          retries_for: "{{ (vol_wait_retries_for | default(5)) | int }}"
          wait_for: "{{ (vol_wait_for | default(10)) | int }}"
        when:
        - newvol is defined
        - newvol is changed
        - >
            (rwo_storage is defined
             and (vol_access_mode | default('ReadWriteOnce')) == 'ReadWriteOnce'
             and (rwo_binding_mode | default('Immediate')) == 'Immediate')
            or (rwx_storage is defined
                and (vol_access_mode | default('ReadWriteOnce')) == 'ReadWriteMany'
                and (rwx_binding_mode | default('Immediate')) == 'Immediate')
      when:
      - (has_pvc.resources | length) == 0
    - block:
      - name: Resolves Requested and Existing Volume Sizes
        set_fact:
          left_hr_gi: "{{ vol_size | regex_replace('^([0-9]*)Gi$', '\\1') }}"
          left_hr_g: "{{ vol_size | regex_replace('^([0-9]*)G$', '\\1') }}"
          left_hr_mi: "{{ vol_size | regex_replace('^([0-9]*)Mi$', '\\1') }}"
          left_hr_m: "{{ vol_size | regex_replace('^([0-9]*)M$', '\\1') }}"
          right_hr_gi: "{{ has_pvc.resources[0].resources.requests.storage | regex_replace('^([0-9]*)Gi$', '\\1') }}"
          right_hr_g: "{{ has_pvc.resources[0].resources.requests.storage | regex_replace('^([0-9]*)G$', '\\1') }}"
          right_hr_mi: "{{ has_pvc.resources[0].resources.requests.storage | regex_replace('^([0-9]*)Mi$', '\\1') }}"
          right_hr_m: "{{ has_pvc.resources[0].resources.requests.storage | regex_replace('^([0-9]*)M$', '\\1') }}"
      - name: Convert Values to Kbits
        set_fact:
          left_num: "{{ (left_hr_m | int * 1000) if (left_hr_m is regex('^[0-9]+$'))
                                                 else ((left_hr_mi | int * 1024) if (left_hr_mi is regex('^[0-9]+$'))
                                                                                 else ((left_hr_g | int * 1000000) if (left_hr_g is regex('^[0-9]+$'))
                                                                                                                   else ((left_hr_gi | int * 1024 * 1024) if (left_hr_gi is regex('^[0-9]+$'))
                                                                                                                                                          else -1))) }}"
          right_num: "{{ (right_hr_m | int * 1000) if (right_hr_m is regex('^[0-9]+$'))
                                                   else ((right_hr_mi | int * 1024) if (right_hr_mi is regex('^[0-9]+$'))
                                                                                    else ((right_hr_g | int * 1000000) if (right_hr_g is regex('^[0-9]+$'))
                                                                                                                       else ((right_hr_gi | int * 1024 * 1024) if (right_hr_gi is regex('^[0-9]+$'))
                                                                                                                                                               else -1))) }}"
      - name: "Resizes PVC {{ vol_name }} ({{ vol_size }})"
        async: 30
        k8s:
          definition: "{{ lookup('template', 'volume.j2') }}"
        poll: 5
        register: rszvol
        when:
        - (left_num | int) > (right_num | int)
      when:
      - (has_pvc.resources | length) > 0
      - has_pvc.resources[0].resources is defined
      - has_pvc.resources[0].resources.requests is defined
      - has_pvc.resources[0].resources.requests.storage is defined
      - vol_size != has_pvc.resources[0].resources.requests.storage
      - >
          (vol_access_mode | default('ReadWriteOnly') == 'ReadWriteOnly'
           and rwo_can_expand | default(False))
          or (vol_access_mode | default('') == 'ReadWriteMany'
              and rwx_can_expand | default(False))
    when:
    - vol_size is defined
    - has_pvc is defined
    - has_pvc.resources is defined
  when:
  - vol_name is defined
  - (vol_state | default('present')) == 'present'
