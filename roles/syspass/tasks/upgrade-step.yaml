- block:
  - include_role:
      name: commons
      tasks_from: helpers/volume.yaml
    vars:
      vol_name: "{{ spms_name if (syspass_db_type != 'percona')
                              else (sppc_name ~ '-client') }}-{{ target }}-snap"
      vol_size: "{{ syspass_db_data_capacity }}"
  - name: Marks operation in progress
    set_fact:
      snap_failed: True
    when:
    - has_pvc is defined
    - has_pvc.resources is defined
    - (has_pvc.resources | length) > 0
  - name: Scales SysPass down to 0
    async: 30
    k8s:
      definition:
        apiVersion: "{{ deployment_apivers }}"
        kind: "{{ deployment_kind }}"
        metadata:
          name: "{{ syspass_name }}"
          namespace: "{{ namespace }}"
        spec:
          replicas: 0
    poll: 5
    when:
    - (snap_failed | default(False)) == False
  - include_role:
      name: commons
      tasks_from: snapshots/mysql.yaml
    vars:
      db_data_capacity: "{{ syspass_db_data_capacity }}"
      db_host: "{{ spms_name if (syspass_db_type != 'percona')
                             else (sppc_name ~ '-client') }}"
      db_secret_name: "{{ spms_name if (syspass_db_type != 'percona')
                                    else sppc_name }}"
      job_assume_pod_netpol: "{{ syspass_name }}"
      provision_volume: False
      snap_mark: "{{ target }}"
  - name: Lookups MySQL Snapshot Job
    async: 30
    changed_when: False
    k8s_info:
      api_version: batch/v1
      kind: Job
      namespace: "{{ namespace }}"
      name: "{{ spms_name if (syspass_db_type != 'percona')
                          else (sppc_name ~ '-client') }}-{{ target }}-snap"
    no_log: True
    poll: 5
    register: mysql_snap
    when:
    - (snap_failed | default(False)) == False
  - block:
    - include_role:
        name: commons
        tasks_from: snapshots/fs.yaml
      vars:
        data_capacity: "{{ syspass_data_capacity }}"
        snap_mark: "{{ target }}"
        source_pvc: "{{ syspass_name }}"
    - name: Lookups SysPass Job
      async: 30
      changed_when: False
      k8s_info:
        api_version: batch/v1
        kind: Job
        namespace: "{{ namespace }}"
        name: "{{ syspass_name }}-{{ target }}-snap"
      no_log: True
      poll: 5
      register: sp_snap
      when:
      - (snap_failed | default(False)) == False
    - name: Disables snapshots for further SysPass upgrades
      set_fact:
        syspass_snapshot_data_on_upgrade: False
    when:
    - (snap_failed | default(False)) == False
    - syspass_snapshot_data_on_upgrade | default(True)
  - block:
    - name: Reverts Upgrade when snapshot failed
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ syspass_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
    - name: Fails when snapshot failed
      fail:
        msg: "Failed upgrading SysPass to {{ target }}"
    when:
    - >
        snap_failed | default(False)
        or (mysql_snap is defined
            and mysql_snap.resources is defined
            and (mysql_snap.resources | length == 0
                 or (mysql_snap.resources[0].status is defined
                     and mysql_snap.resources[0].status is defined
                     and (mysql_snap.resources[0].status.completionTime is not defined
                          or (not ((mysql_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
        or (sp_snap is defined
            and sp_snap.resources is defined
            and (sp_snap.resources | length == 0
                 or (sp_snap.resources | length > 0
                     and sp_snap.resources[0].status is defined
                     and (sp_snap.resources[0].status.completionTime is not defined
                          or (not ((sp_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
  when:
  - do_backups
- block:
  - include_role:
      name: commons
      tasks_from: helpers/deployment.yaml
    vars:
      cone_cpu_limit: "{{ sshd_cpu_limit }}"
      cone_cpu_request: "{{ sshd_cpu_request }}"
      cone_idx: "{{ 2 if (do_backups
                          and do_exporters)
                      else (1 if (do_backups)
                              else 0) }}"
      cone_memory_limit: "{{ sshd_memory_limit }}"
      cone_memory_request: "{{ sshd_memory_request }}"
      cone_tag: "{{ sshd_tag }}"
      cpu_limit: "{{ syspass_cpu_limit }}"
      cpu_request: "{{ syspass_cpu_request }}"
      dc_data: "{{ lookup('template', 'syspass.j2') }}"
      dc_name: "{{ syspass_name }}"
      dc_replicas: 1
      dc_tag: "{{ syspass_tag }}"
      exporter_idx: 1
      exporter_tag: "{{ httpexporter_tag }}"
      memory_limit: "{{ syspass_memory_limit }}"
      memory_request: "{{ syspass_memory_request }}"
  - include_role:
      name: commons
      tasks_from: helpers/wait-for.yaml
    vars:
      check_with: "{{ lookup('k8s', api_version=deployment_apivers,
                             kind=deployment_kind, namespace=namespace,
                             resource_name=syspass_name) | default('') }}"
      obj_name: "{{ syspass_name }}"
      retries_for: 60
      wait_for: 10
    when:
    - dc_started is defined
    - dc_started is changed
  - name: Checks SysPass Upgrade Status
    async: 30
    changed_when: False
    k8s_info:
      api_version: "{{ deployment_apivers }}"
      kind: "{{ deployment_kind }}"
      name: "{{ syspass_name }}"
      namespace: "{{ namespace }}"
    no_log: True
    poll: 5
    register: has_sp
  - block:
    - name: Fails when upgrade is still running
      fail:
        msg: "Failed upgrading SysPass to {{ target }}"
      when:
      - has_sp.resources[0].status.readyReplicas == 0
    - include_role:
        name: commons
        tasks_from: helpers/deployment.yaml
      vars:
        cone_cpu_limit: "{{ sshd_cpu_limit }}"
        cone_cpu_request: "{{ sshd_cpu_request }}"
        cone_idx: "{{ 2 if (do_backups
                            and do_exporters)
                        else (1 if (do_backups)
                                else 0) }}"
        cone_memory_limit: "{{ sshd_memory_limit }}"
        cone_memory_request: "{{ sshd_memory_request }}"
        cone_tag: "{{ sshd_tag }}"
        cpu_limit: "{{ syspass_cpu_limit }}"
        cpu_request: "{{ syspass_cpu_request }}"
        dc_data: "{{ lookup('template', 'syspass.j2') }}"
        dc_name: "{{ syspass_name }}"
        dc_replicas: "{{ syspass_replicas }}"
        dc_tag: "{{ syspass_tag }}"
        exporter_idx: 1
        exporter_tag: "{{ httpexporter_tag }}"
        memory_limit: "{{ syspass_memory_limit }}"
        memory_request: "{{ syspass_memory_request }}"
      when:
      - syspass_replicas not in [ '1', 1 ]
      - has_sp.resources[0].status.readyReplicas > 0
    when:
    - has_sp is defined
    - has_sp.resources is defined
    - has_sp.resources | length > 0
    - has_sp.resources[0].status is defined
    - has_sp.resources[0].status.readyReplicas is defined
  when:
  - not (snap_failed | default(False))
