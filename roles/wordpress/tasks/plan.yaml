- include_role:
    name: backups
    tasks_from: wait.yaml
  vars:
    check_only: True
- include_role:
    name: directory
    tasks_from: wait.yaml
  vars:
    check_only: True
    in_namespace: "{{ auth_namespace | default(namespace) }}"
    obj_selectors: "{{ directory_selectors | default([ 'opsperator.io/' ~ cr_kind ~ '=' ~ cr_name ]) }}"
- include_role:
    name: postfix
    tasks_from: wait.yaml
  vars:
    check_only: True
    in_namespace: "{{ postfix_namespace | default(namespace) }}"
    obj_selectors: "{{ postfix_selectors | default([ 'opsperator.io/' ~ cr_kind ~ '=' ~ cr_name ]) }}"
  when:
  - smtp_relay is not defined
- include_role:
    name: squid
    tasks_from: wait.yaml
  vars:
    check_only: True
    in_namespace: "{{ squid_namespace | default(namespace) }}"
    obj_selectors: "{{ squid_selectors | default([ 'opsperator.io/' ~ cr_kind ~ '=' ~ cr_name ]) }}"
- block:
  - include_role:
      name: prometheus
      tasks_from: check.yaml
    vars:
      check_only: True
  - block:
    - name: Sets WordPress Prometheus Facts
      set_fact:
        dashboards_for_deployments:
        - deployments:
          - "{{ wordpress_name }}"
          product: apache
        - deployments:
          - "{{ wpms_name if (wordpress_db_type in [ 'mariadb', 'mysql' ]) else False }}"
          - "{{ wppc_name if (wordpress_db_type == 'percona')              else False }}"
          product: mysql
        dashboards_for_namespace: "{{ namespace }}"
    when:
    - grafana_public_address | default(False)
    - grafana_public_address != 'not deployed'
  when:
  - do_exporters
  - do_status_dashboards | default(False)
- name: Sets WordPress Directory Facts
  set_fact:
    do_ldap: "{{ True if (ldap_address is defined
                          and directory_crd.resources | length > 0
                          and directory_crd.resources[0].spec is defined
                          and directory_crd.resources[0].spec.do_wordpress | default(False))
                      else False }}"
    do_lemon: "{{ True if (lemon_manager is defined
                           and lemon_manager != 'not deployed'
                           and directory_crd.resources | length > 0
                           and directory_crd.resources[0].spec is defined
                           and directory_crd.resources[0].spec.do_wordpress | default(False))
                       else False }}"
    wordpress_fqdn: "{{ (wordpress_force_fqdn | default(False))
                          if (wordpress_force_fqdn | default(False))
                          else ((directory_crd.resources[0].spec.directory_fqdn_wordpress | default(False))
                                if (directory_cr is defined
                                    and directory_crd.resources is defined
                                    and directory_crd.resources | length > 0
                                    and directory_crd.resources[0].spec is defined
                                    and directory_crd.resources[0].spec.directory_fqdn_wordpress is defined
                                    and directory_crd.resources[0].spec.do_wordpress | default(False))
                                else ('blog.' ~ root_domain)) }}"
- block:
  - name: Fixes Directory WordPress FQDN
    async: 30
    k8s:
      definition:
        apiVersion: wopla.io/v1beta1
        kind: Directory
        metadata:
          name: "{{ directory_crd.resources[0].metadata.name }}"
          namespace: "{{ ldap_namespace }}"
        spec:
          directory_fqdn_wordpress: "{{ wordpress_fqdn }}"
    when:
    - ldap_root_domain is defined
    - ldap_root_domain != root_domain
    - directory_cr is defined
    - directory_crd.resources is defined
    - directory_crd.resources | length > 0
    - directory_crd.resources[0].spec is defined
    - (directory_crd.resources[0].spec.directory_fqdn_wordpress | default('blog.' ~ ldap_root_domain)) != wordpress_fqdn
  - block:
    - include_role:
        name: directory
        tasks_from: client-config.yaml
      vars:
        config_namespace: "{{ namespace }}"
    - include_role:
        name: directory
        tasks_from: secret-dup.yaml
      vars:
        get_keys:
        - directory-root
        - wordpress-password
    when:
    - ldap_namespace != namespace
  - include_role:
      name: commons
      tasks_from: ssl/ca-init.yaml
    vars:
      tls_provider: "{{ directory_tls_provider }}"
    when:
    - k8s_flavor == 'kubernetes'
    - directory_tls_provider != tls_provider
  when:
  - do_ldap or do_lemon
- block:
  - include_role:
      name: commons
      tasks_from: ssl/ca-init.yaml
    vars:
      tls_provider: "{{ smtp_tls_provider }}"
    when:
    - smtps_address | default(False)
    - smtp_tls_provider is defined
    - (smtp_tls_provider | default(tls_provider)) != tls_provider
    - (smtp_tls_provider | default(tls_provider)) != (directory_tls_provider | default(tls_provider))
  - include_role:
      name: commons
      tasks_from: ssl/ca-init.yaml
  when:
  - k8s_flavor == 'kubernetes'
- name: "Lookups WordPress {{ deployment_kind }}"
  async: 30
  changed_when: False
  failed_when: False
  k8s_info:
    api_version: "{{ deployment_apivers }}"
    kind: "{{ deployment_kind }}"
    namespace: "{{ namespace }}"
    name: "{{ wordpress_name }}"
  no_log: True
  poll: 5
  register: wp_dc
