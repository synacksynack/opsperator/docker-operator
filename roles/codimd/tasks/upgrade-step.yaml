- block:
  - include_role:
      name: commons
      tasks_from: helpers/volume.yaml
    vars:
      vol_name: "{{ cdmdms_name if (codimd_db_type in [ 'mariadb', 'mysql' ])
                                else ((cdmdpc_name ~ '-client')
                                      if (codimd_db_type == 'percona')
                                      else cdmdpg_name) }}-{{ target }}-snap"
      vol_size: "{{ codimd_db_data_capacity }}"
  - name: Marks operation in progress
    set_fact:
      snap_failed: True
    when:
    - has_pvc is defined
    - has_pvc.resources is defined
    - (has_pvc.resources | length) > 0
  - name: Scales CodiMD down to 0
    async: 30
    k8s:
      definition:
        apiVersion: "{{ deployment_apivers }}"
        kind: "{{ deployment_kind }}"
        metadata:
          name: "{{ codimd_name }}"
          namespace: "{{ namespace }}"
        spec:
          replicas: 0
    poll: 5
    when:
    - (snap_failed | default(False)) == False
  - block:
    - include_role:
        name: commons
        tasks_from: snapshots/mysql.yaml
      vars:
        db_data_capacity: "{{ codimd_db_data_capacity }}"
        db_host: "{{ cdmdms_name if (codimd_db_type != 'percona')
                                 else (cdmdpc_name ~ '-client') }}"
        db_secret_name: "{{ cdmdms_name if (codimd_db_type != 'percona')
                                        else cdmdpc_name }}"
        job_assume_pod_netpol: "{{ codimd_name }}"
        provision_volume: False
        snap_mark: "{{ target }}"
    - name: Lookups MySQL Snapshot Job
      async: 30
      changed_when: False
      k8s_info:
        api_version: batch/v1
        kind: Job
        namespace: "{{ namespace }}"
        name: "{{ cdmdms_name if (codimd_db_type != 'percona')
                              else (cdmdpc_name ~ '-client') }}-{{ target }}-snap"
      no_log: True
      poll: 5
      register: mysql_snap
      when:
      - (snap_failed | default(False)) == False
    when:
    - codimd_db_type in [ 'percona', 'mariadb', 'mysql' ]
  - block:
    - include_role:
        name: commons
        tasks_from: snapshots/postgres.yaml
      vars:
        db_data_capacity: "{{ codimd_db_data_capacity }}"
        db_host: "{{ cdmdpg_name }}"
        db_secret_name: "{{ cdmdpg_name }}"
        job_assume_pod_netpol: "{{ codimd_name }}"
        provision_volume: False
        snap_mark: "{{ target }}"
    - name: Lookups Postgres Snapshot Job
      async: 30
      changed_when: False
      k8s_info:
        api_version: batch/v1
        kind: Job
        namespace: "{{ namespace }}"
        name: "{{ cdmdpg_name }}-{{ target }}-snap"
      no_log: True
      poll: 5
      register: pg_snap
      when:
      - (snap_failed | default(False)) == False
    when:
    - codimd_db_type == 'postgres'
  - block:
    - include_role:
        name: commons
        tasks_from: snapshots/fs.yaml
      vars:
        data_capacity: "{{ codimd_data_capacity }}"
        snap_mark: "{{ target }}"
        source_pvc: "{{ codimd_name }}"
    - name: Lookups CodiMD Job
      async: 30
      changed_when: False
      k8s_info:
        api_version: batch/v1
        kind: Job
        namespace: "{{ namespace }}"
        name: "{{ codimd_name }}-{{ target }}-snap"
      no_log: True
      poll: 5
      register: cdmd_snap
      when:
      - (snap_failed | default(False)) == False
    - name: Disables snapshots for further CodiMD upgrades
      set_fact:
        codimd_snapshot_data_on_upgrade: False
    when:
    - (snap_failed | default(False)) == False
    - codimd_snapshot_data_on_upgrade | default(True)
    - not (do_s3 or codimd_imgur_client_id)
  - block:
    - name: Reverts Upgrade when snapshot failed
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ codimd_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
    - name: Fails when snapshot failed
      fail:
        msg: "Failed upgrading CodiMD to {{ target }}"
    when:
    - >
        snap_failed | default(False)
        or (mysql_snap is defined
            and codimd_db_type in [ 'percona', 'mariadb', 'mysql' ]
            and mysql_snap.resources is defined
            and (mysql_snap.resources | length == 0
                 or (mysql_snap.resources[0].status is defined
                     and mysql_snap.resources[0].status is defined
                     and (mysql_snap.resources[0].status.completionTime is not defined
                          or (not ((mysql_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
        or (pg_snap is defined
            and codimd_db_type == 'postgres'
            and pg_snap.resources is defined
            and (pg_snap.resources | length == 0
                 or (pg_snap.resources[0].status is defined
                     and pg_snap.resources[0].status is defined
                     and (pg_snap.resources[0].status.completionTime is not defined
                          or (not ((pg_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
        or ((not (do_s3 or codimd_imgur_client_id))
            and cdmd_snap is defined
            and cdmd_snap.resources is defined
            and (cdmd_snap.resources | length == 0
                 or (cdmd_snap.resources | length > 0
                     and cdmd_snap.resources[0].status is defined
                     and (cdmd_snap.resources[0].status.completionTime is not defined
                          or (not ((cdmd_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
  when:
  - do_backups
- block:
  - include_role:
      name: commons
      tasks_from: helpers/deployment.yaml
    vars:
      cone_cpu_limit: "{{ sshd_cpu_limit }}"
      cone_cpu_request: "{{ sshd_cpu_request }}"
      cone_idx: "{{ 1 if (do_backups
                          and not codimd_imgur_client_id
                          and not do_s3)
                      else 0 }}"
      cone_memory_limit: "{{ sshd_memory_limit }}"
      cone_memory_request: "{{ sshd_memory_request }}"
      cone_tag: "{{ sshd_tag }}"
      cpu_limit: "{{ codimd_cpu_limit }}"
      cpu_request: "{{ codimd_cpu_request }}"
      dc_data: "{{ lookup('template', 'codimd.j2') }}"
      dc_name: "{{ codimd_name }}"
      dc_replicas: 1
      dc_tag: "{{ target }}"
      memory_limit: "{{ codimd_memory_limit }}"
      memory_request: "{{ codimd_memory_request }}"
    when:
    - (snap_failed | default(False)) == False
  - include_role:
      name: commons
      tasks_from: helpers/wait-for.yaml
    vars:
      check_with: "{{ lookup('k8s', api_version=deployment_apivers,
                             kind=deployment_kind, namespace=namespace,
                             resource_name=codimd_name) | default('') }}"
      obj_name: "{{ codimd_name }}"
      retries_for: 60
      wait_for: 10
    when:
    - (snap_failed | default(False)) == False
    - dc_started is defined
    - dc_started is changed
  - name: Checks CodiMD Upgrade Status
    async: 30
    changed_when: False
    k8s_info:
      api_version: "{{ deployment_apivers }}"
      kind: "{{ deployment_kind }}"
      name: "{{ codimd_name }}"
      namespace: "{{ namespace }}"
    no_log: True
    poll: 5
    register: has_cdmd
    when:
    - (snap_failed | default(False)) == False
  - block:
    - name: Fails when upgrade is still running
      fail:
        msg: "Failed upgrading CodiMD to {{ target }}"
      when:
      - has_cdmd.resources[0].status.readyReplicas == 0
    #- include_role:
    #    name: commons
    #    tasks_from: helpers/deployment.yaml
    #  vars:
    #    cone_cpu_limit: "{{ sshd_cpu_limit }}"
    #    cone_cpu_request: "{{ sshd_cpu_request }}"
    #    cone_idx: "{{ 1 if (do_backups
    #                        and not codimd_imgur_client_id
    #                        and not do_s3)
    #                    else 0 }}"
    #    cone_memory_limit: "{{ sshd_memory_limit }}"
    #    cone_memory_request: "{{ sshd_memory_request }}"
    #    cone_tag: "{{ sshd_tag }}"
    #    cpu_limit: "{{ codimd_cpu_limit }}"
    #    cpu_request: "{{ codimd_cpu_request }}"
    #    dc_data: "{{ lookup('template', 'codimd.j2') }}"
    #    dc_name: "{{ codimd_name }}"
    #    dc_replicas: "{{ codimd_replicas }}"
    #    dc_tag: "{{ target }}"
    #    memory_limit: "{{ codimd_memory_limit }}"
    #    memory_request: "{{ codimd_memory_request }}"
    #  when:
    #  - codimd_replicas not in [ '1', 1 ]
    #  #- (has_pvc.resources | default([])) | length > 0
    #  #- has_pvc.resources[0].spec.accessModes[0] == 'ReadWriteMany'
    #  - has_cdmd.resources[0].status.readyReplicas > 0
    when:
    - has_cdmd is defined
    - has_cdmd.resources is defined
    - has_cdmd.resources | length > 0
    - has_cdmd.resources[0].status is defined
    - has_cdmd.resources[0].status.readyReplicas is defined
  when:
  - not (snap_failed | default(False))
