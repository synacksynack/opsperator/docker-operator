- include_role:
    name: directory
    tasks_from: wait.yaml
  vars:
    check_only: True
    in_namespace: "{{ auth_namespace | default(namespace) }}"
    obj_selectors: "{{ directory_selectors | default([ 'opsperator.io/' ~ cr_kind ~ '=' ~ cr_name ]) }}"
- block:
  - include_role:
      name: prometheus
      tasks_from: check.yaml
    vars:
      check_only: True
  - block:
    - name: Sets MinIO Prometheus Facts
      set_fact:
        dashboards_for_deployments:
        - deployments:
          - "{{ minio_name }}"
          product: minio
        dashboards_for_namespace: "{{ namespace }}"
    when:
    - grafana_public_address | default(False)
    - grafana_public_address != 'not deployed'
  when:
  - do_exporters
  - do_status_dashboards | default(False)
- name: Sets MinIO Directory Facts
  set_fact:
    do_fusion: "{{ True if (fusion_address is defined
                            and fusion_address != 'not deployed'
                            and directory_crd.resources | length > 0
                            and directory_crd.resources[0].spec is defined
                            and directory_crd.resources[0].spec.do_fusion | default(False)
                            and directory_crd.resources[0].spec.do_minio | default(False))
                        else False }}"
    do_ldap: "{{ True if (ldap_address is defined
                          and directory_crd.resources | length > 0
                          and directory_crd.resources[0].spec is defined
                          and directory_crd.resources[0].spec.do_minio | default(False))
                      else False }}"
    do_lemon: "{{ True if (lemon_manager is defined
                           and lemon_manager != 'not deployed'
                           and directory_crd.resources | length > 0
                           and directory_crd.resources[0].spec is defined
                           and directory_crd.resources[0].spec.do_minio | default(False))
                       else False }}"
    minio_fqdn: "{{ (minio_force_fqdn | default(False))
                      if (minio_force_fqdn | default(False))
                      else ((directory_crd.resources[0].spec.directory_fqdn_minio | default(False))
                            if (directory_cr is defined
                                and directory_crd.resources is defined
                                and directory_crd.resources | length > 0
                                and directory_crd.resources[0].spec is defined
                                and directory_crd.resources[0].spec.directory_fqdn_minio is defined
                                and directory_crd.resources[0].spec.do_minio | default(False))
                            else ('s3.' ~ root_domain)) }}"
    minioconsole_fqdn: "{{ (minioconsole_force_fqdn | default(False))
                             if (minioconsole_force_fqdn | default(False))
                             else ((directory_crd.resources[0].spec.directory_fqdn_minioconsole | default(False))
                                   if (directory_cr is defined
                                       and directory_crd.resources is defined
                                       and directory_crd.resources | length > 0
                                       and directory_crd.resources[0].spec is defined
                                       and directory_crd.resources[0].spec.directory_fqdn_minioconsole is defined
                                       and directory_crd.resources[0].spec.do_minioconsole | default(False))
                                   else ('minio-console.' ~ root_domain)) }}"
- block:
  - name: Fixes Directory MinIO FQDN
    async: 30
    k8s:
      definition:
        apiVersion: wopla.io/v1beta1
        kind: Directory
        metadata:
          name: "{{ directory_crd.resources[0].metadata.name }}"
          namespace: "{{ ldap_namespace }}"
        spec:
          directory_fqdn_minio: "{{ minio_fqdn }}"
          directory_fqdn_minioconsole: "{{ minioconsole_fqdn }}"
    when:
    - ldap_root_domain is defined
    - ldap_root_domain != root_domain
    - directory_cr is defined
    - directory_crd.resources is defined
    - directory_crd.resources | length > 0
    - directory_crd.resources[0].spec is defined
    - >
        ((directory_crd.resources[0].spec.directory_fqdn_minioconsole | default('minio-console.' ~ ldap_root_domain)) != minioconsole_fqdn
         and minio_do_console)
        or ((directory_crd.resources[0].spec.directory_fqdn_minio | default('s3.' ~ ldap_root_domain)) != minio_fqdn
            and expose_minio)
  - block:
    - include_role:
        name: directory
        tasks_from: secret-dup.yaml
      vars:
        get_keys:
        - directory-root
        - minio-password
    - include_role:
        name: directory
        tasks_from: client-config.yaml
      vars:
        config_namespace: "{{ namespace }}"
    when:
    - ldap_namespace != namespace
  - name: Sets MinIO LDAP Facts
    set_fact:
      useroc: "{{ 'gosaAccount' if (do_fusion | default(False))
                                else minio_ldap_user_objectclass }}"
  - name: Fetches LDAP Secret
    async: 30
    changed_when: False
    failed_when: False
    k8s_info:
      api_version: v1
      kind: Secret
      namespace: "{{ namespace }}"
      name: "{{ ldap_secret_name }}"
    no_log: True
    poll: 5
    register: ldap_secret_data
  - name: Resolves LDAP Directory Root
    set_fact:
      ldap_directory_root: "{{ ldap_secret_data.resources[0].data['directory-root'] | b64decode }}"
    when:
    - ldap_secret_data is defined
    - ldap_secret_data.resources is defined
    - ldap_secret_data.resources | length > 0
    - ldap_secret_data.resources[0].data is defined
    - ldap_secret_data.resources[0].data['directory-root'] | default(False)
  - include_role:
      name: commons
      tasks_from: ssl/ca-init.yaml
    vars:
      tls_provider: "{{ directory_tls_provider }}"
    when:
    - k8s_flavor == 'kubernetes'
    - directory_tls_provider != tls_provider
  when:
  - do_ldap
- include_role:
    name: commons
    tasks_from: ssl/ca-init.yaml
  when:
  - k8s_flavor == 'kubernetes'
- block:
  - include_role:
      name: commons
      tasks_from: helpers/imagestream.yaml
    vars:
      is_import:
      - source: "{{ minio_image_source }}"
        tag: "{{ minio_tag }}"
      is_name: minio
      namespace: "{{ buildns }}"
  - include_role:
      name: commons
      tasks_from: helpers/imagestream.yaml
    vars:
      is_import:
      - source: "{{ minio_console_image_source }}"
        tag: "{{ minio_console_tag }}"
      is_name: minio-console
      namespace: "{{ buildns }}"
  when:
  - k8s_flavor == 'openshift'
- name: Lookups MinIO StatefulSet
  async: 30
  changed_when: False
  failed_when: False
  k8s_info:
    api_version: "{{ sts_apivers }}"
    kind: StatefulSet
    namespace: "{{ namespace }}"
    name: "{{ minio_name }}"
  no_log: True
  poll: 5
  register: mo_dc
