# Operator defaults
allow_rwx_storage: True
cluster_domain: cluster.local
execution_affinity_required: True
pull_policy: IfNotPresent
root_domain: demo.local
scheduling_affinity_required: True
timezone: Europe/Paris

# ARA
ara_playbook_labels:
- Ci
- Opsperator
ara_playbook_name: SweetCi

# Artifactory specifcis
artifactory_cpu_limit: "1"
artifactory_cpu_request: 500m
artifactory_data_capacity: 100Gi
artifactory_db_cpu_limit: 300m
artifactory_db_data_capacity: 16Gi
artifactory_db_memory_limit: 512Mi
artifactory_db_type: postgres
artifactory_image_source: docker.bintray.io/jfrog/artifactory-oss
artifactory_java_options:
- -Xms1g
- -Xmx2g
artifactory_memory_limit: 3Gi
artifactory_memory_request: 2Gi
artifactory_tag: 7.27.3

# LemonLDAP AuthProxy
authproxy_cpu_limit: 50m
authproxy_cpu_request: 10m
authproxy_image_source: https://gitlab.com/synacksynack/opsperator/docker-authproxy
authproxy_memory_limit: 256Mi
authproxy_memory_request: 128Mi

# Clair
clair_cpu_limit: 100m
clair_cpu_request: 10m
clair_db_cpu_limit: 300m
clair_db_data_capacity: 8Gi
clair_db_memory_limit: 512Mi
clair_image_source: quay.io/coreos/clair
clair_memory_limit: 2300Mi
clair_memory_request: 128Mi
clair_tag: v2.1.4

# CI Main Toggles
ci_repos_list: []
# ^ might be dealt with as part of a separate CRD?
do_artifactory: False
do_clair: False
do_directory: False
do_errbit: False
do_exporters: False
do_fusion: False
do_gerrit: False
do_gitea: True
do_gogs: False
do_hpa: False
do_jenkins: True
do_lemon: False
do_network_policy: False
do_nexus: True
do_quotas: True
do_registry: False
do_sonarqube: True
do_tekton: False
do_triggers: False
do_trivy: True

# Errbit
errbit_admin_email: adminci@example.com
errbit_admin_password: secret
errbit_cpu_limit: 200m
errbit_cpu_request: 100m
errbit_db_cpu_limit: 300m
errbit_db_data_capacity: 8Gi
errbit_db_memory_limit: 512Mi
errbit_db_type: postgres
errbit_image_source: https://gitlab.com/synacksynack/opsperator/docker-errbit
errbit_memory_limit: 512Mi
errbit_memory_request: 512Mi
errbit_tag: 0.9.1-20210919

# Directory Fusion
fusion_cpu_limit: 50m
fusion_cpu_request: 10m
fusion_image_source: https://gitlab.com/synacksynack/opsperator/docker-fusiondirectory
fusion_memory_limit: 368Mi
fusion_memory_request: 256Mi

# Gerrit
gerrit_cpu_limit: 500m
gerrit_cpu_request: 300m
gerrit_data_capacity: 20Gi
gerrit_db_cpu_limit: 300m
gerrit_db_data_capacity: 8Gi
gerrit_db_memory_limit: 512Mi
gerrit_db_type: h2
gerrit_image_source: https://gitlab.com/synacksynack/opsperator/docker-gerrit
gerrit_memory_limit: 2Gi
gerrit_memory_request: 2Gi
gerrit_tag: 3.5.1-1

# Gitea
gitea_admin_password: Sec-ret42
gitea_admin_user: Ci
gitea_cpu_limit: 100m
gitea_cpu_request: 10m
gitea_data_capacity: 50Gi
gitea_db_cpu_limit: 300m
gitea_db_data_capacity: 8Gi
gitea_db_memory_limit: 512Mi
gitea_db_type: postgres
gitea_image_source: https://gitlab.com/synacksynack/opsperator/docker-gitea
gitea_memory_limit: 1Gi
gitea_memory_request: 512Mi
gitea_noreply: noreply@example.com
gitea_tag: 1.17.2

# Gogs
gogs_admin_password: Sec-ret42
gogs_admin_user: Ci
gogs_cpu_limit: 100m
gogs_cpu_request: 10m
gogs_data_capacity: 50Gi
gogs_db_cpu_limit: 300m
gogs_db_data_capacity: 8Gi
gogs_db_memory_limit: 512Mi
gogs_db_type: postgres
gogs_image_source: https://gitlab.com/synacksynack/opsperator/docker-gogs
gogs_memory_limit: 1Gi
gogs_memory_request: 512Mi
gogs_noreply: noreply@example.com
gogs_tag: 0.12.10

# Jenkins
jenkins_cpu_limit: 500m
jenkins_cpu_request: 200m
jenkins_data_capacity: 42Gi
jenkins_image_source: quay.io/openshift/origin-jenkins
jenkins_memory_limit: 3Gi
jenkins_memory_request: 1Gi
jenkins_tag: 4.8.0

# Directory OpenLDAP
ldap_cpu_limit: 100m
ldap_cpu_request: 30m
ldap_data_volume_capacity: 8Gi
ldap_debug_level: "0"
ldap_directory_root: dc=demo,dc=local
ldap_image_source: https://gitlab.com/synacksynack/opsperator/docker-openldap
ldap_init_debug_level: "256"
ldap_memory_limit: 512Mi
ldap_memory_request: 384Mi

# Directory LemonLDAP-NG
lemon_cpu_limit: 50m
lemon_cpu_request: 10m
lemon_image_source: https://gitlab.com/synacksynack/opsperator/docker-lemonldap
lemon_memory_limit: 512Mi
lemon_memory_request: 128Mi
lemonjob_cpu_limit: 50m
lemonjob_memory_limit: 64Mi

# NetworkPolicies
default_namespace_match: default
network_policy_deployment_label: name
network_policy_namespace_label: netpol

# Nexus
nexus_admin_password: secret
nexus_cpu_limit: 800m
nexus_cpu_request: 10m
nexus_data_capacity: 50Gi
nexus_image_source: https://gitlab.com/synacksynack/opsperator/docker-nexus
nexus_java_args:
- -Xms1400m
- -Xmx1400m
- -XX:MaxDirectMemorySize=2g
nexus_ldap_backend: ""
nexus_ldap_base: dc=example,dc=com
nexus_ldap_user_prefix: ou=users
nexus_memory_limit: 4200Mi
nexus_memory_request: 3800Mi
nexus_tag: 3.41.1-01

# Prometheus Exporters
exporter_cpu_limit: 100m
exporter_cpu_request: 50m
exporter_memory_limit: 128Mi
exporter_memory_request: 64Mi
prometheus_match: scrape-me
prometheus_namespace_match: prometheus-monitoring
prometheus_service_label: k8s-app

# Registry
registry_cpu_limit: 100m
registry_cpu_request: 20m
registry_data_capacity: 100Gi
registry_image_source: docker.io/registry
registry_memory_limit: 512Mi
registry_memory_request: 128Mi
registry_tag: 2.8.1

# SonarQube
sonarqube_admin_password: secret
sonarqube_cpu_limit: 800m
sonarqube_cpu_request: 50m
sonarqube_data_capacity: 20Gi
sonarqube_db_cpu_limit: 300m
sonarqube_db_data_capacity: 8Gi
sonarqube_db_memory_limit: 512Mi
sonarqube_db_type: postgres
sonarqube_image_source: https://gitlab.com/synacksynack/opsperator/docker-sonarqube
sonarqube_java_options:
- -Xmx512m
- -Xms512m
- -XX:+HeapDumpOnOutOfMemoryError
sonarqube_memory_limit: 2Gi
sonarqube_memory_request: 1Gi
sonarqube_tag: 9.6.1.59531

# Directory ServiceDesk
servicedesk_cpu_limit: 50m
servicedesk_cpu_request: 10m
servicedesk_image_source: https://gitlab.com/synacksynack/opsperator/docker-servicedesk
servicedesk_memory_limit: 128Mi
servicedesk_memory_request: 64Mi

# Directory SelfServicePassword
ssp_cpu_limit: 50m
ssp_cpu_request: 10m
ssp_image_source: https://gitlab.com/synacksynack/opsperator/docker-selfservicepassword
ssp_memory_limit: 128Mi
ssp_memory_request: 64Mi

# Tekton
npmjs_registry: https://registry.npmjs.org
pipeline_sa: tkn-ci
tekton_buildah_image: quay.io/buildah/stable:v1.21.0
tekton_buildkit_image: docker.io/moby/buildkit:v0.6.2
tekton_img_image: r.j3ss.co/img
tekton_k8s_client_image: gcr.io/cloud-builders/kubectl:latest
tekton_k8s_version: 0.29.0
tekton_kaniko_image: gcr.io/kaniko-project/executor:v1.7.0
tekton_oc_client_image: quay.io/openshift/origin-cli:latest
tekton_preferred_builder: buildah
tekton_s2i_image: quay.io/openshift-pipeline/s2i:v0.11
tekton_scan_code: True
tekton_scan_images: True
tekton_sign_images: True
tekton_skopeo_image: docker.io/ananace/skopeo:latest
tekton_uses_resources_limits: True

# Trivy
trivy_cache: fs
trivy_cpu_limit: 100m
trivy_cpu_request: 10m
trivy_data_capacity: 32Gi
trivy_image_source: docker.io/aquasec/trivy
trivy_memory_limit: 1Gi
trivy_memory_request: 128Mi
trivy_tag: 0.28.0
expose_trivy: False

# Directory WhitePages
whitepages_cpu_limit: 50m
whitepages_cpu_request: 10m
whitepages_image_source: https://gitlab.com/synacksynack/opsperator/docker-whitepages
whitepages_memory_limit: 128Mi
whitepages_memory_request: 64Mi
