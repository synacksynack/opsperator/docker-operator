- include_role:
    name: commons
    tasks_from: helpers/volume.yaml
  vars:
    vol_access_mode: ReadWriteMany
    vol_name: "{{ scalelite_name }}"
    vol_size: "{{ scalelite_data_capacity }}"
- include_role:
    name: commons
    tasks_from: helpers/deployment.yaml
  vars:
    ctoo_cpu_limit: "{{ exporter_cpu_limit }}"
    ctoo_cpu_request: "{{ exporter_cpu_request }}"
    ctoo_idx: "{{ 3 if (do_exporters)
                     else 0 }}"
    ctoo_memory_limit: "{{ exporter_memory_limit }}"
    ctoo_memory_request: "{{ exporter_memory_request }}"
    ctoo_tag: "{{ scaleliteexporter_tag }}"
    cpu_limit: "{{ scalelite_cpu_limit }}"
    cpu_request: "{{ scalelite_cpu_request }}"
    cone_cpu_limit: "{{ scalelite_nginx_cpu_limit }}"
    cone_cpu_request: "{{ scalelite_nginx_cpu_request }}"
    cone_idx: 1
    cone_memory_limit: "{{ scalelite_nginx_memory_limit }}"
    cone_memory_request: "{{ scalelite_nginx_memory_request }}"
    cone_tag: "{{ scalelite_tag }}"
    dc_data: "{{ lookup('template', 'scalelite.j2') }}"
    dc_name: "{{ scalelite_name }}"
    dc_replicas: "{{ scalelite_replicas }}"
    dc_tag: "{{ scalelite_tag }}"
    exporter_idx: 2
    exporter_tag: "{{ nginxexporter_tag }}"
    memory_limit: "{{ scalelite_memory_limit }}"
    memory_request: "{{ scalelite_memory_request }}"
- include_role:
    name: commons
    tasks_from: helpers/service.yaml
  vars:
    ports:
    - 8080
    svc_name: "{{ scalelite_name }}"
- include_role:
    name: commons
    tasks_from: helpers/route.yaml
  vars:
    route_host: "scalelite.{{ root_domain }}"
    route_name: "{{ scalelite_name }}"
    svc_name: "{{ scalelite_name }}"
- include_role:
    name: commons
    tasks_from: helpers/network-policy.yaml
  vars:
    source:
    - namespace: "{{ default_namespace_match }}"
    policy_name: "public-to-{{ scalelite_name }}"
    policy_state: "{{ 'present' if (do_network_policy)
                                else 'absent' }}"
    subject: "{{ scalelite_name }}"
    to:
    - number: "8080"
- include_role:
    name: commons
    tasks_from: helpers/service.yaml
  vars:
    is_exporter: True
    ports:
    - 9113
    - 9114
    svc_name: "{{ scalelite_name }}"
    svc_state: "{{ 'present' if (do_exporters)
                             else 'absent' }}"
- include_role:
    name: commons
    tasks_from: helpers/network-policy.yaml
  vars:
    source:
    - namespace: "{{ prometheus_namespace_match }}"
    policy_name: "prometheus-to-{{ scalelite_name }}"
    policy_state: "{{ 'present' if (do_exporters
                                    and do_network_policy)
                                else 'absent' }}"
    subject: "{{ scalelite_name }}"
    to:
    - number: "9113"
    - number: "9114"
- block:
  - block:
    - name: Resolves Scalelite Pods
      async: 30
      changed_when: False
      failed_when: False
      k8s_info:
        api_version: v1
        kind: Pod
        namespace: "{{ namespace }}"
        label_selectors:
        - "name={{ scalelite_name }}"
      no_log: True
      poll: 5
      register: has_pods
    - include_role:
        name: commons
        tasks_from: helpers/restartpod.yaml
      loop: "{{ has_pods.resources }}"
      vars:
        pod_name: "{{ item.metadata.name }}"
        wait_for_apivers: "{{ deployment_apivers }}"
        wait_for_kind: "{{ deployment_kind }}"
        wait_for_objname: "{{ scalelite_name }}"
      when:
      - has_pods is defined
      - has_pods.resources is defined
      - has_pods.resources | length > 0
    when:
    - not (dc_started is changed)
    - >
        (cacfg is defined
         and cacfg is changed)
        or (slcfg is defined
            and slcfg is changed)
  - include_role:
      name: commons
      tasks_from: helpers/wait-for.yaml
    vars:
      check_with: "{{ lookup('k8s', api_version=deployment_apivers,
                             kind=deployment_kind, namespace=namespace,
                             resource_name=scalelite_name) | default('') }}"
      obj_name: "{{ scalelite_name }}"
      retries_for: 90
      wait_for: 30
    when:
    - dc_started is changed
  when:
  - dc_started is defined
- include_role:
    name: commons
    tasks_from: helpers/autoscale.yaml
  vars:
    autoscale_cpu_target: "{{ scalelite_hpa_cpu_target | default(hpa_cpu_target | default(75)) }}"
    autoscale_name: "{{ scalelite_name }}"
    autoscale_state: "{{ 'present' if (scalelite_replicas not in [ 0, '0' ]
                                       and ((has_pvc.resources | default([])) | length == 0
                                            or ((has_pvc.resources | default([])) | length > 0
                                                and has_pvc.resources[0].spec.accessModes[0] == 'ReadWriteMany')))
                                   else 'absent' }}"
    max_replicas: "{{ scalelite_max_replicas | default(hpa_max_replicas | default(4)) }}"
    min_replicas: "{{ scalelite_min_replicas | default(scalelite_replicas) }}"
- include_role:
    name: commons
    tasks_from: helpers/poddisruptionbudget.yaml
  vars:
    pdb_name: "{{ scalelite_name }}"
    pdb_min_available: 1
    pdb_state: "{{ 'present' if ((scalelite_replicas | int) > 1
                                 and do_pdb)
                             else 'absent' }}"
