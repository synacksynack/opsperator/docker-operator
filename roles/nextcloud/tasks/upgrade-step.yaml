- block:
  - block:
    - include_role:
        name: commons
        tasks_from: helpers/volume.yaml
      vars:
        vol_name: "{{ ncms_name if (nextcloud_db_type in [ 'mariadb', 'mysql' ])
                                else (ncpc_name if (nextcloud_db_type == 'percona')
                                                else ncpg_name) }}-{{ target }}-snap"
        vol_size: "{{ nextcloud_db_data_capacity }}"
    - name: Marks operation in progress
      set_fact:
        snap_failed: True
      when:
      - has_pvc is defined
      - has_pvc.resources is defined
      - (has_pvc.resources | length) > 0
    when:
    - not (rwo_can_snap | default(False))
  - name: Scales NextCloud down to 0
    async: 30
    k8s:
      definition:
        apiVersion: "{{ deployment_apivers }}"
        kind: "{{ deployment_kind }}"
        metadata:
          name: "{{ nextcloud_name }}"
          namespace: "{{ namespace }}"
        spec:
          replicas: 0
    poll: 5
    when:
    - (snap_failed | default(False)) == False
  - block:
    - block:
      - include_role:
          name: commons
          tasks_from: snapshots/mysql.yaml
        vars:
          db_data_capacity: "{{ nextcloud_db_data_capacity }}"
          db_host: "{{ ncms_name if (nextcloud_db_type != 'percona')
                                 else (ncpc_name ~ '-client') }}"
          db_secret_name: "{{ ncms_name if (nextcloud_db_type != 'percona')
                                        else ncpc_name }}"
          job_assume_pod_netpol: "{{ nextcloud_name }}"
          provision_volume: False
          snap_mark: "{{ target }}"
          snap_wait_for: 30
          snap_wait_retries: 120
        when:
        - (snap_failed | default(False)) == False
      - name: Lookups MySQL Snapshot Job
        async: 30
        changed_when: False
        k8s_info:
          api_version: batch/v1
          kind: Job
          namespace: "{{ namespace }}"
          name: "{{ ncms_name if (nextcloud_db_type != 'percona')
                              else (ncpc_name ~ '-client') }}-{{ target }}-snap"
        no_log: True
        poll: 5
        register: mysql_snap
        when:
        - (snap_failed | default(False)) == False
      when:
      - nextcloud_db_type in [ 'mariadb', 'mysql', 'percona' ]
    - block:
      - include_role:
          name: commons
          tasks_from: snapshots/postgres.yaml
        vars:
          db_data_capacity: "{{ nextcloud_db_data_capacity }}"
          db_host: "{{ ncpg_name }}"
          db_secret_name: "{{ ncpg_name }}"
          job_assume_pod_netpol: "{{ nextcloud_name }}"
          provision_volume: False
          snap_mark: "{{ target }}"
          snap_wait_for: 30
          snap_wait_retries: 120
        when:
        - (snap_failed | default(False)) == False
      - name: Lookups Postgres Snapshot Job
        async: 30
        changed_when: False
        k8s_info:
          api_version: batch/v1
          kind: Job
          namespace: "{{ namespace }}"
          name: "{{ ncpg_name }}-{{ target }}-snap"
        no_log: True
        poll: 5
        register: pg_snap
        when:
        - (snap_failed | default(False)) == False
      when:
      - nextcloud_db_type == 'postgres'
    when:
    - not (rwo_can_snap | default(False))
  - block:
    - name: Scales Postgres down to 0
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ ncpg_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 0
      poll: 5
      when:
      - nextcloud_db_type == 'postgres'
    - name: Scales MySQL down to 0
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ ncms_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 0
      poll: 5
      when:
      - nextcloud_db_type in [ 'mariadb', 'mysql' ]
    - name: Scales Percona down to replicas-1
      async: 30
      k8s:
        definition:
          apiVersion: "{{ sts_apivers }}"
          kind: StatefulSet
          metadata:
            name: "{{ ncpc_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: "{{ ((percona_replicas | default(3)) | int) - 1
                            if ((percona_replicas | default(3)) | int > 1)
                            else 0 }}"
      poll: 5
      when:
      - nextcloud_db_type == 'percona'
    - include_role:
        name: commons
        tasks_from: helpers/volumesnapshot.yaml
      vars:
        pvc_name: "{{ ncms_name if (nextcloud_db_type != 'percona')
                                else ('data-' ~ ncpc_name ~ '-'
                                      ~ (((percona_replicas | default(3)) | int) - 1)) }}"
        snap_mark: "{{ target }}"
    - name: Lookups VolumeSnapshot
      async: 30
      changed_when: False
      k8s_info:
        api_version: snapshot.storage.k8s.io/v1beta1
        kind: VolumeSnapshot
        namespace: "{{ namespace }}"
        name: "{{ ncms_name if (nextcloud_db_type != 'percona')
                            else ('data-' ~ ncpc_name ~ '-'
                                      ~ (((percona_replicas | default(3)) | int) - 1)) }}"
      no_log: True
      poll: 5
      register: dbvolume_snap
    - name: Checks for failures
      set_fact:
        snap_failed: True
      when:
      - >
          not (dbvolume_snap is defined
               and dbvolume_snap.resources is defined
               and dbvolume_snap.resources | length > 0
               and dbvolume_snap.resources[0].status is defined
               and dbvolume_snap.resources[0].status.readyToUse is defined
               and dbvolume_snap.resources[0].status.readyToUse == True)
    - name: Scales Postgres back up
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ ncpg_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
      when:
      - nextcloud_db_type == 'postgres'
    - name: Scales MySQL back up
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ ncms_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
      when:
      - nextcloud_db_type in [ 'mariadb', 'mysql' ]
    - name: Scales Percona back up
      async: 30
      k8s:
        definition:
          apiVersion: "{{ sts_apivers }}"
          kind: StatefulSet
          metadata:
            name: "{{ ncpc_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: "{{ percona_replicas | default(3) }}"
      poll: 5
      when:
      - nextcloud_db_type == 'percona'
    when:
    - rwo_can_snap | default(False)
  - block:
    - block:
      - include_role:
          name: commons
          tasks_from: snapshots/fs.yaml
        vars:
          data_capacity: "{{ nextcloud_data_capacity }}"
          snap_mark: "{{ target }}"
          snap_wait_for: 30
          snap_wait_retries: 360
          source_pvc: "{{ nextcloud_name }}"
      - name: Lookups NextCloud Job
        async: 30
        changed_when: False
        k8s_info:
          api_version: batch/v1
          kind: Job
          namespace: "{{ namespace }}"
          name: "{{ nextcloud_name }}-{{ target }}-snap"
        no_log: True
        poll: 5
        register: nc_snap
        when:
        - (snap_failed | default(False)) == False
      when:
      - not (rwo_can_snap | default(False))
    - block:
      - include_role:
          name: commons
          tasks_from: helpers/volumesnapshot.yaml
        vars:
          pvc_name: "{{ nextcloud_name }}"
          snap_mark: "{{ target }}"
      - name: Lookups VolumeSnapshot
        async: 30
        changed_when: False
        k8s_info:
          api_version: snapshot.storage.k8s.io/v1beta1
          kind: VolumeSnapshot
          namespace: "{{ namespace }}"
          name: "{{ nextcloud_name }}"
        no_log: True
        poll: 5
        register: appvolume_snap
      - name: Checks for failures
        set_fact:
          snap_failed: True
        when:
        - >
            not (appvolume_snap is defined
                 and appvolume_snap.resources is defined
                 and appvolume_snap.resources | length > 0
                 and appvolume_snap.resources[0].status is defined
                 and appvolume_snap.resources[0].status.readyToUse is defined
                 and appvolume_snap.resources[0].status.readyToUse == True)
      when:
      - rwo_can_snap | default(False)
    - name: Disables snapshots for further NextCloud upgrades
      set_fact:
        nextcloud_snapshot_data_on_upgrade: False
    when:
    - (snap_failed | default(False)) == False
    - nextcloud_snapshot_data_on_upgrade | default(True)
  - block:
    - name: Reverts Upgrade when snapshot failed
      async: 30
      k8s:
        definition:
          apiVersion: "{{ deployment_apivers }}"
          kind: "{{ deployment_kind }}"
          metadata:
            name: "{{ nextcloud_name }}"
            namespace: "{{ namespace }}"
          spec:
            replicas: 1
      poll: 5
    - name: Fails when snapshot failed
      fail:
        msg: "Failed upgrading NextCloud to {{ target }}"
      when:
      - (snap_failed | default(False)) == False
    when:
    - >
        snap_failed | default(False)
        or (nextcloud_db_type in [ 'mariadb', 'mysql', 'percona' ]
            and mysql_snap is defined
            and mysql_snap.resources is defined
            and (mysql_snap.resources | length == 0
                 or (mysql_snap.resources[0].status is defined
                     and mysql_snap.resources[0].status is defined
                     and (mysql_snap.resources[0].status.completionTime is not defined
                          or (not ((mysql_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
        or (nextcloud_db_type == 'postgres'
            and pg_snap is defined
            and pg_snap.resources is defined
            and (pg_snap.resources | length == 0
                 or (pg_snap.resources[0].status is defined
                     and pg_snap.resources[0].status is defined
                     and (pg_snap.resources[0].status.completionTime is not defined
                          or (not ((pg_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
        or (nc_snap is defined
            and nc_snap.resources is defined
            and (nc_snap.resources | length == 0
                 or (nc_snap.resources | length > 0
                     and nc_snap.resources[0].status is defined
                     and (nc_snap.resources[0].status.completionTime is not defined
                          or (not ((nc_snap.resources[0].status.succeeded | default(0)) in [ 1, '1' ]))))))
  when:
  - do_backups
- block:
  - include_role:
      name: commons
      tasks_from: helpers/images-facts.yaml
    vars:
      nextcloud_tag: "{{ (target | regex_replace('-nc.*$')) }}"
      nextcloud_version: "{{ (target | regex_replace('^.*-nc')) }}"
  - include_tasks: "{{ imgplan }}.yaml"
  - include_role:
      name: commons
      tasks_from: helpers/deployment.yaml
    vars:
      cone_cpu_limit: "{{ sshd_cpu_limit }}"
      cone_cpu_request: "{{ sshd_cpu_request }}"
      cone_idx: "{{ 3 if (do_backups
                          and not do_s3
                          and do_exporters)
                      else (1 if (do_backups
                                  and not do_s3)
                              else 0) }}"
      cone_memory_limit: "{{ sshd_memory_limit }}"
      cone_memory_request: "{{ sshd_memory_request }}"
      cone_tag: "{{ sshd_tag }}"
      cpu_limit: "{{ nextcloud_cpu_limit }}"
      cpu_request: "{{ nextcloud_cpu_request }}"
      ctoo_cpu_limit: "{{ exporter_cpu_limit }}"
      ctoo_cpu_request: "{{ exporter_cpu_request }}"
      ctoo_idx: "{{ 2 if (do_exporters) else 0 }}"
      ctoo_memory_limit: "{{ exporter_memory_limit }}"
      ctoo_memory_request: "{{ exporter_memory_request }}"
      ctoo_tag: "{{ ncexporter_tag }}"
      dc_data: "{{ lookup('template', 'nextcloud.j2') }}"
      dc_name: "{{ nextcloud_name }}"
      dc_replicas: 1
      dc_tag: "{{ target }}"
      exporter_idx: 1
      exporter_tag: "{{ httpexporter_tag }}"
      memory_limit: "{{ nextcloud_memory_limit }}"
      memory_request: "{{ nextcloud_memory_request }}"
  - include_role:
      name: commons
      tasks_from: helpers/wait-for.yaml
    vars:
      check_with: "{{ lookup('k8s', api_version=deployment_apivers,
                             kind=deployment_kind, namespace=namespace,
                             resource_name=nextcloud_name) | default('') }}"
      obj_name: "{{ nextcloud_name }}"
      retries_for: 220
      wait_for: 10
    when:
    - dc_started is defined
    - dc_started is changed
  - name: Checks NextCloud Upgrade Status
    async: 30
    changed_when: False
    k8s_info:
      api_version: "{{ deployment_apivers }}"
      kind: "{{ deployment_kind }}"
      name: "{{ nextcloud_name }}"
      namespace: "{{ namespace }}"
    no_log: True
    poll: 5
    register: has_nc
  - block:
    - name: Fails when upgrade is still running
      fail:
        msg: "Failed upgrading NextCloud to {{ target }}"
      when:
      - has_nc.resources[0].status.readyReplicas == 0
    - include_role:
        name: commons
        tasks_from: helpers/deployment.yaml
      vars:
        cone_cpu_limit: "{{ sshd_cpu_limit }}"
        cone_cpu_request: "{{ sshd_cpu_request }}"
        cone_idx: "{{ 3 if (do_backups
                            and not do_s3
                            and do_exporters)
                        else (1 if (do_backups
                                    and not do_s3)
                                else 0) }}"
        cone_memory_limit: "{{ sshd_memory_limit }}"
        cone_memory_request: "{{ sshd_memory_request }}"
        cone_tag: "{{ sshd_tag }}"
        cpu_limit: "{{ nextcloud_cpu_limit }}"
        cpu_request: "{{ nextcloud_cpu_request }}"
        ctoo_cpu_limit: "{{ exporter_cpu_limit }}"
        ctoo_cpu_request: "{{ exporter_cpu_request }}"
        ctoo_idx: "{{ 2 if (do_exporters) else 0 }}"
        ctoo_memory_limit: "{{ exporter_memory_limit }}"
        ctoo_memory_request: "{{ exporter_memory_request }}"
        dc_data: "{{ lookup('template', 'nextcloud.j2') }}"
        dc_name: "{{ nextcloud_name }}"
        dc_replicas: "{{ nextcloud_replicas }}"
        dc_tag: "{{ target }}"
        exporter_idx: 1
        exporter_tag: "{{ httpexporter_tag }}"
        memory_limit: "{{ nextcloud_memory_limit }}"
        memory_request: "{{ nextcloud_memory_request }}"
      when:
      - nextcloud_replicas not in [ '1', 1 ]
      - has_nc.resources[0].status.readyReplicas > 0
    when:
    - has_nc is defined
    - has_nc.resources is defined
    - has_nc.resources | length > 0
    - has_nc.resources[0].status is defined
    - has_nc.resources[0].status.readyReplicas is defined
  when:
  - (snap_failed | default(False)) == False
