# Operator defaults
allow_rwx_storage: True
cluster_domain: cluster.local
execution_affinity_required: True
opsperator_images_registry: False
pull_policy: IfNotPresent
root_domain: demo.local
scheduling_affinity_required: True
timezone: Europe/Paris

do_affinities: True
do_cert_manager: False
do_debugs: False
do_exporters: False
do_grafana: True
do_hpa: False
do_network_policy: False
do_oc: False
do_podsecuritypolicy: True
do_thanos: False
do_thanos_bucket: False
do_thanos_bucketcache: False
do_thanos_queryfrontend: False
do_thanos_receive: False
do_thanos_rule: False
do_triggers: False

# ARA
ara_playbook_labels:
- Monitoring
- Opsperator
- Prometheus
ara_playbook_name: Prometheus

# AlertManager
alertmanager_cpu_limit: 100m
alertmanager_cpu_request: 10m
alertmanager_data_capacity: 2Gi
alertmanager_image_source: docker.io/prom/alertmanager
alertmanager_ldap_user_objectclass: inetOrgPerson
alertmanager_memory_limit: 75Mi
alertmanager_memory_request: 50Mi
alertmanager_replicas: 1
alertmanager_tag: v0.25.0

# CertManager Exporter
certmanagerexporter_cpu_limit: 100m
certmanagerexporter_cpu_request: 100m
certmanagerexporter_image_source: docker.io/joeelliott/cert-exporter
certmanagerexporter_memory_limit: 128Mi
certmanagerexporter_memory_request: 128Mi
certmanagerexporter_replicas: 1
certmanagerexporter_tag: v2.7.0

# LemonLDAP AuthProxy
authproxy_cpu_limit: 50m
authproxy_cpu_request: 10m
authproxy_memory_limit: 256Mi
authproxy_memory_request: 128Mi
authproxy_tag: 2.4.52-20220918

# OpenShift DeploymentConfigs
deploy_pod_cpu_limit: 50m
deploy_pod_cpu_request: 50m
deploy_pod_memory_limit: 512Mi
deploy_pod_memory_request: 128Mi
deploy_revision_history_limit: 3

# Grafana
grafana_admin_password: secret
grafana_admin_user: admin
grafana_cpu_limit: 100m
grafana_cpu_request: 50m
grafana_data_capacity: 2Gi
grafana_db_cpu_limit: 300m
grafana_db_data_capacity: 8Gi
grafana_db_memory_limit: 512Mi
grafana_db_type: sqlite
grafana_image_source: docker.io/grafana/grafana
grafana_is_public: False
grafana_is_readonly: False
grafana_ldap_group_objectclass: groupOfNames
grafana_ldap_user_objectclass: inetOrgPerson
grafana_memory_limit: 128Mi
grafana_memory_request: 64Mi
grafana_replicas: 1
grafana_sessions_type: memory
grafana_tag: 9.3.6

# Kubernetes
k8s_affinity_zone_label: kubernetes.io/hostname
k8s_namespace_nodes_selectors: []
k8s_resource_nodes_selectors:
- kubernetes.io/os=linux
k8s_unprivileged_userid: 1000
tls_provider: internalca

# Kube State Metrics
kubestatemetrics_cpu_limit: 30m
kubestatemetrics_cpu_request: 30m
kubestatemetrics_image_source: k8s.gcr.io/kube-state-metrics/kube-state-metrics
kubestatemetrics_memory_limit: 200Mi
kubestatemetrics_memory_request: 50Mi
kubestatemetrics_tag: v2.5.0

# NetworkPolicies
default_namespace_match: default
network_policy_deployment_label: name
network_policy_namespace_label: netpol

# Node Exporter
nodeexporter_collect_nodes_selectors:
- kubernetes.io/os=linux
nodeexporter_cpu_limit: 60m
nodeexporter_cpu_request: 60m
nodeexporter_image_source: quay.io/prometheus/node-exporter
nodeexporter_memory_limit: 50Mi
nodeexporter_memory_request: 50Mi
nodeexporter_tag: v1.3.1

# AlertManager Notifications
#notify_email_address: alerts@example.com
#notify_http_address: http://some-external-service/notify
notify_outside_business_hours: True
notify_send_resolved: "true"
#notify_slack_address: https://hooks.slack.com/services/XXX/YYY/ZZZ
notify_slack_channel: monitoring
#notify_teams_address: https://outlook.office.com/webhook/XXX/IncomingWebhook/YYY/ZZZ

# OpenShift OAuthProxy
oauthproxy_cpu_limit: 50m
oauthproxy_cpu_request: 50m
oauthproxy_image_source: docker.io/openshift/oauth-proxy
oauthproxy_memory_limit: 128Mi
oauthproxy_memory_request: 64Mi
oauthproxy_tag: latest

# Prometheus
ceph_exporter_port: 9283
exporter_cpu_limit: 100m
exporter_cpu_request: 50m
exporter_memory_limit: 128Mi
exporter_memory_request: 64Mi
httpexporter_tag: 1.1.0
prometheus_cluster_label: k8s-cluster
prometheus_cpu_limit: 500m
prometheus_cpu_request: 150m
prometheus_data_capacity: 360Gi
prometheus_eval_interval: 30s
prometheus_federation: []
prometheus_image_source: docker.io/prom/prometheus
prometheus_ldap_user_objectclass: inetOrgPerson
prometheus_local_cluster: prod
prometheus_match: scrape-me
prometheus_memory_limit: 2Gi
prometheus_memory_request: 1Gi
prometheus_node_label: k8s-node
prometheus_replicas: 1
prometheus_retention: 15d
prometheus_s3_label: k8s-s3
prometheus_scrape_interval: 15s
prometheus_scrape_s3_interval: 60s
prometheus_scrape_s3_timeout: 50s
prometheus_scrape_timeout: 10s
prometheus_service_label: k8s-app
prometheus_tag: v2.42.0

# Teams Proxy
teamsproxy_cpu_limit: 100m
teamsproxy_cpu_request: 10m
teamsproxy_image_source: quay.io/prometheusmsteams/prometheus-msteams
teamsproxy_memory_limit: 48Mi
teamsproxy_memory_request: 8Mi
teamsproxy_tag: v1.5.0

# Thanos
expose_thanos_query: False
expose_thanos_receive: False
thanos_bucket_cache_cpu_limit: 50m
thanos_bucket_cache_cpu_request: 10m
thanos_bucket_cache_memory_limit: 256Mi
thanos_bucket_cache_memory_limit_mbyte: 192
thanos_bucket_cache_memory_request: 64Mi
thanos_bucket_cache_replicas: 3
thanos_bucket_cpu_limit: 420m
thanos_bucket_cpu_request: 120m
thanos_bucket_memory_limit: 432Mi
thanos_bucket_memory_request: 128Mi
thanos_bucket_refresh: 30m
thanos_bucket_replicas: 1
thanos_bucket_timeout: 2m
thanos_compact_cpu_limit: 420m
thanos_compact_cpu_request: 120m
thanos_compact_data_capacity: 32Gi
thanos_compact_interval: 30m
thanos_compact_memory_limit: 3Gi
thanos_compact_memory_request: 1Gi
thanos_compact_replicas: 1
thanos_image_source: quay.io/thanos/thanos
thanos_index_backend: memory
thanos_index_cache_cpu_limit: 50m
thanos_index_cache_cpu_request: 10m
thanos_index_cache_memory_limit: 256Mi
thanos_index_cache_memory_limit_mbyte: 192
thanos_index_cache_memory_request: 64Mi
thanos_index_cache_replicas: 3
thanos_index_max_item_size: 0
thanos_index_max_size: 0
thanos_query_cpu_limit: 420m
thanos_query_cpu_request: 120m
thanos_query_memory_limit: 432Mi
thanos_query_memory_request: 128Mi
thanos_query_replicas: 1
thanos_query_timeout: 5m
thanos_queryfrontend_backend: memory
thanos_queryfrontend_cache_cpu_limit: 50m
thanos_queryfrontend_cache_cpu_request: 10m
thanos_queryfrontend_cache_max_freshness: 2m
thanos_queryfrontend_cache_memory_limit: 256Mi
thanos_queryfrontend_cache_memory_limit_mbyte: 192
thanos_queryfrontend_cache_memory_request: 64Mi
thanos_queryfrontend_cache_replicas: 3
thanos_queryfrontend_cpu_limit: 420m
thanos_queryfrontend_cpu_request: 120m
thanos_queryfrontend_max_item_size: 0
thanos_queryfrontend_memory_limit: 432Mi
thanos_queryfrontend_memory_request: 128Mi
thanos_queryfrontend_replicas: 1
thanos_queryfrontend_split_interval: 2h
thanos_receive_cpu_limit: 420m
thanos_receive_cpu_request: 120m
thanos_receive_data_capacity: 8Gi
thanos_receive_memory_limit: 432Mi
thanos_receive_memory_request: 128Mi
thanos_receive_replicas: 1
thanos_retention_resolution_1h: 90d
thanos_retention_resolution_5m: 90d
thanos_retention_resolution_raw: 90d
thanos_rule_cpu_limit: 420m
thanos_rule_cpu_request: 120m
thanos_rule_data_capacity: 8Gi
thanos_rule_eval_interval: 30s
thanos_rule_memory_limit: 432Mi
thanos_rule_memory_request: 128Mi
thanos_rule_replicas: 1
#thanos_s3_access_key: ABCD
thanos_s3_bucket: thanos
thanos_s3_flavor: minio
#thanos_s3_region: us-east-1
#thanos_s3_secret_key: EFGH
thanos_sidecar_cpu_limit: 100m
thanos_sidecar_cpu_request: 10m
thanos_sidecar_memory_limit: 128Mi
thanos_sidecar_memory_request: 64Mi
thanos_store_cpu_limit: 420m
thanos_store_cpu_request: 120m
thanos_store_data_capacity: 8Gi
thanos_store_memory_limit: 432Mi
thanos_store_memory_request: 128Mi
thanos_store_replicas: 1
thanos_tag: v0.26.0
