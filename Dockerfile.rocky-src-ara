FROM docker.io/golang:1.16 AS builder
#see https://github.com/operator-framework/operator-sdk/blob/master/images/ansible-operator/Dockerfile

ARG SDK_VERSION=1.15.0

ENV DEBIAN_FRONTEND=noninteractive \
    SDK_REPO=https://github.com/operator-framework/operator-sdk

RUN set -x \
    && apt-get install -y git \
    && mkdir /workspace \
    && git clone -b v$SDK_VERSION $SDK_REPO /workspace \
    && cd /workspace \
    && go mod download \
    && if test `uname -m` = aarch64; then \
	export GOARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	export GOARCH=arm; \
    else \
	export GOARCH=amd64; \
    fi \
    && GOOS=linux make build/ansible-operator

FROM docker.io/rockylinux:8.5

ARG ANSIBLE_VERSION=2.9.26
ARG ANSIBLE_RUNNER_HTTP_VERSION=1.0.0
ARG ANSIBLE_RUNNER_VERSION=2.0.2
ARG CRYPTO_VERSION=3.3.2
ARG K8S_VERSION=12.0.1
ARG OPENSHIFT_VERSION=0.12.1
ARG SDK_VERSION=1.15.0

ENV HOME=/opt/ansible \
    PYSITE=/usr/local/lib/python3.6/site-packages \
    RESYNC_PERIOD=10 \
    TINI_REPO=https://github.com/krallin/tini \
    TINI_VERSION=0.19.0 \
    USER_NAME=ansible \
    USER_UID=1001 \
    VERSION=0.0.1

LABEL io.k8s.description="Opsperator - SDK $SDK_VERSION, Ansible $ANSIBLE_VERSION, Ansible Runner $ANSIBLE_RUNNER_VERSION, Kubernetes $K8S_VERSION, OpenShift $OPENSHIFT_VERSION." \
      io.k8s.display-name="Opsperator $VERSION" \
      io.openshift.tags="opsperator,operator" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-operator" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$VERSION"

COPY --from=builder /workspace/build/ansible-operator /usr/local/bin/ansible-operator
COPY config/* /

RUN set -x \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 \
    && mv /my_dlock /tail_logs /list_logs /check_results /rm_timeouts \
	/show_changes /whosthere /usr/bin/ \
    && ARCH=`uname -m` \
    && if test $ARCH = aarch64; then \
	export TARCH=arm64; \
    elif test $ARCH = armv7l; then \
	export TARCH=armhf; \
    else \
	export TARCH=amd64; \
    fi \
    && curl -fsL -o /usr/bin/tini \
	$TINI_REPO/releases/download/v$TINI_VERSION/tini-$TARCH \
    && chmod +x /usr/bin/tini \
    && dnf -y install epel-release \
    && if test "$DO_UPGRADE"; then \
	dnf -y upgrade; \
    fi \
    && dnf -y install openssh openssh-clients openssl opendkim \
	gcc perl-Getopt-Long python3 python3-setuptools python36-devel \
	postgresql libpq-devel mysql redhat-rpm-config \
    && ln -sf /usr/bin/python3 /usr/bin/python \
    && ln -sf /usr/bin/pip3 /usr/bin/pip \
    && pip3 install --upgrade pip setuptools wheel \
    && pip3 install ansible==$ANSIBLE_VERSION \
    && pip3 install ansible_runner==$ANSIBLE_RUNNER_VERSION \
    && pip3 install ansible_runner_http==$ANSIBLE_RUNNER_HTTP_VERSION \
    && pip3 install kubernetes==$K8S_VERSION \
    && pip3 install openshift==$OPENSHIFT_VERSION \
    && pip3 install cryptography==$CRYPTO_VERSION \
    && pip3 install jmespath \
    && pip3 install --upgrade psycopg2 pymysql ara[server] \
    && echo "$USER_NAME:x:$USER_UID:0:$USER_NAME user:$HOME:/sbin/nologin" \
	>>/etc/passwd \
    && mkdir -p /.ara/server $HOME/.ansible/tmp /etc/ansible \
    && ( \
	echo [defaults] \
	&& echo roles_path = /opt/ansible/roles \
	&& python3 -m ara.setup.ansible | grep -v default; \
    ) >>/etc/ansible/ansible.cfg \
    && chown -R $USER_UID:0 /.ara /usr/bin/my_dlock $HOME \
    && chmod -R ug+rwx /.ara/server $HOME/.ansible/tmp \
    && cp $PYSITE/ara/plugins/callback/ara_default.py \
	$PYSITE/ansible_runner/callbacks/ \
    && dnf remove -y redhat-rpm-config gcc python36-devel libpq-devel \
    && dnf clean all -y \
    && rm -rf /var/cache/yum/* /usr/share/doc /usr/share/man /ps /el8.repo \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

COPY roles/ $HOME/roles/
COPY watches.yaml $HOME/watches.yaml

RUN chown -R 1001:root /tmp $HOME \
    && chmod -R g=u /tmp $HOME

WORKDIR $HOME
USER $USER_UID

ENTRYPOINT ["/usr/bin/tini", "--", "/usr/local/bin/ansible-operator", "run", "--watches-file=./watches.yaml"]
