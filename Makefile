SKIP_SQUASH?=1
CINAMESPACE   = ci
OPNAMESPACE   = opsperator
IMAGE         = opsperator/operator
FRONTNAME     = demo
REGISTRY_ADDR = katello.example.com:5000
REGISTRY_REPO = KatelloOrgName-ProductName-RepoName
-include Makefile.cust

##################
### Local      ###
##################

.PHONY: build
build: gencrd
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh $(OS)

.PHONY: gencrd
gencrd:
	@@ls deploy/openshift/crd-v3/*yaml | sort | while read f; \
	    do \
		cat $$f; \
	    done >deploy/openshift/crd-v3.yaml
	@@ls deploy/openshift/crd-v4/*yaml | sort | while read f; \
	    do \
		cat $$f; \
	    done >deploy/openshift/crd-v4.yaml
	@@ls deploy/kubernetes/crd/*yaml | sort | while read f; \
	    do \
		cat $$f; \
	    done >deploy/kubernetes/crd.yaml
	@@echo --- >watches.yaml
	@@ls watches.d/*yaml | sort | while read f; \
	    do \
		cat $$f; \
	    done >>watches.yaml

.PHONY: registry-ca
registry-ca:
	@@if ! test -s /usr/local/share/ca-certificates/$(REGISTRY_ADDRESS).crt; then \
	    openssl s_client -showcerts -connect $(REGISTRY_ADDRESS) </dev/null \
		| sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' \
		>/usr/local/share/ca-certificates/$(REGISTRY_ADDRESS).crt; \
	    update-ca-certificates; \
	    systemctl restart docker; \
	fi

.PHONY: push
push: build
	@@docker tag $(IMAGE) $(REGISTRY_ADDRESS)/$(REGISTRY_REPO):latest
	@@docker push $(REGISTRY_ADDRESS)/$(REGISTRY_REPO)

.PHONY: pushcode
pushcode:
	@@pod=`kubectl get pods -n $(OPNAMESPACE) | awk '/operator/{print $$1}'`; \
	kubectl cp -n $(OPNAMESPACE) roles $$pod:/opt/ansible/

.PHONY: reloadbase
reloadbase:
	@@docker pull quay.io/operator-framework/ansible-operator


##################
### Kubernetes ###
##################

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/ci/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubeclean
kubeclean: kubecheck
	@@ls deploy/openshift/cr/*.yaml | while read f; \
	    do \
		kubectl delete -f $$f || true; \
	    done

.PHONY: kubedemo
kubedemo: kubedeploy
	@@kubectl apply -f deploy/kubernetes/cr/tekton.yaml

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@kubectl apply -f deploy/kubernetes/crd.yaml
	@@kubectl apply -f deploy/kubernetes/rbac.yaml
	@@kubectl apply -f deploy/kubernetes/namespace.yaml
	@@sed 's|image: .*|image: $(REGISTRY_ADDRESS)/$(REGISTRY_REPO):latest|' \
	    deploy/kubernetes/run-ephemeral.yaml | \
	    kubectl apply -f-

.PHONY: kubepurge
kubepurge: kubeclean
	@@kubectl delete -f deploy/kubernetes/run-ephemeral.yaml

.PHONY: kubereload
kubereload: katello kubecheck
	@@if kubectl get pods -n $(OPNAMESPACE) --selector=name=operator \
		2>/dev/null | grep operator >/dev/null; then \
	    kubectl delete pod -n $(OPNAMESPACE) --selector=name=operator; \
	else \
	    kubectl scale -n $(OPNAMESPACE) deploy/operator --replicas=1; \
	fi


##################
### OpenShift  ###
##################

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/ci/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/ci/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "OPERATOR_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/ci/build.yaml \
		-p "OPERATOR_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocdeploy
ocdeploy: occheck
	@@if oc describe project image-registry >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/crd-v4.yaml | oc apply -f-; \
	    oc process -f deploy/openshift/rbac-v4.yaml | oc apply -f-; \
	else \
	    oc process -f deploy/openshift/crd-v3.yaml | oc apply -f-; \
	    oc process -f deploy/openshift/rbac-v3.yaml | oc apply -f-; \
	fi
	@@project=`oc project -q`; \
	oc process -f deploy/openshift/namespace.yaml \
	    -p OPERATOR_NAMESPACE=$$project \
	    | oc apply -f-; \
	oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p BUILDS=$(CINAMESPACE) \
	    -p OPERATOR_NAMESPACE=$$project \
	    | oc apply -f-; \

.PHONY: occlean
occlean: occheck
	@@ls deploy/openshift/cr/*.yaml | while read f; \
	    do \
		oc process -f $$f -p FRONTNAME=$(FRONTNAME) \
		    | oc delete -f- || true; \
	    done

.PHONY: ocdemo
ocdemo: ocdeploy
	@@oc process -f deploy/openshift/cr/jenkins.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/ci/build.yaml -\
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/ci/imagestream.yaml \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    | oc delete -f- || true

.PHONY: ocreload
ocreload: ocecheck
	@@oc delete pod -n $(OPNAMESPACE) --all


##################
### Generic    ###
##################

.PHONY: tektontoken
tektontoken:
	@@if ! kubectl describe ns $(CINAMESPACE); then \
	    if ! kubectl create ns $(CINAMESPACE); then \
		echo Could not create ns $(CINAMESPACE) >&2; \
		exit 1; \
	    fi; \
	fi >/dev/null
	@@( \
	    cat deploy/kubernetes/cr/tekton-token.yaml; \
	    cat ~/.ssh/id_rsa | sed 's|^\(.*\)|      \1|'; \
	) | kubectl apply -f-

.PHONY: checkcr
checkcr:
	@@awk '/^  kind:/{print $$2}' watches.d/*yaml | while read kind; \
	    do \
		test "$$kind" = Ci -o "$$kind" = Wsweet && continue; \
		if ! grep "$$kind" roles/commons/tasks/helpers/wait-for.yaml >/dev/null; then \
		    echo missing $$kind in wait-for.yaml; \
		fi; \
		if ! grep "$$kind" roles/purge/defaults/main.yaml >/dev/null; then \
		    echo missing $$kind in purge defaults; \
		fi; \
	    done

.PHONY: checkdashboards
checkdashboards:
	@@if grep -R '"tableColumn": "[^"]' roles/prometheus/files/dashboards; then \
	    echo tablecolmns should be empty; \
	fi
	@@if grep -R '^  "refresh": ' roles/prometheus/files/dashboards | grep -v 1m | grep refresh; then \
	    echo dashboard should refresh every minute; \
	fi
	@@if grep -R pluginVersion roles/prometheus/files/dashboards; then \
	    echo should remove pluginVersions; \
	fi
	@@if grep -R '^  "timezone": ' roles/prometheus/files/dashboards | grep -v browser | grep timezone; then \
	    echo timezone should be browser; \
	fi
	@@if grep -R '"sort": 0' roles/prometheus/files/dashboards; then \
	    echo some list is not sorted; \
	fi
	@@if grep '^        "refresh": 0' roles/prometheus/files/dashboards/*; then \
	    echo something not refreshing; \
	fi
	@@ls roles/prometheus/files/dashboards/* | while read f; \
	    do grep '^  "refresh":' $$f >/dev/null && continue; \
	    echo "$$f not refreshing"; \
	done
	@@( \
	    NDASH=`grep -R '^  "id": ' roles/prometheus/files/dashboards | cut -d: -f3 | awk 'END{print NR}'`; \
	    LASTID=`grep -R '^  "id": ' roles/prometheus/files/dashboards | cut -d: -f3 | sort -n | tail -1`; \
	    if test $$LASTID -ne $$NDASH 2>/dev/null; then \
		echo something looks wrong with dashboard ids; \
	    fi; \
	)
	@@ls roles/prometheus/files/dashboards/* | while read f; \
	    do cat $$f | jq .title >/dev/null && continue; \
	    echo "$$f JSON error"; \
	done

.PHONY: checktags
checktags:
	@@grep _tag: roles/commons/defaults/main.yaml | cut -d: -f1 \
	    | while read var; \
		do \
		    first=`grep -R "^$$var:" roles/*/defaults | awk -F: '{print $$3}' | sed 's| ||g'`; \
		    if grep -R "^$$var:" roles/*/defaults | grep -v "$$var: $$first" | grep tag >/dev/null; then \
			echo == mismstching $$var == first=$$first ==; \
			grep -R "^$$var:" roles/*/defaults; \
		    fi; \
		done

.PHONY: checkpods
checkpods:
	@@kubectl get pods -A | grep -vE '1/1|2/2|3/3|4/4|5/5|6/6|7/7|8/8|Completed'
